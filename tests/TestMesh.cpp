//: TestMesh.cpp

#include "catch.hpp"

#include "Mesh.hpp"

TEST_CASE("Cells are accesible", "[Mesh]")
{
  Mesh mesh;

  mesh(0, 0, 0) = 1.;

  REQUIRE(mesh(0, 0, 0) == Approx(1.));
}

