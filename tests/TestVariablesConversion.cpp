//: TestVariablesConversion.cpp

#include "catch.hpp"

#include "VariablesConversion.hpp"
#include "VariablesAdditional.hpp"
#include "Utility.hpp"

#include <iostream>

TEST_CASE("Primitive -> Conservative -> Primitive = identity mapping", "[VariablesConversion]")
{
  const double alpha1 = 0.9999999 + 1e-14;
  const double rho1   = 1.2 + 3e-14;
  const double rho2   = 0.3 + 1e-14;
  const double u1     = 1e-3 - 7e-14;
  const double u2     = 2e-3 + 7e-14;
  const double F11    = 1e-7 - 2e-14;
  const double F12    = 1e-2 + 6e-14;
  const double F21    = 1e-1 + 1e-14;
  const double F22    = 1e-4 - 7e-14;

  const double alpha2 = VariablesAdditional::alpha2(alpha1);
  const double rho    = VariablesAdditional::rho(alpha1, alpha2, rho1, rho2);

  const double U0 = VariablesConversion::U0(alpha1);
  const double U1 = VariablesConversion::U1(alpha1, rho1);
  const double U2 = VariablesConversion::U2(alpha2, rho2);
  const double U3 = VariablesConversion::U3(rho, u1);
  const double U4 = VariablesConversion::U4(rho, u2);
  const double U5 = VariablesConversion::U5(rho, F11);
  const double U6 = VariablesConversion::U6(rho, F12);
  const double U7 = VariablesConversion::U7(rho, F21);
  const double U8 = VariablesConversion::U8(rho, F22);

  const double rhoR = U1 + U2;

  const double alpha1R = VariablesConversion::alpha1(U0);
  const double rho1R   = VariablesConversion::rho1(U0, U1);
  const double rho2R   = VariablesConversion::rho2(U0, U2);
  const double u1R     = VariablesConversion::u1(rhoR, U3);
  const double u2R     = VariablesConversion::u2(rhoR, U4);
  const double F11R    = VariablesConversion::F11(rho, U5);
  const double F12R    = VariablesConversion::F12(rho, U6);
  const double F21R    = VariablesConversion::F21(rho, U7);
  const double F22R    = VariablesConversion::F22(rho, U8);

  REQUIRE(alpha1 == Approx(alpha1R).epsilon(1e-16));
  REQUIRE(rho1   == Approx(rho1R)  .epsilon(1e-16));
  REQUIRE(rho2   == Approx(rho2R)  .epsilon(1e-16));
  REQUIRE(u1     == Approx(u1R)    .epsilon(1e-16));
  REQUIRE(u2     == Approx(u2R)    .epsilon(1e-16));
  REQUIRE(F11    == Approx(F11R)   .epsilon(1e-16));
  REQUIRE(F12    == Approx(F12R)   .epsilon(1e-16));
  REQUIRE(F21    == Approx(F21R)   .epsilon(1e-16));
  REQUIRE(F22    == Approx(F22R)   .epsilon(1e-16));
}

