//: TestVariablesAdditional.cpp

#include "catch.hpp"

#include "VariablesAdditional.hpp"

TEST_CASE("Additional variables test", "[VariablesAdditional]")
{
  const double alpha1 =  0.488277273;
  const double rho1   =  2.128849444;
  const double rho2   =  1.773295382;

  const double alpha2 = VariablesAdditional::alpha2(alpha1);
  const double rho    = VariablesAdditional::rho   (alpha1, alpha2, rho1, rho2);
  const double c1     = VariablesAdditional::c1    (alpha1, alpha2, rho1, rho2);
  const double c2     = VariablesAdditional::c2    (alpha1, alpha2, rho1, rho2);

  REQUIRE(alpha2 == Approx(1. - alpha1).epsilon(1e-14));
  REQUIRE(rho    == Approx(alpha1 * rho1 + (1. - alpha1) * rho2).epsilon(1e-14));
  REQUIRE(c1     == Approx(alpha1 * rho1 / rho).epsilon(1e-14));
  REQUIRE(c2     == Approx(alpha2 * rho2 / rho).epsilon(1e-14));
}

