//: TestBoundaryConditionYBack.cpp

#include "catch.hpp"

#include "BoundaryConditionYBack.hpp"

#include <iostream>

TEST_CASE("y-back hybrid (nonslip/slip) boundary condition test", "[BoundaryConditionYBack: Hybrid]")
{
  /*const double alpha1 = 0.5;*/
  /*const double rho    = 2.6;*/
  /*const double rho1   = 2.3;*/
  /*const double u11    = 1e-14;*/
  /*const double u12    = 1e-5;*/
  /*const double u21    = 1e-14;*/
  /*const double u22    = 1e-14;*/
  /*const double F11    = 1. + 1e-12;*/
  /*const double F12    = 1e-14;*/
  /*const double F21    = 2e-14;*/
  /*const double F22    = 1. + 2e-12;*/

  /*double xBalpha1 = 0.;*/
  /*double xBrho    = 0.;*/
  /*double xBrho1   = 0.;*/ 
  /*double xBu11    = 0.;*/
  /*double xBu12    = 0.;*/
  /*double xBu21    = 0.;*/
  /*double xBu22    = 0.;*/
  /*double xBF11    = 0.;*/
  /*double xBF12    = 0.;*/
  /*double xBF21    = 0.;*/
  /*double xBF22    = 0.;*/

  /*BC::slipHybridYBack( alpha1*/
  /*                   , rho*/
  /*                   , rho1*/
  /*                   , u11*/
  /*                   , u12*/
  /*                   , u21*/
  /*                   , u22*/
  /*                   , F11*/
  /*                   , F12*/
  /*                   , F21*/
  /*                   , F22*/

  /*                   , xBalpha1*/
  /*                   , xBrho*/
  /*                   , xBrho1*/
  /*                   , xBu11*/
  /*                   , xBu12*/
  /*                   , xBu21*/
  /*                   , xBu22*/
  /*                   , xBF11*/
  /*                   , xBF12*/
  /*                   , xBF21*/
  /*                   , xBF22);*/
 
  /***/
  /*std::cout << "xBalpha1 = " << xBalpha1 << std::endl*/
  /*          << "xBrho    = " << xBrho    << std::endl*/
  /*          << "xBrho1   = " << xBrho1   << std::endl*/
  /*          << "xBu11    = " << xBu11    << "*" << std::endl*/
  /*          << "xBu12    = " << xBu12    << std::endl*/
  /*          << "xBu21    = " << xBu21    << "*" << std::endl*/
  /*          << "xBu22    = " << xBu22    << "*"  << std::endl*/
  /*          << "xBF11    = " << xBF11    << std::endl*/
  /*          << "xBF12    = " << xBF12    << std::endl*/
  /*          << "xBF21    = " << xBF21    << std::endl*/
  /*          << "xBF22    = " << xBF22    << std::endl;*/
  /**/

  /*REQUIRE(alpha1 == Approx(xBalpha1) .margin (1e-14));*/
  /*REQUIRE(rho    == Approx(xBrho)    .epsilon(1e-8));*/
  /*REQUIRE(rho1   == Approx(xBrho1)   .epsilon(1e-8));*/
  /*REQUIRE(u11    == Approx(xBu11)    .margin (1e-12));*/
  /*REQUIRE(u12    == Approx(xBu12)    .margin (1e-12));*/
  /*REQUIRE(u21    == Approx(xBu21)    .margin (1e-12));*/
  /*REQUIRE(u22    == Approx(xBu22)    .margin (1e-12));*/
  /*REQUIRE(F11    == Approx(xBF11)    .epsilon(1e-8));*/
  /*REQUIRE(F12    == Approx(xBF12)    .margin (1e-12));*/
  /*REQUIRE(F21    == Approx(xBF21)    .margin (1e-12));*/
  /*REQUIRE(F22    == Approx(xBF22)    .epsilon(1e-8));*/
}

