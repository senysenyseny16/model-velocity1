//: VariablesConversion.cpp

#include "VariablesConversion.hpp"

#include <cassert>
#include <cmath>

namespace VariablesConversion {

double alpha1(const double U0)
{
  assert(U0 > 0. and U0 < 1.);

  return U0;
}

double rho1( const double U0
           , const double U1)
{
  assert(U0 > 0. and U0 < 1.);
  assert(U1 > 0.);

  const double rho1 = U1 / U0;

  assert (rho1 > 0.);

  return rho1;
}

double rho2( const double U0
           , const double U2)
{
  assert(U0 > 0. and U0 < 1.);
  assert(U2 > 0.);

  const double rho2 = U2 / (1. - U0);

  assert(rho2 > 0.);

  return rho2;
}

double u1( const double rho
         , const double U3)
{
  assert(rho > 0.);

  return U3 / rho;
}

double u2( const double rho
         , const double U4)
{
  assert(rho > 0.);

  return U4 / rho;
}

double F11( const double rho
          , const double U5)
{
  assert(rho > 0.);

  return U5 / rho;
}

double F12( const double rho
          , const double U6)
{
  assert(rho > 0.);

  return U6 / rho;
}

double F21( const double rho
          , const double U7)
{
  assert(rho > 0.);

  return U7 / rho;
}

double F22( const double rho
          , const double U8)
{
  assert(rho > 0.);

  return U8 / rho;
}

double U0(const double alpha1)
{
  assert(alpha1 > 0. and alpha1 < 1.);

  return alpha1;
}

double U1( const double alpha1
         , const double rho1)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(rho1 > 0.);

  const double U1 = rho1 * alpha1;

  assert(U1 > 0.);

  return U1;
}

double U2( const double alpha2
         , const double rho2)
{
  assert(alpha2 > 0. and alpha2 < 1.);
  assert(rho2 > 0.);

  const double U2 = rho2 * alpha2;

  assert(U2 > 0.);

  return U2;
}

double U3( const double rho
         , const double u1)
{
  assert(rho > 0.);

  return rho * u1;
}

double U4( const double rho
         , const double u2)
{
  assert(rho > 0.);

  return rho * u2;
}

double U5( const double rho
         , const double F11)
{
  assert(rho > 0.);

  return (rho * F11);
}

double U6( const double rho
         , const double F12)
{
  assert(rho > 0.);

  return (rho * F12);
}

double U7( const double rho
         , const double F21)
{
  assert(rho > 0.);

  return (rho * F21);
}

double U8( const double rho
         , const double F22)
{
  assert(rho > 0.);

  return (rho * F22);
}

} // namespace VariablesConversion

