//: GNUPlot.cpp

#include "GNUPlot.hpp"

#include <iostream>

GNUPlot::GNUPlot()
{
  #ifdef WIN32
    gnuPlotPipe = _popen(GNUPLOT_NAME, "w");
  #else
    gnuPlotPipe =  popen(GNUPLOT_NAME, "w");
  #endif

  if (!gnuPlotPipe) {
    std::cerr << "GNU Plot not found";
  }
}

GNUPlot::~GNUPlot()
{
  fprintf(gnuPlotPipe, "exit\n");

  #ifdef WIN32
    _pclose(gnuPlotPipe);
  #else
     pclose(gnuPlotPipe);
  #endif
}

void GNUPlot::operator()(const std::string& command)
{
  fprintf(gnuPlotPipe, "%s\n", command.c_str());
  fflush(gnuPlotPipe);
}

