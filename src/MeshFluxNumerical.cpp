//: MeshFluxNumerical.cpp

#include "MeshFluxNumerical.hpp"
#include "MeshFluxDifferential.hpp"

#include "FluxNumerical.hpp"
#include "FluxDifferential.hpp"

#include "VariablesConversion.hpp"
#include "VariablesAdditional.hpp"
#include "Energy.hpp"
#include "ShearStress.hpp"
#include "TotalStress.hpp"

#include "HLLC.hpp"

#include "SoundSpeed.hpp"

#include <cmath>
#include <iostream>

namespace FluxNumerical {

MeshFluxNumerical::MeshFluxNumerical()
  : c()
  , nC()
{ }

MeshFluxNumerical::~MeshFluxNumerical()
{ }

HLLC::HLLC()
  : MeshFluxNumerical()
  , maxCGlobalX(0.)
  , maxCGlobalY(0.)
{ }


void HLLC::calcFlux( const MeshFluxDifferential& fD
                   , const MeshEdge&             rC
                   , const MeshEdge&             rP
                   , const Mesh&                 p)
{
  maxCGlobalX = 0.;
  maxCGlobalY = 0.;

  #pragma omp parallel for reduction(max: maxCGlobalX)
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell + 1; i <= Geometry::lastCellX; ++i) {

      HLLCX( fD.dataXF()
           , fD.dataXB()
           , rC.dataXF()
           , rC.dataXB()
           , rP.dataXF()
           , rP.dataXB()
           , p
           , i - 1
           , i
           , j
           , c
           , nC
           , maxCGlobalX);
    }
  }

  #pragma omp parallel for reduction(max: maxCGlobalY)
  for (size_t j = Geometry::firstCell + 1; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      HLLCY( fD.dataYF()
           , fD.dataYB()
           , rC.dataYF()
           , rC.dataYB()
           , rP.dataYF()
           , rP.dataYB()
           , p
           , i
           , j - 1
           , j
           , c
           , nC
           , maxCGlobalY);
    }
  }
}

void HLLC::HLLCX( const Mesh& fDB
                , const Mesh& fDF
                , const Mesh& rCB
                , const Mesh& rCF
                , const Mesh& rPB
                , const Mesh& rPF
                , const Mesh& p
                , const size_t iB
                , const size_t iF
                , const size_t j
                , MeshEdgeMutual& c
                , MeshEdgeMutual& nC
                , double& maxCGlobalX)
{
  // Unpack variables.
  const double alpha1B = rPB(0, iB, j);
  const double rho1B   = rPB(1, iB, j);
  const double rho2B   = rPB(2, iB, j);
  const double u1B     = rPB(3, iB, j);
  const double u2B     = rPB(4, iB, j);
  const double F11B    = rPB(5, iB, j);
  const double F12B    = rPB(6, iB, j);
  const double F21B    = rPB(7, iB, j);
  const double F22B    = rPB(8, iB, j);
  const double alpha2B = VariablesAdditional::alpha2(alpha1B);

  const double alpha1F = rPF(0, iF, j);
  const double rho1F   = rPF(1, iF, j);
  const double rho2F   = rPF(2, iF, j);
  const double u1F     = rPF(3, iF, j);
  const double u2F     = rPF(4, iF, j);
  const double F11F    = rPF(5, iF, j);
  const double F12F    = rPF(6, iF, j);
  const double F21F    = rPF(7, iF, j);
  const double F22F    = rPF(8, iF, j);
  const double alpha2F = VariablesAdditional::alpha2(alpha1F);

  // 1. Find SB, SF.

  // 1. 1. Find longitudinal velocity of mixture.
  const double c1B = VariablesAdditional::c1(alpha1B, alpha2B, rho1B, rho2B);
  const double c2B = 1. - c1B;

  const double c1F = VariablesAdditional::c1(alpha1F, alpha2F, rho1F, rho2F);
  const double c2F = 1. - c1F;

  const double longVelocityMixtureB = SoundSpeed::longitudinalVelocityMixture(rho1B, rho2B, c1B, c2B);
  const double longVelocityMixtureF = SoundSpeed::longitudinalVelocityMixture(rho1F, rho2F, c1F, c2F);

  // 1. 2. Find SB, SF.
  const double SB = std::min(u1B - longVelocityMixtureB, u1F - longVelocityMixtureF);
  const double SF = std::max(u1B + longVelocityMixtureB, u1F + longVelocityMixtureF);

  // 2. Find SM.
  const double totalStress11B = TotalStress::totalStress11( alpha1B
                                                          , alpha2B
                                                          , Energy::p1(rho1B)
                                                          , Energy::p2(rho2B)
                                                          , ShearStress::shearStress11(rho1B, F11B, F12B, F21B, F22B));

  const double totalStress11F = TotalStress::totalStress11( alpha1F
                                                          , alpha2F
                                                          , Energy::p1(rho1F)
                                                          , Energy::p2(rho2F)
                                                          , ShearStress::shearStress11(rho1F, F11F, F12F, F21F, F22F));

  const double rhoB = VariablesAdditional::rho(alpha1B, alpha2B, rho1B, rho2B);
  const double rhoF = VariablesAdditional::rho(alpha1F, alpha2F, rho1F, rho2F);

  const double SM = FHLLC::SM(rhoB, rhoF, u1B, u1F, totalStress11B, totalStress11F, SB, SF);

  // 3. Find TotalStress21M.
  const double totalStress21B = TotalStress::totalStress21(alpha1B, ShearStress::shearStress21(rho1B, F11B, F12B, F21B, F22B));
  const double totalStress21F = TotalStress::totalStress21(alpha1F, ShearStress::shearStress21(rho1F, F11F, F12F, F21F, F22F));

  // Sliding treatment.
  double totalStress21M;
  if (alpha1B > SlidingTreatment::eps and alpha1F > SlidingTreatment::eps) {

    totalStress21M = ((u1F - SF) * rhoF * totalStress21B - (u1B - SB) * rhoB * totalStress21F + (u1B - SB) * (u1F - SF) * (u2F - u2B) * (rhoB * rhoF)) / ((u1F - SF) * rhoF - (u1B - SB) * rhoB);

  } else {

    // Sliding.
    totalStress21M = 0.;

  }

  // 4. Calc HLLC B-flux.
  // 4. 1. SB <= 0 and SM >= 0.
  if (SB <= 0. and SM > 0.) {

    // Calc UM.
    const double alpha1 = alpha1B;
    const double alpha2 = VariablesAdditional::alpha2(alpha1);

    const double mulDensity = (SB - u1B) / (SB - SM);
    const double rho    = rhoB  * mulDensity;
    const double rho1   = rho1B * mulDensity;
    const double rho2   = rho2B * mulDensity;

    assert(rho > 0.);
    assert(rho1 > 0.);
    assert(rho2 > 0.);

    const double u2 = u2B + (totalStress21M - totalStress21B) / ((u1B - SB) * rhoB);

    const double rhoF11 = rhoB * F11B;
    const double rhoF12 = rhoB * F12B;
    /* const double rhoF21 = rhoB * (F21B * (SB - u1B) - F11B * (u2 - u2B)) / (SB - SM); */
    /* const double rhoF22 = rhoB * (F22B * (SB - u1B) - F12B * (u2 - u2B)) / (SB - SM); */
    const double rhoF21 = rhoB * ((F21B * SB - F11B * u2) - (F21B  * u1B - F11B * u2B)) / (SB - SM);
    const double rhoF22 = rhoB * ((F22B * SB - F12B * u2) - (F22B  * u1B - F12B * u2B)) / (SB - SM);

    // Form conservative vectors.
    double U[Physics::varCount];

    U[1]  = alpha1 * rho1;
    U[2]  = alpha2 * rho2;
    U[3]  = rho * SM;
    U[4]  = rho * u2;

    // Calc HLLC flux.
    // Calc conservative part.
    for (size_t v = 1; v < 5; ++v) {
      c.edgeX(v, iF, j) = fDB(v, iB, j) + SB * (U[v] - rCB(v, iB, j));
    }

    c.edgeX(0, iF, j) = alpha1 * SM;
    c.edgeX(5, iF, j) = rhoF11 * SM;
    c.edgeX(6, iF, j) = rhoF12 * SM;
    c.edgeX(7, iF, j) = rhoF21 * SM;
    c.edgeX(8, iF, j) = rhoF22 * SM;

    // Fill non-conservative part.
    nC.edgeX(0, iF, j) = SM;
    nC.edgeX(5, iF, j) = SM;
    nC.edgeX(6, iF, j) = SM;
    nC.edgeX(7, iF, j) = u2;
    nC.edgeX(8, iF, j) = u2;

  } else if (SM <= 0. and SF >= 0.) { // 4. 2. SM <= 0 and SF >= 0.

    // Calc UM.
    const double alpha1 = alpha1F;
    const double alpha2 = VariablesAdditional::alpha2(alpha1);

    const double mulDensity = (SF - u1F) / (SF - SM);
    const double rho    = rhoF  * mulDensity;
    const double rho1   = rho1F * mulDensity;
    const double rho2   = rho2F * mulDensity;

    assert(rho > 0.);
    assert(rho1 > 0.);
    assert(rho2 > 0.);

    const double u2 = u2F + (totalStress21M - totalStress21F) / ((u1F - SF) * rhoF);

    const double rhoF11 = rhoF * F11F;
    const double rhoF12 = rhoF * F12F;
    /* const double rhoF21 = rhoF * (F21F * (SF - u1F) - F11F * (u2 - u2F)) / (SF - SM); */
    /* const double rhoF22 = rhoF * (F22F * (SF - u1F) - F12F * (u2 - u2F)) / (SF - SM); */
    const double rhoF21 = rhoF * ((F21F * SF - F11F * u2) - (F21F  * u1F - F11F * u2F)) / (SF - SM);
    const double rhoF22 = rhoF * ((F22F * SF - F12F * u2) - (F22F  * u1F - F12F * u2F)) / (SF - SM);

    // Form conservative vectors.
    double U[Physics::varCount];

    U[1]  = alpha1 * rho1;
    U[2]  = alpha2 * rho2;
    U[3]  = rho * SM;
    U[4]  = rho * u2;

    // Calc HLLC flux.
    // Calc conservative part.
    for (size_t v = 1; v < 5; ++v) {
      c.edgeX(v, iF, j) = fDF(v, iF, j) + SF * (U[v] - rCF(v, iF, j));
    }

    c.edgeX(0, iF, j) = alpha1 * SM;
    c.edgeX(5, iF, j) = rhoF11 * SM;
    c.edgeX(6, iF, j) = rhoF12 * SM;
    c.edgeX(7, iF, j) = rhoF21 * SM;
    c.edgeX(8, iF, j) = rhoF22 * SM;

    // Fill non-conservative part.
    nC.edgeX(0, iF, j) = SM;
    nC.edgeX(5, iF, j) = SM;
    nC.edgeX(6, iF, j) = SM;
    nC.edgeX(7, iF, j) = u2;
    nC.edgeX(8, iF, j) = u2;

  } else if (SB >= 0.) { // 4. 3. SB >= 0.

    std::cout << "Supersonic flow. Abort." << std::endl;
    std::abort();

  } else { // 4. 4. SF <= 0.

    std::cout << "Supersonic flow. Abort." << std::endl;
    std::abort();

  }

  // Find maximum sound speed in x-direction.
  const double maxC = std::max(std::abs(SB), std::abs(SF));
  if (maxC > maxCGlobalX) maxCGlobalX = maxC;
}

void HLLC::HLLCY( const Mesh& fDB
                , const Mesh& fDF
                , const Mesh& rCB
                , const Mesh& rCF
                , const Mesh& rPB
                , const Mesh& rPF
                , const Mesh& p
                , const size_t i
                , const size_t jB
                , const size_t jF
                , MeshEdgeMutual& c
                , MeshEdgeMutual& nC
                , double& maxCGlobalY)
{
  // Unpack variables.
  const double alpha1B = rPB(0, i, jB);
  const double rho1B   = rPB(1, i, jB);
  const double rho2B   = rPB(2, i, jB);
  const double u1B     = rPB(3, i, jB);
  const double u2B     = rPB(4, i, jB);
  const double F11B    = rPB(5, i, jB);
  const double F12B    = rPB(6, i, jB);
  const double F21B    = rPB(7, i, jB);
  const double F22B    = rPB(8, i, jB);
  const double alpha2B = VariablesAdditional::alpha2(alpha1B);

  const double alpha1F = rPF(0, i, jF);
  const double rho1F   = rPF(1, i, jF);
  const double rho2F   = rPF(2, i, jF);
  const double u1F     = rPF(3, i, jF);
  const double u2F     = rPF(4, i, jF);
  const double F11F    = rPF(5, i, jF);
  const double F12F    = rPF(6, i, jF);
  const double F21F    = rPF(7, i, jF);
  const double F22F    = rPF(8, i, jF);
  const double alpha2F = VariablesAdditional::alpha2(alpha1F);

  // 1. Find SB, SF.

  // 1. 1. Find longitudinal velocity of mixture.
  const double c1B = VariablesAdditional::c1(alpha1B, alpha2B, rho1B, rho2B);
  const double c2B = 1. - c1B;

  const double c1F = VariablesAdditional::c1(alpha1F, alpha2F, rho1F, rho2F);
  const double c2F = 1. - c1F;

  const double longVelocityMixtureB = SoundSpeed::longitudinalVelocityMixture(rho1B, rho2B, c1B, c2B);
  const double longVelocityMixtureF = SoundSpeed::longitudinalVelocityMixture(rho1F, rho2F, c1F, c2F);

  // 1. 2. Find SB, SF.
  const double SB = std::min(u2B - longVelocityMixtureB, u2F - longVelocityMixtureF);
  const double SF = std::max(u2B + longVelocityMixtureB, u2F + longVelocityMixtureF);

  // 2. Find SM.
  const double totalStress22B = TotalStress::totalStress22( alpha1B
                                                          , alpha2B
                                                          , Energy::p1(rho1B)
                                                          , Energy::p2(rho2B)
                                                          , ShearStress::shearStress22(rho1B, F11B, F12B, F21B, F22B));

  const double totalStress22F = TotalStress::totalStress22( alpha1F
                                                          , alpha2F
                                                          , Energy::p1(rho1F)
                                                          , Energy::p2(rho2F)
                                                          , ShearStress::shearStress22(rho1F, F11F, F12F, F21F, F22F));

  const double rhoB = VariablesAdditional::rho(alpha1B, alpha2B, rho1B, rho2B);
  const double rhoF = VariablesAdditional::rho(alpha1F, alpha2F, rho1F, rho2F);

  const double SM = FHLLC::SM(rhoB, rhoF, u2B, u2F, totalStress22B, totalStress22F, SB, SF);

  // 3. Find TotalStress12M.
  const double totalStress12B = TotalStress::totalStress12(alpha1B, ShearStress::shearStress12(rho1B, F11B, F12B, F21B, F22B));
  const double totalStress12F = TotalStress::totalStress12(alpha1F, ShearStress::shearStress12(rho1F, F11F, F12F, F21F, F22F));

  // Sliding treatment.
  double totalStress12M;
  if (alpha1B > SlidingTreatment::eps and alpha1F > SlidingTreatment::eps) {

    totalStress12M = ((u2F - SF) * rhoF * totalStress12B - (u2B - SB) * rhoB * totalStress12F + (u2B - SB) * (u2F - SF) * (u1F - u1B) * (rhoB * rhoF)) / ((u2F - SF) * rhoF - (u2B - SB) * rhoB);

  } else {

    // Sliding.
    totalStress12M = 0.;

  }

  // 4. Calc HLLC B-flux.
  // 4. 1. SB <= 0 and SM >= 0.
  if (SB <= 0. and SM > 0.) {

    // Calc UM.
    const double alpha1 = alpha1B;
    const double alpha2 = VariablesAdditional::alpha2(alpha1);

    const double mulDensity = (SB - u2B) / (SB - SM);
    const double rho    = rhoB  * mulDensity;
    const double rho1   = rho1B * mulDensity;
    const double rho2   = rho2B * mulDensity;

    assert(rho > 0.);
    assert(rho1 > 0.);
    assert(rho2 > 0.);

    const double u1 = u1B + (totalStress12M - totalStress12B) / ((u2B - SB) * rhoB);

    const double rhoF21 = rhoB * F21B;
    const double rhoF22 = rhoB * F22B;
    /* const double rhoF11 = rhoB * (F11B * (SB - u2B) - F21B * (u1 - u1B)) / (SB - SM); */
    /* const double rhoF12 = rhoB * (F12B * (SB - u2B) - F22B * (u1 - u1B)) / (SB - SM); */
    const double rhoF11 = rhoB * ((F11B * SB - F21B * u1) - (F11B  * u2B - F21B * u1B)) / (SB - SM);
    const double rhoF12 = rhoB * ((F12B * SB - F22B * u1) - (F12B  * u2B - F22B * u1B)) / (SB - SM);

    // Form conservative vectors.
    double U[Physics::varCount];

    U[1]  = alpha1 * rho1;
    U[2]  = alpha2 * rho2;
    U[3]  = rho * u1;
    U[4]  = rho * SM;

    // Calc HLLC flux.
    // Calc conservative part.
    for (size_t v = 1; v < 5; ++v) {
      c.edgeY(v, i, jF) = fDB(v, i, jB) + SB * (U[v] - rCB(v, i, jB));
    }

    c.edgeY(0, i, jF) = alpha1 * SM;
    c.edgeY(5, i, jF) = rhoF11 * SM;
    c.edgeY(6, i, jF) = rhoF12 * SM;
    c.edgeY(7, i, jF) = rhoF21 * SM;
    c.edgeY(8, i, jF) = rhoF22 * SM;

    // Fill non-conservative part.
    nC.edgeY(0, i, jF) = SM;
    nC.edgeY(5, i, jF) = u1;
    nC.edgeY(6, i, jF) = u1;
    nC.edgeY(7, i, jF) = SM;
    nC.edgeY(8, i, jF) = SM;

  } else if (SM <= 0. and SF >= 0.) { // 4. 2. SM <= 0 and SF >= 0.

    // Calc UM.
    const double alpha1 = alpha1F;
    const double alpha2 = VariablesAdditional::alpha2(alpha1);

    const double mulDensity = (SF - u2F) / (SF - SM);
    const double rho    = rhoF  * mulDensity;
    const double rho1   = rho1F * mulDensity;
    const double rho2   = rho2F * mulDensity;

    assert(rho > 0.);
    assert(rho1 > 0.);
    assert(rho2 > 0.);

    const double u1 = u1F + (totalStress12M - totalStress12F) / ((u2F - SF) * rhoF);

    const double rhoF21 = rhoF * F21F;
    const double rhoF22 = rhoF * F22F;
    /* const double rhoF11 = rhoF * (F11F * (SF - u2F) - F21F * (u1 - u1F)) / (SF - SM); */
    /* const double rhoF12 = rhoF * (F12F * (SF - u2F) - F22F * (u1 - u1F)) / (SF - SM); */
    const double rhoF11 = rhoF * ((F11F * SF - F21F * u1) - (F11F  * u2F - F21F * u1F)) / (SF - SM);
    const double rhoF12 = rhoF * ((F12F * SF - F22F * u1) - (F12F  * u2F - F22F * u1F)) / (SF - SM);

    // Form conservative vectors.
    double U[Physics::varCount];

    U[1]  = alpha1 * rho1;
    U[2]  = alpha2 * rho2;
    U[3]  = rho * u1;
    U[4]  = rho * SM;

    // Calc HLLC flux.
    // Calc conservative part.
    for (size_t v = 1; v < 5; ++v) {
      c.edgeY(v, i, jF) = fDF(v, i, jF) + SF * (U[v] - rCF(v, i, jF));
    }

    c.edgeY(0, i, jF) = alpha1 * SM;
    c.edgeY(5, i, jF) = rhoF11 * SM;
    c.edgeY(6, i, jF) = rhoF12 * SM;
    c.edgeY(7, i, jF) = rhoF21 * SM;
    c.edgeY(8, i, jF) = rhoF22 * SM;

    // Fill non-conservative part.
    nC.edgeY(0, i, jF) = SM;
    nC.edgeY(5, i, jF) = u1;
    nC.edgeY(6, i, jF) = u1;
    nC.edgeY(7, i, jF) = SM;
    nC.edgeY(8, i, jF) = SM;

  } else if (SB >= 0.) { // 4. 3. SB >= 0.

    std::cout << "Supersonic flow. Abort." << std::endl;
    std::abort();

  } else { // 4. 4. SF <= 0.

    std::cout << "Supersonic flow. Abort." << std::endl;
    std::abort();

  }

  // Find maximum sound speed in x-direction.
  const double maxC = std::max(std::abs(SB), std::abs(SF));
  if (maxC > maxCGlobalY) maxCGlobalY = maxC;
}

double HLLC::maxCX() const
{
  return maxCGlobalX;
}

double HLLC::maxCY() const
{
  return maxCGlobalY;
}

MeshFluxNumerical* createMeshFluxNumerical()
{
  switch (SchemePars::fluxType) {

  case (SchemePars::FluxType::HLLC):

    return (new HLLC);

  break;

  default:

    return nullptr;

  break;
  }
}

} // namespace FluxNumerical

