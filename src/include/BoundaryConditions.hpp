//: BoundaryConditions.hpp

#ifndef BC_HPP
#define BC_HPP

class Mesh;
class MeshRec;
namespace FluxNumerical { class MeshFluxNumerical; }

namespace BC {

/** Implement all boundary conditions.
 *
 *  @param physicalVars is Mesh with physical variables.
 *  @param fluxNumerical is calculated numerical flux.
 */
void applyBoundaryConditions1( const Mesh& primitive
                             , MeshRec&    rec);

void applyBoundaryConditions2( const Mesh& primitive
                             , const MeshRec& rec
                             , FluxNumerical::MeshFluxNumerical& fluxNum);

} // namespace BC

#endif // BC_HPP

