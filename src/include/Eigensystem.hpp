//: Eigensystem.hpp

#ifndef EIGENSYSTEM_HPP
#define EIGENSYSTEM_HPP

#include "DefinitionsEigen.hpp"

namespace Eigensystem {

std::pair<Eigen::VectorRow9, Eigen::Matrix9> eigensystemX( double alpha1
                                                         , double rho1
                                                         , double rho2
                                                         , double u1
                                                         , double F11
                                                         , double F12
                                                         , double F21
                                                         , double F22);

std::pair<Eigen::VectorRow9, Eigen::Matrix9> eigensystemY( double alpha1
                                                         , double rho1
                                                         , double rho2
                                                         , double u1
                                                         , double u2
                                                         , double F11
                                                         , double F12
                                                         , double F21
                                                         , double F22);

} // namespace Eigensystem

#endif // EIGENSYSTEM_HPP

