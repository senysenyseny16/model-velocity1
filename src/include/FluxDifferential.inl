//: FluxDifferential.inl

namespace Flux {

inline double xF0( const double alpha1
                 , const double u1)
{
  assert(alpha1 > 0. and alpha1 < 1.);

  return alpha1 * u1;
}

inline double xF1( const double alpha1
                 , const double rho1
                 , const double u1)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(rho1 > 0.);

  return (alpha1 * rho1) * u1;
}

inline double xF2( const double alpha2
                 , const double rho2
                 , const double u1)
{
  assert(alpha2 > 0. and alpha2 < 1.);
  assert(rho2 > 0.);

  return (alpha2 * rho2) * u1;
}

inline double xF3( const double rho
                 , const double u1
                 , const double totalStress11)
{
  assert(rho > 0.);

  return rho * (u1 * u1) - totalStress11;
}

inline double xF4( const double rho
                 , const double u1
                 , const double u2
                 , const double totalStress21)
{
  assert(rho > 0.);

  return rho * (u1 * u2) - totalStress21;
}

inline double xF5( const double rho
                 , const double u1
                 , const double F11)
{
  assert(rho > 0.);

  return (rho * F11) * u1;
}

inline double xF6( const double rho
                 , const double u1
                 , const double F12)
{
  assert(rho > 0.);

  return (rho * F12) * u1;
}

inline double xF7( const double rho
                 , const double u1
                 , const double F21)
{
  assert(rho > 0.);

  return (rho * F21) * u1;
}

inline double xF8( const double rho
                 , const double u1
                 , const double F22)
{
  assert(rho > 0.);

  return (rho * F22) * u1;
}

inline double yF0( const double alpha1
                 , const double u2)
{
  assert(alpha1 > 0. and alpha1 < 1.);

  return alpha1 * u2;
}

inline double yF1( const double alpha1
                 , const double rho1
                 , const double u2)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(rho1 > 0.);

  return (alpha1 * rho1) * u2;
}

inline double yF2( const double alpha2
                 , const double rho2
                 , const double u2)
{
  assert(alpha2 > 0. and alpha2 < 1.);
  assert(rho2 > 0.);

  return (alpha2 * rho2) * u2;
}

inline double yF3( const double rho
                 , const double u1
                 , const double u2
                 , const double totalStress12)
{
  assert(rho > 0.);

  return rho * (u1 * u2) - totalStress12;
}

inline double yF4( const double rho
                 , const double u2
                 , const double totalStress22)
{
  assert(rho > 0.);

  return rho * (u2 * u2) - totalStress22;
}

inline double yF5( const double rho
                 , const double u2
                 , const double F11)
{
  assert(rho > 0.);

  return (rho * F11) * u2;
}

inline double yF6( const double rho
                 , const double u2
                 , const double F12)
{
  assert(rho > 0.);

  return (rho * F12) * u2;
}

inline double yF7( const double rho
                 , const double u2
                 , const double F21)
{
  assert(rho > 0.);

  return (rho * F21) * u2;
}

inline double yF8( const double rho
                 , const double u2
                 , const double F22)
{
  assert(rho > 0.);

  return (rho * F22) * u2;
}

} // namespace Flux

