//: InterfaceNormal.hpp

#ifndef INTERFACE_NORMAL_HPP
#define INTERFACE_NORMAL_HPP

namespace InterfaceNormal {

/* Find grad(f) = (f_x, f_y) by Parker and Young's method.
 *
 * James Edward Pilliod Jr., Elbridge Gerry Puckett //
 * Second-order accurate volume-of-fluid algorithms for tracking material interfaces,
 * Journal of Computational Physics, 199, 2004, 465-502.
 *
 * +-------+-------+-------+------> x
 * |       |       |       |
 * | fxByB | fxCyB | fxFyB |
 * |       |       |       |
 * +-------+-------+-------+
 * |       |       |       |
 * | fxByC |   x   | fxFyC |
 * |       |       |       |
 * +-------+-------+-------+
 * |       |       |       |
 * | fxByF | fxCyF | fxFyF |
 * |       |       |       |
 * +-------+-------+-------+
 * |
 * |
 * |
 * v y
 *
 *
 * @param fx - partial derivative of f with respect to x.
 * @param fy - partial derivative of f with respect to y.
 *
 */
void interfaceNormalPY ( double fxByB
                       , double fxCyB
                       , double fxFyB
                       , double fxByC
                       , double fxFyC
                       , double fxByF
                       , double fxCyF
                       , double fxFyF
                       , double& fx
                       , double& fy);

} // namespace InterfaceNormal

#endif //  INTERFACE_NORMAL_HPP

