//: Definitions.hpp

#ifndef DEFINITIONS_HPP
#define DEFINITIONS_HPP

#include "GNUPlot.hpp"

#include <cstddef> // for size_t.
#include <algorithm> // for std::max.

//! Geometry parameters of mesh.
namespace Geometry {

  /// Number of cells in x-direction.
  const size_t cellCountX = 400;

  /// Number of cells in y-direction.
  const size_t cellCountY = 6;

  /// Number of ghost cells.
  const size_t cellCountGhost = 1;

  /// Index of first cell.
  const size_t firstCell = cellCountGhost;

  /// Index of last cell in x-direction.
  const size_t lastCellX = firstCell + cellCountX - 1;

  /// Index of last cell in y-direction.
  const size_t lastCellY = firstCell + cellCountY - 1;

  /// Number of [normal + ghost] cells in x-direction.
  const size_t meshSizeX = (cellCountX + 2 * cellCountGhost);

  /// Number of [normal + ghost] cells in y-direction.
  const size_t meshSizeY = (cellCountY + 2 * cellCountGhost);

  /// Number of cells in mesh for one variable.
  const size_t meshSizeVar = meshSizeX * meshSizeY;

  enum class Edge {
      XB
    , XF
    , YB
    , YF
  };

  enum class Direction {
      X
    , Y
  };

} // namespace Mesh

//! Physics definitions of problem.
namespace Physics {

  /// Evolution field variables.
  const size_t varCount = 9;

  /** Sound speed, reference density and pressure in porous media.
   *  gamma1 - constant in liquid part of EOS.
   *  mu     - constant in elastic part of EOS.
   */
  const double C1     = 4.760;  // [cm / 10^-5 s]
  const double rho10  = 8.49;   // [g  / cm^3]
  const double p10    = 0.0001; // [g  / (cm * 10^-10 s)]
  const double gamma1 = 2.8;
  const double mu     = 98.;

  /** Sound speed, reference density and pressure in liquid.
   *  gamma2 - constant of EOS.
   */
  const double C2     = 1.5;    // [cm / 10^-5 s]
  const double rho20  = 1.0;    // [g  / cm^3]
  const double p20    = 0.0001; // [g  / (cm * 10^-10 s)]
  const double gamma2 = 2.4;

  /// Area size in x-direction
  const double lengthX = 100.;
  /// Area size in y-direction.
  const double lengthY = 100.;

  /// Time of physical process.
  const double timeEnd = 1e+5; // [10^(-5) s]

} // namespace Physics

//! Numerical scheme parameters.
namespace SchemePars {

  /// Size of cells edge in x-direction.
  const double hx = Physics::lengthX / Geometry::cellCountX;
  /// Size of cells edge in y-direction.
  const double hy = Physics::lengthY / Geometry::cellCountY;

  /// CFL number.
  const double CFL = 0.3;

  /// Types of numerical flux.
  enum class FluxType {
      HLLC    /// HLLC
  };

  /// Type of numerical flux used in scheme.
  const FluxType fluxType = FluxType::HLLC;

  /// Default value of frequency of data saving.
  const size_t defaultSaveInterval = 100;

  /// Default value of frequency of status output.
  const size_t defaultStatusInterval = 50;

  /// Weight of WENO-reconstruction.
  const double wenoEps = 1e-154;

  /// Alpha1 is bounded by interval (alpha1Eps, 1 - alpha1Eps).
  const double alpha1Eps = 1e-12;

  /// Value for filling ghost cells.
  const double ghostCellsFillValue = 1e+7;

} // namespace SchemePars

namespace PressureRelaxation {

  /// Enable or disable pressure relaxation.
  const bool enablePressureRelaxation = false;

  /// Accuracy of instant pressure relaxation.
  const double pressureRelaxationTol = 1e-14;

  /// Max number of interations by Newton method.
  const size_t maxIterationNewton = 30;

  /// Max number of interations by Bisection method.
  const size_t maxIterationBisection = 150;

} // namespace PressureRelaxation

//! THINC reconstruction parameters.
namespace THINC {

  /// Enable or disable THINC reconstruction.
  const bool enableTHINC = false;

  /// Smoothness of reconstruction.
  const double beta = 1.8;

  /// Minimum beta value (for weighted reconstruction).
  const double minBeta = 0.01;

  /// Interface indicator.
  const double eps = 9e-6;

  enum class RecType {
      Constant
    , WenoThird
    , WenoFifth
    , Muscl2
  };

} // namespace THINC

namespace SlidingTreatment {

  const double eps = 0.0;

} // namespace SlidingTreatment

//! Boundary conditions.
namespace BC {

  /// Types of X-back (left) boundary conditions.
  enum class BoundaryCondXBack {
    Periodic,
    NonReflect,
    NonReflectCD,
  };

  /// X-back (left) boundary condition.
  const BoundaryCondXBack  boundaryCondXBack  = BoundaryCondXBack::NonReflectCD;

  /// Types of X-front (right) boundary conditions.
  enum class BoundaryCondXFront {
    Periodic,
    NonReflect,
    NonReflectCD
  };

  /// X-front (right) boundary condition.
  const BoundaryCondXFront boundaryCondXFront = BoundaryCondXFront::NonReflectCD;

  /// Types of Y-back boundary conditions.
  enum class BoundaryCondYBack {
    Periodic,
    NonReflect,
    NonReflectCD,
  };

  /// Y-back boundary condition.
  const BoundaryCondYBack  boundaryCondYBack  = BoundaryCondYBack::Periodic;

  /// Types of Y-front boundary conditions.
  enum class BoundaryCondYFront {
    Periodic,
    NonReflect,
    //NonReflectCD,
  };

  /// Y-front boundary condition.
  const BoundaryCondYFront boundaryCondYFront = BoundaryCondYFront::Periodic;

  /// Are boundary condition in x-direction periodic?
  const bool isBoundaryCondXPeriodic =    boundaryCondXBack  == BoundaryCondXBack ::Periodic
                                       or boundaryCondXFront == BoundaryCondXFront::Periodic;

  /// Are boundary condition in y-direction periodic?
  const bool isBoundaryCondYPeriodic =    boundaryCondYBack  == BoundaryCondYBack ::Periodic
                                       or boundaryCondYFront == BoundaryCondYFront::Periodic;

} // namespace BC

//! Visualization parameters.
namespace Visualization {

  /// Wrap for GNU Plot.
  static GNUPlot plotter;

} // namespace Visualization

#endif // DEFINITIONS_HPP

