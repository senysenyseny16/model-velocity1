//: MeshEdge.inl

inline double MeshEdge::xB( const size_t var
                          , const size_t i
                          , const size_t j) const
{
  return meshXB(var, i, j);
}

inline double MeshEdge::xF( const size_t var
                          , const size_t i
                          , const size_t j) const
{
  return meshXF(var, i, j);
}

inline double MeshEdge::yB( const size_t var
                          , const size_t i
                          , const size_t j) const
{
  return meshYB(var, i, j);
}

inline double MeshEdge::yF( const size_t var
                          , const size_t i
                          , const size_t j) const
{
  return meshYF(var, i, j);
}

inline double& MeshEdge::xB( const size_t var
                           , const size_t i
                           , const size_t j)
{
  return meshXB(var, i, j);
}

inline double& MeshEdge::xF( const size_t var
                           , const size_t i
                           , const size_t j)
{
  return meshXF(var, i, j);
}

inline double& MeshEdge::yB( const size_t var
                           , const size_t i
                           , const size_t j)
{
  return meshYB(var, i, j);
}

inline double& MeshEdge::yF( const size_t var
                           , const size_t i
                           , const size_t j)
{
  return meshYF(var, i, j);
}

inline const Mesh& MeshEdge::dataXB() const
{
  return meshXB;
}

inline const Mesh& MeshEdge::dataXF() const
{
  return meshXF;
}

inline const Mesh& MeshEdge::dataYB() const
{
  return meshYB;
}

inline const Mesh& MeshEdge::dataYF() const
{
  return meshYF;
}

inline Mesh& MeshEdge::dataXB()
{
  return meshXB;

}
inline Mesh& MeshEdge::dataXF()
{
  return meshXF;
}

inline Mesh& MeshEdge::dataYB()
{
  return meshYB;
}

inline Mesh& MeshEdge::dataYF()
{
  return meshYF;
}

