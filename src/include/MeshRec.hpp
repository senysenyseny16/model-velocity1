//: MeshRec.hpp
// "Rec" from "Reconstruction".

#ifndef MESH_REC_HPP
#define MESH_REC_HPP

#include "MeshEdge.hpp"

/** @class MeshRec
 *  @brief Implement reconstruction of averaged physical variables
 *         to edges of finite volumes (cells).
 */
class MeshRec: public MeshEdge {
 public:

  /** Allocate memory for variable reconstruction. */
  MeshRec();

  /** Reconstruction orders. */
  enum class RecType {
      Constant
    , Weno3
    , Weno5
    , Muscl2
  };

  /** Reconstruct averaged physical variables to edges of finite volumes.
   *  @param source is a Mesh with averaged physical variables.
   */
  void reconstruct(const Mesh& source, RecType recType);
};

#endif // MESH_REC_HPP

