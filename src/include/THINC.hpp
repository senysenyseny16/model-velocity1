//: THINC.hpp

#ifndef THINC_HPP
#define THINC_HPP

#include "Definitions.hpp"

#include <utility>
#include <array>

class Mesh;
class MeshEdge;

namespace THINC {

typedef std::array<double, 11> VarVector;

std::pair<double, double> thinc( double qB
                               , double qC
                               , double qF
                               , double beta);

std::pair<double, double> thincCentered( double qB
                                       , double qC
                                       , double qF
                                       , double beta);

/** THINC reconstruction in one cell. */
std::pair<VarVector, VarVector> thinc( const Mesh& v
                                     , size_t i
                                     , size_t j
                                     , Geometry::Direction direction
                                     , double beta);

std::pair<VarVector, VarVector> weno( const Mesh& v
                                    , size_t i
                                    , size_t j
                                    , Geometry::Direction direction
                                    , THINC::RecType recType);

double tbv( double qwBF
          , double qwFB

          , double qtBF
          , double qtFB

          , double qCB
          , double qCF);

void bvd( const Mesh& v
        , MeshEdge& rec
        , THINC::RecType recType);

} // namespace THINC

#endif // THINC_HPP

