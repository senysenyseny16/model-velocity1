//: DebugOutput.hpp

#ifndef DEBUG_OUTPUT_HPP
#define DEBUG_OUTPUT_HPP

#include <cstddef>

namespace FluxNumerical { class MeshFluxNumerical; }
class Mesh;
class MeshFluxDifferential;

namespace DebugOutput {

void printFluxNum1DX( const FluxNumerical::MeshFluxNumerical& fN
                    , size_t j);

void printMesh1DX( const Mesh& m
                 , size_t j);

void printFluxDiff1DX( const MeshFluxDifferential& fD
                     , size_t j);

} // namespace DebugOutput

#endif // DEBUG_OUTPUT_HPP

