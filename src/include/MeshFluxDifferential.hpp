//: MeshFluxDifferential.hpp

#ifndef MESH_FLUX_DIFF_HPP
#define MESH_FLUX_DIFF_HPP

#include "MeshEdge.hpp"

/** @class MeshFluxDifferential
 *  @brief Storage for calculated differential flux on the cells edges.
 */
class MeshFluxDifferential: public MeshEdge {
 public:

  MeshFluxDifferential();

  /** Calculate fluxes on the cells edges.
   *
   *  @param rec is reconstruction of physical variables.
   */
  void calcFlux(const MeshEdge& rec);
};

#endif // MESH_FLUX_DIFF_HPP

