//: Visualization.hpp

#ifndef VISUALIZATION_HPP
#define VISUALIZATION_HPP

#include <cstddef>
#include <string>

class Mesh;
class MeshRec;
class MeshEdgeMutual1D;

namespace FluxNumerical { class MeshFluxNumerical; }

namespace Visualization {

/** Plot mesh with physical variables. */
void plotMeshPhysical(const Mesh& mesh);

/** Plot mesh with physical variables to file. */
void plotMeshPhysicalToFile( const Mesh& mesh
                           , const std::string filename);

/** Plot MeshEdgeMutual1D. */
void plotMeshEdgeMutual1D(const MeshEdgeMutual1D& meshEdgeMutual1D);

/** Plot mesh slice Y = j with physical variables. */
void plotMeshPhysical1DX( const Mesh& mesh
                        , size_t j);

/** Plot mesh slice X = i with physical variables. */
void plotMeshPhysical1DY( const Mesh& mesh
                        , size_t i);

/** Plot backward, forward reconstruction and source variable. */
void plotRecX( const MeshRec& rec
             , const Mesh& mesh
             , size_t var
             , size_t j);

/** Plot backward, forward reconstruction and source variable. */
void plotRecY( const MeshRec& rec
             , const Mesh& mesh
             , const size_t var
             , const size_t i);

/** Plot flux slice Y = j. */
void plotFlux1DX( const Mesh& mB
                , const Mesh& mF
                , size_t j);

/** Plot MeshFluxNumerical. */
void plotMeshFluxNumerical(const FluxNumerical::MeshFluxNumerical& fN);

void plotFlux1DX2( const FluxNumerical::MeshFluxNumerical& fN
                 , size_t j);

} // namespace Visualization

#endif // VISUALIZATION_HPP

