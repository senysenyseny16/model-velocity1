//: FluxNumerical.hpp

#ifndef FLUX_NUMERICAL_HPP
#define FLUX_NUMERICAL_HPP

namespace Flux {

/** Lax-Friedrichs numerical flux.
 *
 *  @param cB is conservative variable on the B (left)  edge of cell.
 *  @param cF is conservative variable on the F (right) edge of cell.
 *
 *  @param fB is flux across the B edge.
 *  @param fF is flux across the F edge.
 *
 *  @param K is LxF artificial viscosity coefficient (h / tau).
 */
double LF( double cB
         , double cF
         , double fB
         , double fF
         , double k);

double LFAlpha1( double alpha1B
               , double alpha1F
               , double u
               , double k);

/** Rusanov numerical flux.
 *
 *  @param cB is conservative variable on the B (left)  edge of cell.
 *  @param cF is conservative variable on the F (right) edge of cell.
 *
 *  @param fB is flux across the B edge.
 *  @param fF is flux across the F edge.
 *
 *  @param cmax is local maximum characteristic velocity.
 */
double LLF( double cB
          , double cF
          , double fB
          , double fF
          , double maxC);

double LLFAlpha1( double alpha1B
                , double alpha1F
                , double u
                , double maxC);

/** Calc U (conservative value) for Lax-Wendroff numerical flux.
 *
 *  @param cB is conservative variable on the B (left)  edge of cell.
 *  @param cF is conservative variable on the F (right) edge of cell.
 *
 *  @param fB is flux across the B edge.
 *  @param fF is flux across the F edge.
 *
 *  @param k = tau / h.
 */
double ULW( double cB
          , double cF
          , double fB
          , double fF
          , double k);

double LWAlpha1( double alpha1B
               , double alpha1F
               , double u
               , double k);

} // namespace Flux

#include "FluxNumerical.inl" // Inlined bodies.

#endif // FLUX_NUMERICAL_HPP

