// TotalStress.inl

namespace TotalStress {

inline double totalStress11( const double alpha1
                           , const double alpha2
                           , const double p1
                           , const double p2
                           , const double shearStress11)
{
  return alpha1 * shearStress11 - (alpha1 * p1 + alpha2 * p2);
}

inline double totalStress12( const double alpha1
                           , const double shearStress12)
{
  return alpha1 * shearStress12;
}

inline double totalStress21( const double alpha1
                           , const double shearStress21)
{
  return alpha1 * shearStress21;
}

inline double totalStress22( const double alpha1
                           , const double alpha2
                           , const double p1
                           , const double p2
                           , const double shearStress22)
{
  return alpha1 * shearStress22 - (alpha1 * p1 + alpha2 * p2);
}

} // namespace TotalStress

