//: PressureRelaxation.hpp

#ifndef PRESSURE_RELAXATION_HPP
#define PRESSURE_RELAXATION_HPP

class Mesh;

namespace PressureRelaxation {

/** Instant pressure relaxation.
 *
 *  Preserve mass of each phase (and total mass).
 */
double relaxPressure( double alpha1
                    , double rho1
                    , double rho2);

/** Instant pressure relaxation for each cell of Mesh. */
void relaxPressureMesh(Mesh& mesh);

} // namespace PressureRelaxation

#endif // PRESSURE_RELAXATION_HPP

