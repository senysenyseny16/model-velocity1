//: TotalStress.hpp

#ifndef TOTAL_STRESS_HPP
#define TOTAL_STRESS_HPP

namespace TotalStress {

double totalStress11( double alpha1
                    , double alpha2
                    , double p1
                    , double p2
                    , double shearStress11);

double totalStress12( double alpha1
                    , double shearStress12);

double totalStress21( double alpha1
                    , double shearStress21);

double totalStress22( double alpha1
                    , double alpha2
                    , double p1
                    , double p2
                    , double shearStress22);

} // namespace TotalStress

#include "TotalStress.inl" // Inlined bodies.

#endif // TOTAL_STRESS_HPP

