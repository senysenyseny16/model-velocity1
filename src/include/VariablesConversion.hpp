//: VariablesConversion.hpp

#ifndef VARIABLES_CONVERSION_HPP
#define VARIABLES_CONVERSION_HPP

/** VariablesConversion provide functions for convert
 *  primitive (physical) variables to conservative
 *  and vice versa.
 */
namespace VariablesConversion {

/** Main 11 primitive variables:
 *  alpha1 - volume fraction of elastic porous medium,
 *  rho2   - mass density of liquid,
 *  rho1   - mass density of elastic porous medium,
 *  u1    - velocity component along x axis of mixture,
 *  u2    - velocity component along x axis of mixture,
 *  and
 *  F11, F12, F21, F22 - components of elastic deformation gradient tensor.
 */

double alpha1(double U0);

double rho1(double U0, double U1);

double rho2(double U0, double U2);

double u1(double rho, double U3);

double u2(double rho, double U4);

double F11(double rho, double U5);

double F12(double rho, double U6);

double F21(double rho, double U7);

double F22(double rho, double U8);

/** Conservative variables U0,..., U10. **/

double U0(double alpha1);

double U1(double alpha1, double rho1);

double U2(double alpha2, double rho2);

double U3(double rho, double u1);

double U4(double rho, double u2);

double U5(double rho, double F11);

double U6(double rho, double F12);

double U7(double rho, double F21);

double U8(double rho, double F22);

} // namespace VariablesConversion

#endif // VARIABLES_CONVERSION_HPP

