//: HLLC.hpp

#ifndef HLLC_HPP
#define HLLC_HPP

namespace FHLLC {

inline double SM( const double rhoB
                , const double rhoF
                , const double uB
                , const double uF
                , const double totalStressB
                , const double totalStressF
                , const double SB
                , const double SF)
{
  return (rhoB * (uB * (uB - SB)) - rhoF * (uF * (uF - SF)) + (totalStressF - totalStressB)) / (rhoB * (uB - SB) - rhoF * (uF - SF));
}

} // namespace FHLLC

#endif // HLLC_HPP

