//: BoundaryConditionYFront.hpp

#ifndef BC_Y_FRONT_HPP
#define BC_Y_FRONT_HPP

namespace BC {

/* Calc hybrid (nonslip for skeleton and slip for liquid) boundary condition
 * on the y-front boundary.
 *
 * @param alpha1, rho, rho1, u, F - cell average.
 * @param xBalpha1, ... , xBF - primitive variables
 * on the y-front edge of the y-front cell (boundary condition).
 */
void slipHybridYFront( double alpha1
                     , double rho
                     , double rho1
                     , double u11
                     , double u12
                     , double u21
                     , double u22
                     , double F11
                     , double F12
                     , double F21
                     , double F22

                     , double& xBalpha1
                     , double& xBrho
                     , double& xBrho1
                     , double& xBu11
                     , double& xBu12
                     , double& xBu21
                     , double& xBu22
                     , double& xBF11
                     , double& xBF12
                     , double& xBF21
                     , double& xBF22);

} // namespace BC

#endif // BC_Y_FRONT_HPP

