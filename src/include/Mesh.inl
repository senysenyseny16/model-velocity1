//: Mesh.inl

#include "Definitions.hpp"

#include <cassert>

inline double Mesh::operator()(const size_t var, const size_t i, const size_t j) const
{
  // Check triple (var, i, j).
  assert(var < Physics::varCount);
  assert(i < Geometry::meshSizeX);
  assert(j < Geometry::meshSizeY);

  // Row-major order.
  return data[   j * Physics::varCount * Geometry::meshSizeX // shift by rows.
               + i * Physics::varCount                       // shift by cols.
               + var                                         // shift by var.
             ];
}

inline double& Mesh::operator()(const size_t var, const size_t i, const size_t j)
{
  // Check triple (var, i, j).
  assert(var < Physics::varCount);
  assert(i < Geometry::meshSizeX);
  assert(j < Geometry::meshSizeY);

  // Row-major order.
  return data[   j * Physics::varCount * Geometry::meshSizeX // shift by rows.
               + i * Physics::varCount                       // shift by cols.
               + var                                         // shift by var.
             ];
}

