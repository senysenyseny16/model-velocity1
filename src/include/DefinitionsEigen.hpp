//: DefinitionsEigen.hpp

#ifndef DEFINITIONS_EIGEN_HPP
#define DEFINITIONS_EIGEN_HPP

#include <Eigen/Dense>

namespace Eigen {

/* Useful definitions for Eigen library. */

typedef Eigen::Matrix<double, 9, 1> VectorCol9;
typedef Eigen::Matrix<double, 1, 9> VectorRow9;

typedef Eigen::Matrix<double, 9, 9> Matrix9;

typedef Eigen::EigenSolver<Matrix9> EigenSolver9;

typedef Eigen::Matrix<double, 3, 1> VectorCol3;
typedef Eigen::Matrix<double, 1, 3> VectorRow3;

typedef Eigen::Matrix<double, 3, 3> Matrix3;

typedef Eigen::EigenSolver<Matrix3> EigenSolver3;

} // namespace Eigen

#endif // DEFINITIONS_EIGEN_HPP

