//: MeshEdgeMutual.hpp

#ifndef MESH_EDGE_MUTUAL_HPP
#define MESH_EDGE_MUTUAL_HPP

#include "Mesh.hpp"

class MeshEdgeMutual {
 public:

  /** Ctor **/
  MeshEdgeMutual();

  virtual ~MeshEdgeMutual();

  /** Operator to access the elements of array (const).
   *  syntax: mesh(var, i, j).
   *
   *  @param var - variable number.
   *  @param i   - x-coordinate.
   *  @param j   - y-coordinate.
   */
  double edgeX(size_t var, size_t i, size_t j) const;
  double edgeY(size_t var, size_t i, size_t j) const;

  /** Operator to access the elements of array (by reference).
   *  syntax: mesh(var, i, j).
   *
   *  @param var - variable number.
   *  @param i   - x-coordinate.
   *  @param j   - y-coordinate.
   */
  double& edgeX(size_t var, size_t i, size_t j);
  double& edgeY(size_t var, size_t i, size_t j);

  /** Get value in backward x-direction. */
  double xB(size_t var, size_t i, size_t j) const;

  /** Get value in forward  x-direction. */
  double xF(size_t var, size_t i, size_t j) const;

  /** Get value in backward y-direction. */
  double yB(size_t var, size_t i, size_t j) const;

  /** Get value in forward  y-direction. */
  double yF(size_t var, size_t i, size_t j) const;

  /** Get value in backward x-direction. */
  double& xB(size_t var, size_t i, size_t j);

  /** Get value in forward  x-direction. */
  double& xF(size_t var, size_t i, size_t j);

  /** Get value in backward y-direction. */
  double& yB(size_t var, size_t i, size_t j);

  /** Get value in forward  y-direction. */
  double& yF(size_t var, size_t i, size_t j);

  Mesh& dataEdgesX();
  Mesh& dataEdgesY();

 protected:

  Mesh edgesX;
  Mesh edgesY;

};

#include "MeshEdgeMutual.inl" // Inlined bodies for xB, xF, yB, yF and data ref.

#endif // MESH_EDGE_MUTUAL_HPP

