//: BoundaryConditionXBack.hpp

#ifndef BC_X_BACK_HPP
#define BC_X_BACK_HPP

#include <cstddef>

class Mesh;
class MeshRec;

//! Namespace for boundary conditions.
namespace BC {

/* Calc inflow boundary condition on the x-back boundary.
 *
 * @param velPorous - velocity of porous medium that we want to set.
 * @param velLiquid - velocity of liquid that we want to set.
 */
void inflowXBack( const Mesh& primitive

                , size_t j

                , double velPorous
                , double velLiquid

                , MeshRec& rec);

void nonReflectXB( const Mesh& primitive
                 , size_t j
                 , MeshRec& rec);

} // namespace BC

#endif // BC_X_BACK_HPP

