//: MeshConversion.hpp

#ifndef MESH_CONVERSION_HPP
#define MESH_CONVERSION_HPP

#include "Definitions.hpp"

class Mesh;
class MeshEdge;

/** Conversion between primitive (physical)
 *  and conservative variables for all mesh (!except ghost cells).
 */
namespace MeshConversion {

/** Calc mesh with conservative variables from mesh with primitive variables. */
void toConservative( const Mesh& meshPrimitive
                   ,       Mesh& meshConservative);

/** Calc mesh with primitive variables from mesh with conservative variables. */
void toPrimitive( const Mesh& meshConservative
                ,       Mesh& meshPrimitive);

/** Convert primitive (physical) cell to conservative cell.
 *
 *  @param p is Mesh with primitive cells.
 *  @param c is destination (conservative cell) of conversion.
 *  @param i, j are coordinates of cell for conversion.
 */
void toConservative( const Mesh& p
                   , double (&c)[Physics::varCount]
                   , size_t i
                   , size_t j);


/** Convert conservative cell to primitive (physical) cell.
 *
 *  @param c is conservative cell.
 *  @param p is mesh with primitive cells.
 *  @param i, j are coordinates of cell in p for converted cell.
 */
void toPrimitive( const double (&c)[Physics::varCount]
                , Mesh& p
                , size_t i
                , size_t j);

/** Calc MeshEdge with conservative variables from MeshEdge with primitive variables. */
void toConservative( const MeshEdge& meshPrimitive
                   ,       MeshEdge& meshConservative);

} // namespace MeshConversion

#endif // MESH_CONVERSION_HPP

