//: MUSCL.hpp

#ifndef MUSCL_HPP
#define MUSCL_HPP

namespace MUSCL {

void MUSCL2( double  valB
           , double  valC
           , double  valF
           , double& recB
           , double& recF);

} // namespace MUSCL

#endif // MUSCL_HPP

