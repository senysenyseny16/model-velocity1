//: SchemeO2.hpp

#ifndef SCHEME_O2_HPP
#define SCHEME_O2_HPP

#include "Scheme.hpp"

/** Numerical scheme of second order accuracy. */
class SchemeO2: public Scheme {
 public:

  SchemeO2(const Mesh& initialData);

  /** Run second order numerical method. */
  void run() override;

  void info() const override;
};

#endif // SCHEME_O2_HPP

