//: GNUPlot.hpp

#ifndef GNU_PLOT_HPP
#define GNU_PLOT_HPP

#ifdef WIN32
    #define GNUPLOT_NAME "gnuplot -persist"
#else
    #define GNUPLOT_NAME "gnuplot -persist"
#endif

#include <cstdio>
#include <string>

/** @class GNUPlot
 *  @brief Wrap for GNU Plot.
 */
class GNUPlot {

 public:

  /** Create wrap for GNU Plot. */
  GNUPlot();
  ~GNUPlot();

  /** Redefinition of Operator ().
   *
   *  Example: gnuPlot("Plot me");
   *
   *  @param command which you want to send to GNU Plot.
   */
  void operator()(const std::string& command);

 protected:

  /// Stream.
  FILE* gnuPlotPipe;
};

#endif // GNU_PLOT_HPP

