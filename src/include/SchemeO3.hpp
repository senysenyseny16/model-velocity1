//: SchemeO3.hpp

#ifndef SCHEME_O3_HPP
#define SCHEME_O3_HPP

#include "Scheme.hpp"

/** Numerical scheme of third order accuracy. */
class SchemeO3: public Scheme {
 public:

  SchemeO3(const Mesh& initialData);

  /** Run third order numerical method. */
  void run() override;

  void info() const override;
};

#endif // SCHEME_O3_HPP

