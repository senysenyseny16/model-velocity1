//: ShearStress.hpp

#ifndef SHEAR_STRESS_HPP
#define SHEAR_STRESS_HPP

/** ShearStress provide functions to calc
 *  shear stresses sigma_{ij}.
 *
 *  Note! From symmetry follows sigma12 = sigma21.
 */
namespace ShearStress {

/// Shear stress in x-direction.
double shearStress11( double rho1
                    , double F11
                    , double F12
                    , double F21
                    , double F22);

/// Shear stress in xy-direction.
double shearStress12( double rho1
                    , double F11
                    , double F12
                    , double F21
                    , double F22);

/// From symmetry (sigma12 = sigma21).
double shearStress21( double rho1
                    , double F11
                    , double F12
                    , double F21
                    , double F22);

/// Shear stress in y-direction.
double shearStress22( double rho1
                    , double F11
                    , double F12
                    , double F21
                    , double F22);

double s11dRho1( double F11
               , double F12
               , double F21
               , double F22);

double s11dF11( double rho1
              , double F11
              , double F12
              , double F21
              , double F22);

double s11dF12( double rho1
              , double F11
              , double F12
              , double F21
              , double F22);

double s11dF21( double rho1
              , double F11
              , double F12
              , double F21
              , double F22);

double s11dF22( double rho1
              , double F11
              , double F12
              , double F21
              , double F22);

double s12dRho1( double F11
               , double F12
               , double F21
               , double F22);

double s12dF11( double rho1
              , double F11
              , double F12
              , double F21
              , double F22);

double s12dF12( double rho1
              , double F11
              , double F12
              , double F21
              , double F22);

double s12dF21( double rho1
              , double F11
              , double F12
              , double F21
              , double F22);

double s12dF22( double rho1
              , double F11
              , double F12
              , double F21
              , double F22);

} // namespace ShearStress

#endif // SHEAR_STRESS_HPP

