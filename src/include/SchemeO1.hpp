//: SchemeO1.hpp

#ifndef SCHEME_O1_HPP
#define SCHEME_O1_HPP

#include "Scheme.hpp"

/** Numerical scheme of first order accuracy. */
class SchemeO1: public Scheme {
 public:

  SchemeO1(const Mesh& initialData);

  /** Run second order numerical method. */
  void run() override;

  void info() const override;
};

#endif // SCHEME_O1_HPP

