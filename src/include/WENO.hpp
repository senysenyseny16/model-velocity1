//: WENO.hpp

#ifndef WENO_HPP
#define WENO_HPP

/** Weighted Essentially Non-Oscillatory reconstruction schemes.
 *
 *  @sa Essentially Non-Oscillatory and Weighted Essentially
 *      Non-Oscillatory Schemes for Hyperbolic Conservation Laws,
 *      NASA/CR-97-206253, Chu-Wang Shu.
 */
namespace WENO {

/** Weighted Essentially Non-Oscillatory reconstruction
 *  of 3rd order based on the stencils
 *  {x_{ i }, x_{i+1}},
 *  {x_{i-1}, x_{ i }}.
 *
 *  @param valB - value in x_{i-1} cell.
 *  @param valC - value in x_{ i } cell.
 *  @param valF - value in x_{i+1} cell.
 *
 *  @param recB - ref to destination of reconstructed value in {x-1/2}.
 *  @param recF - ref to destination of reconstructed value in {x+1/2}.
 */
void weno3( double  valB
          , double  valC
          , double  valF
          , double& recB
          , double& recF);

/** Weighted Essentially Non-Oscillatory reconstruction
 *  of 5th order based on the stencils
 *  {x_{ i }, x_{i+1}, x_{i+2}},
 *  {x_{i-1}, x_{ i }, x_{i+1}},
 *  {x_{i-2}, x_{i-1}, x_{ i }}.
 *
 *  @param valBB - value in x_{i-2} cell.
 *  @param valB  - value in x_{i-1} cell.
 *  @param valC  - value in x_{ i } cell.
 *  @param valF  - value in x_{i+1} cell.
 *  @param valFF - value in x_{i+2} cell.
 *
 *  @param recB - ref to destination of reconstructed value in {x-1/2}.
 *  @param recF - ref to destination of reconstructed value in {x+1/2}.
 */
void weno5( double  valBB
          , double  valB
          , double  valC
          , double  valF
          , double  valFF
          , double& recB
          , double& recF);

} // namespace WENO

#endif // WENO_HPP

