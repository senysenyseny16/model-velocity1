//: FluxDifferential.hpp

#ifndef FLUX_DIFFERENTIAL_HPP
#define FLUX_DIFFERENTIAL_HPP

#include <cstddef>
#include <cassert>

class Mesh;

/** Differential flux.
 *
 *  Additional variables:
 *  u1, u2,
 *  e1, e2,
 *  p1, p2,
 *  alpha2,
 *  sigma{ij}.
 */
namespace Flux {

/** Calculate differential flux in point (local) in x-direction.
 *
 *  @param p is mesh with reconstructed physical variables.
 *  @param i, j are coordiantes of point.
 *  @param fD is destination of calculated flux.
 */
void calcFluxDXLocal( const Mesh& p
                    , size_t i
                    , size_t j
                    , Mesh& fD);

/** Calculate differential flux in point (local) in y-direction.
 *
 *  @param p is mesh with reconstructed physical variables.
 *  @param i, j are coordiantes of point.
 *  @param fD is destination of calculated flux.
 */
void calcFluxDYLocal( const Mesh& p
                    , size_t i
                    , size_t j
                    , Mesh& fD);

double xF0( double alpha1
          , double u1);

double xF1( double alpha1
          , double rho1
          , double u1);

double xF2( double alpha2
          , double rho2
          , double u1);

double xF3( double rho
          , double u1
          , double totalStress11);

double xF4( double rho
          , double u1
          , double u2
          , double totalStress21);

double xF5( double rho
          , double u1
          , double F11);

double xF6( double rho
          , double u1
          , double F12);

double xF7( double rho
          , double u1
          , double F21);

double xF8( double rho
          , double u1
          , double F22);

double yF0( double alpha1
          , double u2);

double yF1( double alpha1
          , double rho1
          , double u2);

double yF2( double alpha2
          , double rho2
          , double u2);

double yF3( double rho
          , double u1
          , double u2
          , double totalStress12);

double yF4( double rho
          , double u2
          , double totalStress22);

double yF5( double rho
          , double u2
          , double F11);

double yF6( double rho
          , double u2
          , double F12);

double yF7( double rho
          , double u2
          , double F21);

double yF8( double rho
          , double u2
          , double F22);

} // namespace Flux

#include "FluxDifferential.inl" // Inlined bodies for flux.

#endif // FLUX_DIFFERENTIAL_HPP

