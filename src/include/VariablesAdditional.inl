//: VariablesAdditional.inl

namespace VariablesAdditional {

inline double alpha2(const double alpha1)
{
  assert(alpha1 > 0. and alpha1 < 1.);

  const double alpha2 = 1. - alpha1;

  assert(alpha2 > 0. and alpha2 < 1.);

  return alpha2;
}

inline double rho( const double alpha1
                 , const double alpha2
                 , const double rho1
                 , const double rho2)
{
  assert(alpha1 > 0. and alpha1 < 1.);
  assert(alpha2 > 0. and alpha2 < 1.);
  assert(rho1 > 0.);
  assert(rho2 > 0.);

  const double rho = alpha1 * rho1 + alpha2 * rho2;

  assert(rho > 0.);

  return rho;
}

inline double c1( const double alpha1
                , const double alpha2
                , const double rho1
                , const double rho2)
{
  const double c1 = (alpha1 * rho1) / rho(alpha1, alpha2, rho1, rho2);

  assert(c1 > 0. and c1 < 1.);

  return c1;
}

inline double c2( const double alpha1
                , const double alpha2
                , const double rho1
                , const double rho2)
{
  const double c2 = (alpha2 * rho2) / rho(alpha1, alpha2, rho1, rho2);

  assert(c2 > 0. and c2 < 1.);

  return c2;
}

} // namespace VariablesAdditional

