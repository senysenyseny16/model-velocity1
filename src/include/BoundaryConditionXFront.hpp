//: BoundaryConditionXFront.hpp

#ifndef BC_X_FRONT_HPP
#define BC_X_FRONT_HPP

#include <cstddef>

class Mesh;
class MeshRec;

//! Namespace for boundary conditions.
namespace BC {

/* Calc inflow boundary condition on the x-front boundary.
 *
 * @param alpha1, rho, rho1, u, F - cell average.
 * @param velPorous - velocity of porous medium that we want to set.
 * @param velLiquid - velocity of liquid that we want to set.
 * @param xBalpha1, ... , xBF - primitive variables
 * on the x-front edge of the x-front cell (boundary condition).
 */
void inflowXFront( double alpha1
                 , double rho
                 , double rho1
                 , double u11
                 , double u12
                 , double u21
                 , double u22
                 , double F11
                 , double F12
                 , double F21
                 , double F22

                 , double velPorous
                 , double velLiquid

                 , double& xF0
                 , double& xF1
                 , double& xF2
                 , double& xF3
                 , double& xF4
                 , double& xF5
                 , double& xF6
                 , double& xF7
                 , double& xF8
                 , double& xF9
                 , double& xF10);

void nonReflectXF( const Mesh& p
                 , size_t j
                 , MeshRec& rec);

} // namespace BC

#endif // BC_X_FRONT_HPP

