//: SchemeO3.cpp

#include "SchemeO3.hpp"

#include "MeshRec.hpp"
#include "MeshFluxDifferential.hpp"
#include "MeshFluxNumerical.hpp"

#include "MeshConversion.hpp"
#include "PressureRelaxation.hpp"
#include "THINC.hpp"
#include "BoundaryConditions.hpp"
#include "VariablesAdditional.hpp"

#include "Utility.hpp"
#include "Visualization.hpp"

#include <iostream>

SchemeO3::SchemeO3(const Mesh& initialData)
  : Scheme(initialData)
{}

void SchemeO3::run()
{
  /* using namespace SchemePars; */

  /* Mesh                    p1; */
  /* Mesh                    p2; */
  /* MeshRec                 rec;  // Reconstruction. */
  /* MeshEdge                recC; // Conservative vars on edges. */
  /* MeshFluxDifferential    fD;   // Differential flux. */

  /* std::unique_ptr<FluxNumerical::MeshFluxNumerical> pfN(FluxNumerical::createMeshFluxNumerical()); */
  /* std::unique_ptr<FluxNumerical::MeshNCFluxAlpha1>  pNCFAlpha1(FluxNumerical::createMeshNCFluxAlpha1()); */

  /* MeshRightSide           rs;   // Right side. */
  /* MeshMaxC                maxC; */

  /* // Temporary vectors for conservative cells. */
  /* double cVector0  [Physics::varCount]; */
  /* double cVector1  [Physics::varCount]; */
  /* double cVector2  [Physics::varCount]; */
  /* double cVectorNew[Physics::varCount]; */

  /* // Plot initial data. */
  /* //Visualization::plotMeshPhysical(p0); */
  /* Visualization::plotMeshPhysicalToFile(p0, std::to_string(iteration)); */
  /* //Visualization::plotMeshPhysical1DX(p0, Geometry::meshSizeY / 2); */
  /* //Visualization::plotMeshPhysical1DY(p0, Geometry::meshSizeX / 2); */

  /* // Time-loop. */
  /* for (timeCur = 0.; timeCur < Physics::timeEnd; timeCur += tau) */
  /* { */
  /*   // Part of boundary conditions. */
  /*   if (BC::isBoundaryCondXPeriodic) { */
  /*     Utility::glueMeshBoundariesX(p0); */
  /*   } else { */
  /*     Utility::fillMeshBoundariesX(p0); */
  /*   } */
  /*   if (BC::isBoundaryCondYPeriodic) { */
  /*     Utility::glueMeshBoundariesY(p0); */
  /*   } else { */
  /*     Utility::fillMeshBoundariesY(p0); */
  /*   } */

  /*   // Calculate reconstruction. */
  /*   rec.reconstruct(p0, MeshRec::RecOrder::Fifth); */
  /*   if (THINC::enableTHINC) THINC::bvd(p0, rec, THINC::WENORecOrder::Fifth); */

  /*   // Apply boundary conditions. */
  /*   BC::applyBoundaryConditions1(p0, rec); */

  /*   Utility::boundAlpha1(rec); */

  /*   // Part of boundary conditions. */
  /*   if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(rec); */
  /*   if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(rec); */

  /*   // Calculate differential flux. */
  /*   fD.calcFlux(rec); */

  /*   // Part of boundary conditions. */
  /*   if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(fD); */
  /*   if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(fD); */

  /*   // Calculate conservative variables. */
  /*   MeshConversion::toConservative(rec, recC); */

  /*   // Part of boundary conditions. */
  /*   if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(recC); */
  /*   if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(recC); */

  /*   // Calc local and global maximum of characteristics. */
  /*   maxC.calcMaxC(rec); */

  /*   // Recalc timestep. */
  /*   calcTau(maxC.maxCX(), maxC.maxCY()); */

  /*   // Calculate numerical flux. */
  /*   pfN->calcFlux(fD, recC, rec, p0); */

  /*   // Calculate nonconservative part for alpha1. */
  /*   pNCFAlpha1->calcFlux(rec, p0); */

  /*   // Apply boundary conditions. */
  /*   BC::applyBoundaryConditions2(p0, rec, *pfN, *pNCFAlpha1); */

  /*   // Calculate right side. */
  /*   rs.calcRightSide(p0); */

  /*   // Change tau for last iteration. */
  /*   if (Physics::timeEnd - timeCur < tau) { */
  /*     tau = Physics::timeEnd - timeCur; */
  /*   } */

  /*   const double kx = tau / hx; */
  /*   const double ky = tau / hy; */

  /*   // First step of RK-3 method. */
  /*   #pragma omp parallel for private(cVector0, cVectorNew) */
  /*   for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) { */
  /*     for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) { */

  /*       // Convert primitive cell to conservative cell cVector0. */
  /*       MeshConversion::toConservative(p0, cVector0, i, j); */

  /*       // Evolve alpha1. */
  /*       const double alpha1FluxConsPart    = (   (pfN->xF(0, i, j) - pfN->xB(0, i, j)) * kx */
  /*                                              + (pfN->yF(0, i, j) - pfN->yB(0, i, j)) * ky); */
  /*       const double alpha1FluxNonConsPart = (   (pNCFAlpha1->xF(i, j) - pNCFAlpha1->xB(i, j)) * kx */
  /*                                              + (pNCFAlpha1->yF(i, j) - pNCFAlpha1->yB(i, j)) * ky); */

  /*       cVectorNew[0] = cVector0[0] - (alpha1FluxConsPart + alpha1FluxNonConsPart); */
  /*       Utility::boundAlpha1(cVectorNew[0]); */

  /*       // Evaluate U0, U1, U2. */
  /*       for (size_t v = 1; v < 3; ++v) { */

  /*         const double flux =   (pfN->xF(v, i ,j) - pfN->xB(v, i, j)) * kx */
  /*                             + (pfN->yF(v, i, j) - pfN->yB(v, i, j)) * ky; */

  /*         cVectorNew[v] = cVector0[v] - flux; */
  /*       } */

  /*       // Implement implicit scheme for relative velocity equations (U3, U4). */
  /*       const double c1 = VariablesAdditional::c1( p0(0, i, j) */
  /*                                                , p0(2, i, j) */
  /*                                                , p0(1, i, j)); */
  /*       const double c2 = 1. - c1; */

  /*       for (size_t v = 3; v <= 4; ++v) { */

  /*         const double flux =   (pfN->xF(v, i ,j) - pfN->xB(v, i, j)) * kx */
  /*                             + (pfN->yF(v, i, j) - pfN->yB(v, i, j)) * ky; */

  /*         cVectorNew[v] = (   cVector0[v] */
  /*                           - flux */
  /*                           + rs(v, i, j) * tau */
  /*                         ) / (1. + tau * c1 * c2 * Physics::chi); */
  /*       } */

  /*       // Evaluate U5 - U10. */
  /*       for (size_t v = 5; v < Physics::varCount; ++v) { */

  /*         const double flux =   (pfN->xF(v, i ,j) - pfN->xB(v, i, j)) * kx */
  /*                             + (pfN->yF(v, i, j) - pfN->yB(v, i, j)) * ky; */

  /*         cVectorNew[v] = cVector0[v] - flux; */
  /*       } */
  /*       // Save new cell as primitive to p1. */
  /*       MeshConversion::toPrimitive(cVectorNew, p1, i, j); */
  /*     } */
  /*   } */

  /*   if (PressureRelaxation::enablePressureRelaxation) PressureRelaxation::relaxPressureMesh(p1); */

  /*   // Part of boundary conditions. */
  /*   if (BC::isBoundaryCondXPeriodic) { */
  /*     Utility::glueMeshBoundariesX(p1); */
  /*   } else { */
  /*     Utility::fillMeshBoundariesX(p1); */
  /*   } */
  /*   if (BC::isBoundaryCondYPeriodic) { */
  /*     Utility::glueMeshBoundariesY(p1); */
  /*   } else { */
  /*     Utility::fillMeshBoundariesY(p1); */
  /*   } */

  /*   // Calculate reconstruction. */
  /*   rec.reconstruct(p1, MeshRec::RecOrder::Fifth); */
  /*   if (THINC::enableTHINC) THINC::bvd(p1, rec, THINC::WENORecOrder::Fifth); */

  /*   // Apply boundary conditions. */
  /*   BC::applyBoundaryConditions1(p1, rec); */

  /*   Utility::boundAlpha1(rec); */

  /*   // Part of boundary conditions. */
  /*   if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(rec); */
  /*   if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(rec); */

  /*   // Calculate differential flux. */
  /*   fD.calcFlux(rec); */

  /*   // Part of boundary conditions. */
  /*   if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(fD); */
  /*   if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(fD); */

  /*   // Calculate conservative variables. */
  /*   MeshConversion::toConservative(rec, recC); */

  /*   // Part of boundary conditions. */
  /*   if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(recC); */
  /*   if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(recC); */

  /*   // Calcualte numerical flux. */
  /*   pfN->calcFlux(fD, recC, rec, p1); */

  /*   // Calculate nonconservative part for alpha1. */
  /*   pNCFAlpha1->calcFlux(rec, p1); */

  /*   // Apply boundary conditions. */
  /*   BC::applyBoundaryConditions2(p1, rec, *pfN, *pNCFAlpha1); */

  /*   // Calculate right side. */
  /*   rs.calcRightSide(p1); */

  /*   // Second step of RK-3 method. */
  /*   #pragma omp parallel for private(cVector0, cVector1, cVectorNew) */
  /*   for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) { */
  /*     for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) { */

  /*       // Convert primitive cells p0, p1 to conservative cells cVector0, cVector1. */
  /*       MeshConversion::toConservative(p0, cVector0, i, j); */
  /*       MeshConversion::toConservative(p1, cVector1, i, j); */

  /*       // Evolve alpha1. */
  /*       const double alpha1FluxConsPart    = (   (pfN->xF(0, i, j) - pfN->xB(0, i, j)) * kx */
  /*                                              + (pfN->yF(0, i, j) - pfN->yB(0, i, j)) * ky); */
  /*       const double alpha1FluxNonConsPart = (   (pNCFAlpha1->xF(i, j) - pNCFAlpha1->xB(i, j)) * kx */
  /*                                              + (pNCFAlpha1->yF(i, j) - pNCFAlpha1->yB(i, j)) * ky); */

  /*       cVectorNew[0] = cVector0[0] * 0.75 + (cVector1[0] - (alpha1FluxConsPart + alpha1FluxNonConsPart)) * 0.25; */
  /*       Utility::boundAlpha1(cVectorNew[0]); */

  /*       // Evalulate U0, U1, U2. */
  /*       for (size_t v = 1; v < 3; ++v) { */

  /*         const double flux =   (pfN->xF(v, i, j) - pfN->xB(v, i, j)) * kx */
  /*                             + (pfN->yF(v, i, j) - pfN->yB(v, i, j)) * ky; */

  /*         cVectorNew[v] =   cVector0[v] * 0.75 */
  /*                         + (   cVector1[v] */
  /*                             - flux */
  /*                           ) * 0.25; */
  /*       } */

  /*       // Implement implicit scheme for relative velocity equations (U3, U4). */
  /*       const double c1 = VariablesAdditional::c1( p1(0, i, j) */
  /*                                                , p1(2, i, j) */
  /*                                                , p1(1, i, j)); */
  /*       const double c2 = 1. - c1; */

  /*       for (size_t v = 3; v <= 4; ++v) { */

  /*         const double flux =   (pfN->xF(v, i, j) - pfN->xB(v, i, j)) * kx */
  /*                             + (pfN->yF(v, i, j) - pfN->yB(v, i, j)) * ky; */

  /*         cVectorNew[v] =    cVector0[v] * 0.75 */
  /*                          + ((   cVector1[v] */
  /*                               - flux */
  /*                               + rs(v, i, j) * tau */
  /*                             ) / (1. + tau * c1 * c2 * Physics::chi) */
  /*                            ) * 0.25; */
  /*       } */

  /*       // Evalulate U5 - U10. */
  /*       for (size_t v = 5; v < Physics::varCount; ++v) { */

  /*         const double flux =   (pfN->xF(v, i, j) - pfN->xB(v, i, j)) * kx */
  /*                             + (pfN->yF(v, i, j) - pfN->yB(v, i, j)) * ky; */

  /*         cVectorNew[v] =   cVector0[v] * 0.75 */
  /*                         + (   cVector1[v] */
  /*                             - flux */
  /*                           ) * 0.25; */
  /*       } */

  /*       // Save new cell as primitive to p2. */
  /*       MeshConversion::toPrimitive(cVectorNew, p2, i, j); */
  /*     } */
  /*   } */

  /*   if (PressureRelaxation::enablePressureRelaxation) PressureRelaxation::relaxPressureMesh(p2); */

  /*   // Part of boundary conditions. */
  /*   if (BC::isBoundaryCondXPeriodic) { */
  /*     Utility::glueMeshBoundariesX(p2); */
  /*   } else { */
  /*     Utility::fillMeshBoundariesX(p2); */
  /*   } */
  /*   if (BC::isBoundaryCondYPeriodic) { */
  /*     Utility::glueMeshBoundariesY(p2); */
  /*   } else { */
  /*     Utility::fillMeshBoundariesY(p2); */
  /*   } */

  /*   // Calculate reconstruction. */
  /*   rec.reconstruct(p2, MeshRec::RecOrder::Fifth); */
  /*   if (THINC::enableTHINC) THINC::bvd(p2, rec, THINC::WENORecOrder::Fifth); */

  /*   // Apply boundary conditions. */
  /*   BC::applyBoundaryConditions1(p2, rec); */

  /*   Utility::boundAlpha1(rec); */

  /*   // Part of boundary conditions. */
  /*   if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(rec); */
  /*   if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(rec); */

  /*   // Calculate differential flux. */
  /*   fD.calcFlux(rec); */

  /*   // Part of boundary conditions. */
  /*   if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(fD); */
  /*   if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(fD); */

  /*   // Calculate conservative variables. */
  /*   MeshConversion::toConservative(rec, recC); */

  /*   // Part of boundary conditions. */
  /*   if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(recC); */
  /*   if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(recC); */

  /*   // Calculate numerical flux. */
  /*   pfN->calcFlux(fD, recC, rec, p2); */

  /*   // Calculate nonconservative part for alpha1. */
  /*   pNCFAlpha1->calcFlux(rec, p2); */

  /*   // Apply boundary conditions. */
  /*   BC::applyBoundaryConditions2(p2, rec, *pfN, *pNCFAlpha1); */

  /*   // Calculate right side. */
  /*   rs.calcRightSide(p2); */

  /*   // Third step of RK-3 method. */
  /*   #pragma omp parallel for private(cVector0, cVector2, cVectorNew) */
  /*   for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) { */
  /*     for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) { */

  /*       // Convert primitive cells p0, p2 to conservative cells cVector0, cVector2. */
  /*       MeshConversion::toConservative(p0, cVector0, i, j); */
  /*       MeshConversion::toConservative(p2, cVector2, i, j); */

  /*       // Evolve alpha1. */
  /*       const double alpha1FluxConsPart    = (   (pfN->xF(0, i, j) - pfN->xB(0, i, j)) * kx */
  /*                                              + (pfN->yF(0, i, j) - pfN->yB(0, i, j)) * ky); */
  /*       const double alpha1FluxNonConsPart = (   (pNCFAlpha1->xF(i, j) - pNCFAlpha1->xB(i, j)) * kx */
  /*                                              + (pNCFAlpha1->yF(i, j) - pNCFAlpha1->yB(i, j)) * ky); */

  /*       cVectorNew[0] = cVector0[0] * (1. / 3.) + (cVector2[0] - (alpha1FluxConsPart + alpha1FluxNonConsPart)) * (2. / 3.); */
  /*       Utility::boundAlpha1(cVectorNew[0]); */

  /*       // Scheme step. */
  /*       // Evalulate U0, U1, U2. */
  /*       for (size_t v = 1; v < 3; ++v) { */

  /*         const double flux =   (pfN->xF(v, i, j) - pfN->xB(v, i, j)) * kx */
  /*                             + (pfN->yF(v, i, j) - pfN->yB(v, i, j)) * ky; */

  /*         cVectorNew[v] =   cVector0[v] * (1. / 3.) */
  /*                         + (   cVector2[v] */
  /*                             - flux */
  /*                           ) * (2. / 3.); */
  /*       } */

  /*       // Implement implicit scheme for relative velocity equations (U3, U4). */
  /*       const double c1 = VariablesAdditional::c1( p2(0, i, j) */
  /*                                                , p2(2, i, j) */
  /*                                                , p2(1, i, j)); */
  /*       const double c2 = 1. - c1; */

  /*       for (size_t v = 3; v <= 4; ++v) { */

  /*         const double flux =   (pfN->xF(v, i, j) - pfN->xB(v, i, j)) * kx */
  /*                             + (pfN->yF(v, i, j) - pfN->yB(v, i, j)) * ky; */

  /*         cVectorNew[v] =    cVector0[v] * (1. / 3.) */
  /*                          + ((   cVector2[v] */
  /*                               - flux */
  /*                               + rs(v, i, j) * tau */
  /*                             ) / (1. + tau * c1 * c2 * Physics::chi) */
  /*                            ) * (2. / 3.); */
  /*       } */

  /*       // Evalulate U5 - U10. */
  /*       for (size_t v = 5; v < Physics::varCount; ++v) { */

  /*         const double flux =   (pfN->xF(v, i, j) - pfN->xB(v, i, j)) * kx */
  /*                             + (pfN->yF(v, i, j) - pfN->yB(v, i, j)) * ky; */

  /*         cVectorNew[v] =   cVector0[v] * (1. / 3.) */
  /*                         + (   cVector2[v] */
  /*                             - flux */
  /*                           ) * (2. / 3.); */
  /*       } */

  /*       // Save new cell as primitive to p0. */
  /*       // p0 is solution. */
  /*       MeshConversion::toPrimitive(cVectorNew, p0, i, j); */
  /*     } */
  /*   } */

  /*   if (PressureRelaxation::enablePressureRelaxation) PressureRelaxation::relaxPressureMesh(p0); */

  /*   // Update iteration. */
  /*   ++iteration; */

  /*   // Update status and graps. */
  /*   if (iteration % statusInterval == 0) { */
  /*     status(); */

  /*     //Visualization::plotMeshPhysical(p0); */
  /*     Visualization::plotMeshPhysicalToFile(p0, std::to_string(iteration)); */
  /*     //Visualization::plotMeshPhysical1DX(p0, Geometry::meshSizeY / 2); */
  /*     //Visualization::plotMeshPhysical1DY(p0, Geometry::meshSizeX / 2); */
  /*   } */
  /* } */

  /* // Show status and solution. */
  /* status(); */
  /* //Visualization::plotMeshPhysical(p0); */
  /* Visualization::plotMeshPhysicalToFile(p0, std::to_string(iteration)); */
  /* //Visualization::plotMeshPhysical1DX(p0, Geometry::meshSizeY / 2); */
  /* //Visualization::plotMeshPhysical1DY(p0, Geometry::meshSizeX / 2); */

  /* // Save solution to file. */
  /* std::string filename(   "mesh-" */
  /*                       + std::to_string(Geometry::cellCountX) */
  /*                       + "x" */
  /*                       + std::to_string(Geometry::cellCountY) */
  /*                       + "-i" */
  /*                       + std::to_string(iteration) */
  /*                       + "-t" */
  /*                       + std::to_string(timeCur) */
  /*                       + ".dat"); */
  /* Utility::saveMeshToFile(p0, filename); */
}

void SchemeO3::info() const
{
  Scheme::info();

  std::cout << "Scheme: RK-3, WENO-5" << std::endl;
}

