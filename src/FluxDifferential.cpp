//: FluxDifferential.cpp

#include "FluxDifferential.hpp"
#include "Mesh.hpp"

#include "VariablesAdditional.hpp"
#include "Energy.hpp"
#include "ShearStress.hpp"
#include "TotalStress.hpp"

namespace Flux {

void calcFluxDXLocal( const Mesh& p
                    , const size_t i
                    , const size_t j
                    , Mesh& fD)
{
  // Unpack variables.
  const double alpha1 = p(0, i, j);
  const double rho1   = p(1, i, j);
  const double rho2   = p(2, i, j);
  const double u1     = p(3, i, j);
  const double u2     = p(4, i, j);
  const double F11    = p(5, i, j);
  const double F12    = p(6, i, j);
  const double F21    = p(7, i, j);
  const double F22    = p(8, i, j);

  // Calc additional variables.
  const double alpha2 = VariablesAdditional::alpha2(alpha1);
  const double rho    = VariablesAdditional::rho(alpha1, alpha2, rho1, rho2);

  const double p1 = Energy::p1(rho1);
  const double p2 = Energy::p2(rho2);

  const double shearStress11 = ShearStress::shearStress11(rho1, F11, F12, F21, F22);
  const double shearStress21 = ShearStress::shearStress21(rho1, F11, F12, F21, F22);

  const double totalStress11 = TotalStress::totalStress11(alpha1, alpha2, p1, p2, shearStress11);
  const double totalStress21 = TotalStress::totalStress21(alpha1, shearStress21);

  // Calc differential flux for pure conservative equations.
  fD(1,  i, j) = Flux::xF1(alpha1, rho1, u1);
  fD(2,  i, j) = Flux::xF2(alpha2, rho2, u1);
  fD(3,  i, j) = Flux::xF3(rho, u1,     totalStress11);
  fD(4,  i, j) = Flux::xF4(rho, u1, u2, totalStress21);
}

void calcFluxDYLocal( const Mesh& p
                    , const size_t i
                    , const size_t j
                    , Mesh& fD)
{
  // Unpack variables.
  const double alpha1 = p(0, i, j);
  const double rho1   = p(1, i, j);
  const double rho2   = p(2, i, j);
  const double u1     = p(3, i, j);
  const double u2     = p(4, i, j);
  const double F11    = p(5, i, j);
  const double F12    = p(6, i, j);
  const double F21    = p(7, i, j);
  const double F22    = p(8, i, j);

  // Calc additional variables.
  const double alpha2 = VariablesAdditional::alpha2(alpha1);
  const double rho    = VariablesAdditional::rho(alpha1, alpha2, rho1, rho2);

  const double p1 = Energy::p1(rho1);
  const double p2 = Energy::p2(rho2);

  const double shearStress12 = ShearStress::shearStress12(rho1, F11, F12, F21, F22);
  const double shearStress22 = ShearStress::shearStress22(rho1, F11, F12, F21, F22);

  const double totalStress12 = TotalStress::totalStress12(alpha1, shearStress12);
  const double totalStress22 = TotalStress::totalStress22(alpha1, alpha2, p1, p2, shearStress22);

  // Calc differential flux for pure conservative equations.
  fD(1,  i, j) = Flux::yF1(alpha1, rho1, u2);
  fD(2,  i, j) = Flux::yF2(alpha2, rho2, u2);
  fD(3,  i, j) = Flux::yF3(rho, u1, u2, totalStress12);
  fD(4,  i, j) = Flux::yF4(rho, u2,     totalStress22);
}

} // namespace Flux

