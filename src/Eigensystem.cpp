//: Eigensystem.cpp

#include "Eigensystem.hpp"
#include "Definitions.hpp"
#include "VariablesAdditional.hpp"
#include "ShearStress.hpp"
#include "Energy.hpp"

#include <iostream>

namespace Eigensystem {

std::pair<Eigen::VectorRow9, Eigen::Matrix9> eigensystemX( const double alpha1
                                                         , const double rho1
                                                         , const double rho2
                                                         , const double u1
                                                         , const double F11
                                                         , const double F12
                                                         , const double F21
                                                         , const double F22)
{
  using namespace Physics;

  Eigen::VectorRow9 eigenvalues;

  // First 5 eigenvalues are contact.
  eigenvalues(0) = u1;
  eigenvalues(1) = u1;
  eigenvalues(2) = u1;
  eigenvalues(3) = u1;
  eigenvalues(4) = u1;

  Eigen::Matrix9 lEigenvectors;

  // First 5 eigenvectors are contact.
  lEigenvectors(0, 0) = 0.;
  lEigenvectors(0, 1) = 0.;
  lEigenvectors(0, 2) = 0.;
  lEigenvectors(0, 3) = 0.;
  lEigenvectors(0, 4) = 0.;
  lEigenvectors(0, 5) = 0.;
  lEigenvectors(0, 6) = 0.;
  lEigenvectors(0, 7) = -F12;
  lEigenvectors(0, 8) = F11;

  lEigenvectors(1, 0) = 0.;
  lEigenvectors(1, 1) = F12;
  lEigenvectors(1, 2) = 0.;
  lEigenvectors(1, 3) = 0.;
  lEigenvectors(1, 4) = 0.;
  lEigenvectors(1, 5) = 0.;
  lEigenvectors(1, 6) = rho1;
  lEigenvectors(1, 7) = 0.;
  lEigenvectors(1, 8) = 0.;

  lEigenvectors(2, 0) = 0.;
  lEigenvectors(2, 1) = F11;
  lEigenvectors(2, 2) = 0.;
  lEigenvectors(2, 3) = 0.;
  lEigenvectors(2, 4) = 0.;
  lEigenvectors(2, 5) = rho1;
  lEigenvectors(2, 6) = 0.;
  lEigenvectors(2, 7) = 0.;
  lEigenvectors(2, 8) = 0.;

  lEigenvectors(3, 0) = 0.;
  lEigenvectors(3, 1) = -rho2;
  lEigenvectors(3, 2) = rho1;
  lEigenvectors(3, 3) = 0.;
  lEigenvectors(3, 4) = 0.;
  lEigenvectors(3, 5) = 0.;
  lEigenvectors(3, 6) = 0.;
  lEigenvectors(3, 7) = 0.;
  lEigenvectors(3, 8) = 0.;

  lEigenvectors(4, 0) = 1.;
  lEigenvectors(4, 1) = 0.;
  lEigenvectors(4, 2) = 0.;
  lEigenvectors(4, 3) = 0.;
  lEigenvectors(4, 4) = 0.;
  lEigenvectors(4, 5) = 0.;
  lEigenvectors(4, 6) = 0.;
  lEigenvectors(4, 7) = 0.;
  lEigenvectors(4, 8) = 0.;

  // Evaluate additional physical variables.
  const double alpha2 = VariablesAdditional::alpha2(alpha1);
  const double rho    = VariablesAdditional::rho(alpha1, alpha2, rho1, rho2);
  const double p1     = Energy::p1(rho1);
  const double p2     = Energy::p2(rho2);
  const double C12    = Energy::dp1(rho1);
  const double C22    = Energy::dp2(rho2);
  const double s11    = ShearStress::shearStress11(rho1, F11, F12, F21, F22);
  const double s12    = ShearStress::shearStress12(rho1, F11, F12, F21, F22);

  const double s11dRho1 = ShearStress::s11dRho1(     F11, F12, F21, F22);
  const double s11dF11  = ShearStress::s11dF11(rho1, F11, F12, F21, F22);
  const double s11dF12  = ShearStress::s11dF12(rho1, F11, F12, F21, F22);
  const double s11dF21  = ShearStress::s11dF21(rho1, F11, F12, F21, F22);
  const double s11dF22  = ShearStress::s11dF22(rho1, F11, F12, F21, F22);

  const double s12dRho1 = ShearStress::s12dRho1(     F11, F12, F21, F22);
  const double s12dF11  = ShearStress::s12dF11(rho1, F11, F12, F21, F22);
  const double s12dF12  = ShearStress::s12dF12(rho1, F11, F12, F21, F22);
  const double s12dF21  = ShearStress::s12dF21(rho1, F11, F12, F21, F22);
  const double s12dF22  = ShearStress::s12dF22(rho1, F11, F12, F21, F22);

  Eigen::Matrix9 AxT;

  AxT(0, 0) = u1;
  AxT(0, 1) = 0.;
  AxT(0, 2) = 0.;
  AxT(0, 3) = ((p1 - p2) - s11) / rho;
  AxT(0, 4) = (-s12) / rho;
  AxT(0, 5) = 0.;
  AxT(0, 6) = 0.;
  AxT(0, 7) = 0.;
  AxT(0, 8) = 0.;

  AxT(1, 0) = 0.;
  AxT(1, 1) = u1;
  AxT(1, 2) = 0.;
  AxT(1, 3) = (alpha1 * (C12 - s11dRho1)) / rho;
  AxT(1, 4) = (-alpha1 * s12dRho1) / rho;
  AxT(1, 5) = 0.;
  AxT(1, 6) = 0.;
  AxT(1, 7) = 0.;
  AxT(1, 8) = 0.;

  AxT(2, 0) = 0.;
  AxT(2, 1) = 0.;
  AxT(2, 2) = u1;
  AxT(2, 3) = (C22 * alpha2) / rho;
  AxT(2, 4) = 0.;
  AxT(2, 5) = 0.;
  AxT(2, 6) = 0.;
  AxT(2, 7) = 0.;
  AxT(2, 8) = 0.;

  AxT(3, 0) = 0.;
  AxT(3, 1) = rho1;
  AxT(3, 2) = rho2;
  AxT(3, 3) = u1;
  AxT(3, 4) = 0.;
  AxT(3, 5) = -F11;
  AxT(3, 6) = -F12;
  AxT(3, 7) = 0.;
  AxT(3, 8) = 0.;

  AxT(4, 0) = 0.;
  AxT(4, 1) = 0.;
  AxT(4, 2) = 0.;
  AxT(4, 3) = 0.;
  AxT(4, 4) = u1;
  AxT(4, 5) = 0.;
  AxT(4, 6) = 0.;
  AxT(4, 7) = -F11;
  AxT(4, 8) = -F12;

  AxT(5, 0) = 0.;
  AxT(5, 1) = 0.;
  AxT(5, 2) = 0.;
  AxT(5, 3) = (-alpha1 * s11dF11) / rho;
  AxT(5, 4) = (-alpha1 * s12dF11) / rho;
  AxT(5, 5) = u1;
  AxT(5, 6) = 0.;
  AxT(5, 7) = 0.;
  AxT(5, 8) = 0.;

  AxT(6, 0) = 0.;
  AxT(6, 1) = 0.;
  AxT(6, 2) = 0.;
  AxT(6, 3) = (-alpha1 * s11dF12) / rho;
  AxT(6, 4) = (-alpha1 * s12dF12) / rho;
  AxT(6, 5) = 0.;
  AxT(6, 6) = u1;
  AxT(6, 7) = 0.;
  AxT(6, 8) = 0.;

  AxT(7, 0) = 0.;
  AxT(7, 1) = 0.;
  AxT(7, 2) = 0.;
  AxT(7, 3) = (-alpha1 * s11dF21) / rho;
  AxT(7, 4) = (-alpha1 * s12dF21) / rho;
  AxT(7, 5) = 0.;
  AxT(7, 6) = 0.;
  AxT(7, 7) = u1;
  AxT(7, 8) = 0.;

  AxT(8, 0) = 0.;
  AxT(8, 1) = 0.;
  AxT(8, 2) = 0.;
  AxT(8, 3) = (-alpha1 * s11dF22) / rho;
  AxT(8, 4) = (-alpha1 * s12dF22) / rho;
  AxT(8, 5) = 0.;
  AxT(8, 6) = 0.;
  AxT(8, 7) = 0.;
  AxT(8, 8) = u1;

  // @TODO Check rank.

  return std::make_pair(eigenvalues, lEigenvectors);
}

std::pair<Eigen::VectorRow9, Eigen::Matrix9> eigensystemY( const double alpha1
                                                         , const double rho1
                                                         , const double rho2
                                                         , const double u1
                                                         , const double u2
                                                         , const double F11
                                                         , const double F12
                                                         , const double F21
                                                         , const double F22)
{
  using namespace Physics;

  const double s22dalpha1 = -p10+p20+((pow(F12,4)*(-2+pow(F21,4))-2*F11*F12*F21*(1+2*pow(F12,2)*pow(F21,2))*F22-pow(F12,2)*pow(F22,2)-4*pow(F11,3)*F12*F21*pow(F22,3)+pow(pow(F21,2)+pow(F22,2),2)+pow(F11,4)*(-2+pow(F22,4))+pow(F11,2)*(-pow(F21,2)+pow(F12,2)*(-4+6*pow(F21,2)*pow(F22,2))))*mu*rho1)/(6.*pow(pow(F12*F21-F11*F22,-2),0.6666666666666666)*pow(F12*F21-F11*F22,4)*rho10)+(pow(C1,2)*rho10)/gamma1-(pow(C1,2)*pow(rho1/rho10,gamma1)*rho10)/gamma1-(pow(C2,2)*rho20)/gamma2+(pow(C2,2)*pow(rho2/rho20,gamma2)*rho20)/gamma2;
  const double s22drho1   = -(alpha1*pow(C1,2)*pow(rho1/rho10,-1+gamma1))+(alpha1*(pow(F12,4)*(-2+pow(F21,4))-2*F11*F12*F21*(1+2*pow(F12,2)*pow(F21,2))*F22-pow(F12,2)*pow(F22,2)-4*pow(F11,3)*F12*F21*pow(F22,3)+pow(pow(F21,2)+pow(F22,2),2)+pow(F11,4)*(-2+pow(F22,4))+pow(F11,2)*(-pow(F21,2)+pow(F12,2)*(-4+6*pow(F21,2)*pow(F22,2))))*mu)/(6.*pow(pow(F12*F21-F11*F22,-2),0.6666666666666666)*pow(F12*F21-F11*F22,4)*rho10);
  const double s22drho2   = (-1+alpha1)*pow(C2,2)*pow(rho2/rho20,-1+gamma2);
  const double s22dF11    = -(alpha1*(4*pow(F11,3)*F12*F21*(3-2*pow(F22,4))+2*pow(F11,4)*F22*(-2+pow(F22,4))+F22*(2*pow(F12,4)*(4+pow(F21,4))-4*pow(pow(F21,2)+pow(F22,2),2)+pow(F12,2)*(3*pow(F21,2)+4*pow(F22,2)))+F11*F12*F21*(3*pow(F21,2)+5*pow(F22,2)+pow(F12,2)*(12-8*pow(F21,2)*pow(F22,2)))+pow(F11,2)*F22*(pow(F21,2)+4*pow(F12,2)*(1+3*pow(F21,2)*pow(F22,2))))*mu*rho1)/(9.*pow(pow(F12*F21-F11*F22,-2),0.6666666666666666)*pow(F12*F21-F11*F22,5)*rho10);
  const double s22dF12    = (alpha1*(F11*F12*F22*(5*pow(F21,2)+pow(F12,2)*(12-8*pow(F21,4))+3*pow(F22,2))+4*pow(F11,3)*F12*F22*(3-2*pow(F21,2)*pow(F22,2))+pow(F11,2)*F21*(4*(pow(F12,2)+pow(F21,2))+3*(1+4*pow(F12,2)*pow(F21,2))*pow(F22,2))+2*pow(F11,4)*F21*(4+pow(F22,4))+F21*(2*pow(F12,4)*(-2+pow(F21,4))+pow(F12,2)*pow(F22,2)-4*pow(pow(F21,2)+pow(F22,2),2)))*mu*rho1)/(9.*pow(pow(F12*F21-F11*F22,-2),0.6666666666666666)*pow(F12*F21-F11*F22,5)*rho10);
  const double s22dF21    = (alpha1*(F12*(8*pow(pow(F11,2)+pow(F12,2),2)+pow(F11,2)*pow(F21,2)+2*(1+pow(F12,4))*pow(F21,4))+F11*F21*(3*pow(F11,2)+5*pow(F12,2)-2*(3+4*pow(F12,4))*pow(F21,2))*F22+F12*(4*pow(F12,2)-2*pow(F21,2)+3*pow(F11,2)*(1+4*pow(F12,2)*pow(F21,2)))*pow(F22,2)-2*F11*(3+4*pow(F11,2)*pow(F12,2))*F21*pow(F22,3)+2*(-2+pow(F11,4))*F12*pow(F22,4))*mu*rho1)/(9.*pow(pow(F12*F21-F11*F22,-2),0.6666666666666666)*pow(F12*F21-F11*F22,5)*rho10);
  const double s22dF22    = -(alpha1*(F11*(8*pow(pow(F11,2)+pow(F12,2),2)+(4*pow(F11,2)+3*pow(F12,2))*pow(F21,2)+2*(-2+pow(F12,4))*pow(F21,4))+F12*F21*(5*pow(F11,2)+3*pow(F12,2)-2*(3+4*pow(F11,2)*pow(F12,2))*pow(F21,2))*F22+F11*(pow(F12,2)+2*(-1+6*pow(F11,2)*pow(F12,2))*pow(F21,2))*pow(F22,2)-2*(3+4*pow(F11,4))*F12*F21*pow(F22,3)+2*F11*(1+pow(F11,4))*pow(F22,4))*mu*rho1)/(9.*pow(pow(F12*F21-F11*F22,-2),0.6666666666666666)*pow(F12*F21-F11*F22,5)*rho10);

  const double s12dalpha1 = (pow(pow(F12*F21-F11*F22,-2),1.3333333333333333)*(F11*F21+F12*F22)*(pow(F11,2)+pow(F12,2)+pow(F21,2)+pow(F22,2))*mu*rho1)/(2.*rho10);
  const double s12drho1   = (alpha1*pow(pow(F12*F21-F11*F22,-2),1.3333333333333333)*(F11*F21+F12*F22)*(pow(F11,2)+pow(F12,2)+pow(F21,2)+pow(F22,2))*mu)/(2.*rho10);
  const double s12dF11    = (alpha1*pow(pow(F12*F21-F11*F22,-2),0.3333333333333333)*(-(pow(F11,3)*F21*F22)+pow(F11,2)*F12*(9*pow(F21,2)+2*pow(F22,2))+F12*(pow(F12,2)+pow(F21,2)+pow(F22,2))*(3*pow(F21,2)+8*pow(F22,2))+F11*F21*F22*(11*pow(F12,2)+5*(pow(F21,2)+pow(F22,2))))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho10);
  const double s12dF12    = (alpha1*pow(pow(F12*F21-F11*F22,-2),0.3333333333333333)*(-2*F11*pow(F21,2)*(4*pow(F11,2)+pow(F12,2)+4*pow(F21,2))+F12*F21*(-11*pow(F11,2)+pow(F12,2)-5*pow(F21,2))*F22-F11*(3*pow(F11,2)+9*pow(F12,2)+11*pow(F21,2))*pow(F22,2)-5*F12*F21*pow(F22,3)-3*F11*pow(F22,4))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho10);
  const double s12dF21    = -(alpha1*pow(pow(F12*F21-F11*F22,-2),0.3333333333333333)*(5*pow(F11,3)*F12*F21+3*pow(F11,4)*F22+pow(F11,2)*F22*(11*pow(F12,2)+9*pow(F21,2)+3*pow(F22,2))+2*pow(F12,2)*F22*(4*pow(F12,2)+pow(F21,2)+4*pow(F22,2))+F11*F12*F21*(5*pow(F12,2)-pow(F21,2)+11*pow(F22,2)))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho10);
  const double s12dF22    = (alpha1*pow(pow(F12*F21-F11*F22,-2),0.3333333333333333)*((8*pow(F11,2)+3*pow(F12,2))*F21*(pow(F11,2)+pow(F12,2)+pow(F21,2))+F11*F12*(5*(pow(F11,2)+pow(F12,2))+11*pow(F21,2))*F22+(2*pow(F11,2)+9*pow(F12,2))*F21*pow(F22,2)-F11*F12*pow(F22,3))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho10);

  Eigen::VectorRow9 eigenvalues;

  eigenvalues(0) = u2;
  eigenvalues(1) = u2;
  eigenvalues(2) = u2;
  eigenvalues(3) = u2;
  eigenvalues(4) = u2;

  // u2 - ?
  eigenvalues(5) = -(sqrt((alpha1*rho1-(-1+alpha1)*rho2)*(F21*(s12dF11+s22dF21)+F22*(s12dF12+s22dF22)-rho1*s22drho1-rho2*s22drho2+sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2))))/(sqrt(2)*(alpha1*(rho1-rho2)+rho2)))+u2;
  // u2 + ?
  eigenvalues(6) = sqrt((alpha1*rho1-(-1+alpha1)*rho2)*(F21*(s12dF11+s22dF21)+F22*(s12dF12+s22dF22)-rho1*s22drho1-rho2*s22drho2+sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2))))/(sqrt(2)*(alpha1*(rho1-rho2)+rho2))+u2;
  // u2 - ?
  eigenvalues(7) = -(sqrt((-(alpha1*(rho1-rho2))-rho2)*(-(F21*(s12dF11+s22dF21))-F22*(s12dF12+s22dF22)+rho1*s22drho1+rho2*s22drho2+sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2))))/(sqrt(2)*(alpha1*(rho1-rho2)+rho2)))+u2;
  // u2 + ?
  eigenvalues(8) = sqrt((-(alpha1*(rho1-rho2))-rho2)*(-(F21*(s12dF11+s22dF21))-F22*(s12dF12+s22dF22)+rho1*s22drho1+rho2*s22drho2+sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2))))/(sqrt(2)*(alpha1*(rho1-rho2)+rho2))+u2;

  Eigen::Matrix9 lEigenvectors;

  lEigenvectors(0, 0) = 0.;
  lEigenvectors(0, 1) = F22;
  lEigenvectors(0, 2) = 0.;
  lEigenvectors(0, 3) = 0.;
  lEigenvectors(0, 4) = 0.;
  lEigenvectors(0, 5) = 0.;
  lEigenvectors(0, 6) = 0.;
  lEigenvectors(0, 7) = 0.;
  lEigenvectors(0, 8) = rho1;

  lEigenvectors(1, 0) = 0.;
  lEigenvectors(1, 1) = F21;
  lEigenvectors(1, 2) = 0.;
  lEigenvectors(1, 3) = 0.;
  lEigenvectors(1, 4) = 0.;
  lEigenvectors(1, 5) = 0.;
  lEigenvectors(1, 6) = 0.;
  lEigenvectors(1, 7) = rho1;
  lEigenvectors(1, 8) = 0.;

  lEigenvectors(2, 0) = 0.;
  lEigenvectors(2, 1) = 0.;
  lEigenvectors(2, 2) = 0.;
  lEigenvectors(2, 3) = 0.;
  lEigenvectors(2, 4) = 0.;
  lEigenvectors(2, 5) = -F22;
  lEigenvectors(2, 6) = F21;
  lEigenvectors(2, 7) = 0.;
  lEigenvectors(2, 8) = 0.;

  lEigenvectors(3, 0) = 0.;
  lEigenvectors(3, 1) = -rho2;
  lEigenvectors(3, 2) = rho1;
  lEigenvectors(3, 3) = 0.;
  lEigenvectors(3, 4) = 0.;
  lEigenvectors(3, 5) = 0.;
  lEigenvectors(3, 6) = 0.;
  lEigenvectors(3, 7) = 0.;
  lEigenvectors(3, 8) = 0.;

  lEigenvectors(4, 0) = 1.;
  lEigenvectors(4, 1) = 0.;
  lEigenvectors(4, 2) = 0.;
  lEigenvectors(4, 3) = 0.;
  lEigenvectors(4, 4) = 0.;
  lEigenvectors(4, 5) = 0.;
  lEigenvectors(4, 6) = 0.;
  lEigenvectors(4, 7) = 0.;
  lEigenvectors(4, 8) = 0.;

  lEigenvectors(5, 0) = sqrt(2)*(F21*s12dalpha1*s12dF11+2*F21*s12dF21*s22dalpha1-2*rho1*s12drho1*s22dalpha1-F21*s12dalpha1*s22dF21+F22*(2*s12dF22*s22dalpha1+s12dalpha1*(s12dF12-s22dF22))+rho1*s12dalpha1*s22drho1+rho2*s12dalpha1*s22drho2+s12dalpha1*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(5, 1) = sqrt(2)*(F21*s12dF11*s12drho1-F21*s12drho1*s22dF21+2*F21*s12dF21*s22drho1-rho1*s12drho1*s22drho1+F22*(s12drho1*(s12dF12-s22dF22)+2*s12dF22*s22drho1)+rho2*s12drho1*s22drho2+s12drho1*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(5, 2) = 2*sqrt(2)*(F21*s12dF21+F22*s12dF22-rho1*s12drho1)*s22drho2;
  lEigenvectors(5, 3) = sqrt((alpha1*rho1-(-1+alpha1)*rho2)*(F21*(s12dF11+s22dF21)+F22*(s12dF12+s22dF22)-rho1*s22drho1-rho2*s22drho2+sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2))))*(F21*(s12dF11-s22dF21)+F22*(s12dF12-s22dF22)+rho1*s22drho1+rho2*s22drho2+sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(5, 4) = 2*(F21*s12dF21+F22*s12dF22-rho1*s12drho1)*sqrt((alpha1*rho1-(-1+alpha1)*rho2)*(F21*(s12dF11+s22dF21)+F22*(s12dF12+s22dF22)-rho1*s22drho1-rho2*s22drho2+sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2))));
  lEigenvectors(5, 5) = sqrt(2)*(F22*s12dF11*s12dF12+2*F22*s12dF22*s22dF11-2*rho1*s12drho1*s22dF11+F21*(pow(s12dF11,2)+2*s12dF21*s22dF11-s12dF11*s22dF21)-F22*s12dF11*s22dF22+rho1*s12dF11*s22drho1+rho2*s12dF11*s22drho2+s12dF11*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(5, 6) = sqrt(2)*(F21*s12dF11*s12dF12+2*F21*s12dF21*s22dF12-2*rho1*s12drho1*s22dF12-F21*s12dF12*s22dF21+F22*(pow(s12dF12,2)+2*s12dF22*s22dF12-s12dF12*s22dF22)+rho1*s12dF12*s22drho1+rho2*s12dF12*s22drho2+s12dF12*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(5, 7) = sqrt(2)*(F22*s12dF12*s12dF21+2*F22*s12dF22*s22dF21-2*rho1*s12drho1*s22dF21+F21*s12dF21*(s12dF11+s22dF21)-F22*s12dF21*s22dF22+rho1*s12dF21*s22drho1+rho2*s12dF21*s22drho2+s12dF21*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(5, 8) = sqrt(2)*(F21*s12dF11*s12dF22-F21*s12dF22*s22dF21+2*F21*s12dF21*s22dF22-2*rho1*s12drho1*s22dF22+F22*s12dF22*(s12dF12+s22dF22)+rho1*s12dF22*s22drho1+rho2*s12dF22*s22drho2+s12dF22*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));

  lEigenvectors(6, 0) = sqrt(2)*(F21*s12dalpha1*s12dF11+2*F21*s12dF21*s22dalpha1-2*rho1*s12drho1*s22dalpha1-F21*s12dalpha1*s22dF21+F22*(2*s12dF22*s22dalpha1+s12dalpha1*(s12dF12-s22dF22))+rho1*s12dalpha1*s22drho1+rho2*s12dalpha1*s22drho2+s12dalpha1*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(6, 1) = sqrt(2)*(F21*s12dF11*s12drho1-F21*s12drho1*s22dF21+2*F21*s12dF21*s22drho1-rho1*s12drho1*s22drho1+F22*(s12drho1*(s12dF12-s22dF22)+2*s12dF22*s22drho1)+rho2*s12drho1*s22drho2+s12drho1*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(6, 2) = 2*sqrt(2)*(F21*s12dF21+F22*s12dF22-rho1*s12drho1)*s22drho2;
  lEigenvectors(6, 3) = -(sqrt((alpha1*rho1-(-1+alpha1)*rho2)*(F21*(s12dF11+s22dF21)+F22*(s12dF12+s22dF22)-rho1*s22drho1-rho2*s22drho2+sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2))))*(F21*(s12dF11-s22dF21)+F22*(s12dF12-s22dF22)+rho1*s22drho1+rho2*s22drho2+sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2))));
  lEigenvectors(6, 4) = -2*(F21*s12dF21+F22*s12dF22-rho1*s12drho1)*sqrt((alpha1*rho1-(-1+alpha1)*rho2)*(F21*(s12dF11+s22dF21)+F22*(s12dF12+s22dF22)-rho1*s22drho1-rho2*s22drho2+sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2))));
  lEigenvectors(6, 5) = sqrt(2)*(F22*s12dF11*s12dF12+2*F22*s12dF22*s22dF11-2*rho1*s12drho1*s22dF11+F21*(pow(s12dF11,2)+2*s12dF21*s22dF11-s12dF11*s22dF21)-F22*s12dF11*s22dF22+rho1*s12dF11*s22drho1+rho2*s12dF11*s22drho2+s12dF11*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(6, 6) = sqrt(2)*(F21*s12dF11*s12dF12+2*F21*s12dF21*s22dF12-2*rho1*s12drho1*s22dF12-F21*s12dF12*s22dF21+F22*(pow(s12dF12,2)+2*s12dF22*s22dF12-s12dF12*s22dF22)+rho1*s12dF12*s22drho1+rho2*s12dF12*s22drho2+s12dF12*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(6, 7) = sqrt(2)*(F22*s12dF12*s12dF21+2*F22*s12dF22*s22dF21-2*rho1*s12drho1*s22dF21+F21*s12dF21*(s12dF11+s22dF21)-F22*s12dF21*s22dF22+rho1*s12dF21*s22drho1+rho2*s12dF21*s22drho2+s12dF21*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(6, 8) = sqrt(2)*(F21*s12dF11*s12dF22-F21*s12dF22*s22dF21+2*F21*s12dF21*s22dF22-2*rho1*s12drho1*s22dF22+F22*s12dF22*(s12dF12+s22dF22)+rho1*s12dF22*s22drho1+rho2*s12dF22*s22drho2+s12dF22*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));

  lEigenvectors(7, 0) = sqrt(2)*(F21*s12dalpha1*s12dF11+2*F21*s12dF21*s22dalpha1-2*rho1*s12drho1*s22dalpha1-F21*s12dalpha1*s22dF21+F22*(2*s12dF22*s22dalpha1+s12dalpha1*(s12dF12-s22dF22))+rho1*s12dalpha1*s22drho1+rho2*s12dalpha1*s22drho2-s12dalpha1*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(7, 1) = sqrt(2)*(F21*s12dF11*s12drho1-F21*s12drho1*s22dF21+2*F21*s12dF21*s22drho1-rho1*s12drho1*s22drho1+F22*(s12drho1*(s12dF12-s22dF22)+2*s12dF22*s22drho1)+rho2*s12drho1*s22drho2-s12drho1*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(7, 2) = 2*sqrt(2)*(F21*s12dF21+F22*s12dF22-rho1*s12drho1)*s22drho2;
  lEigenvectors(7, 3) = (F21*(s12dF11-s22dF21)+F22*(s12dF12-s22dF22)+rho1*s22drho1+rho2*s22drho2-sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)))*sqrt((-(alpha1*(rho1-rho2))-rho2)*(-(F21*(s12dF11+s22dF21))-F22*(s12dF12+s22dF22)+rho1*s22drho1+rho2*s22drho2+sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2))));
  lEigenvectors(7, 4) = 2*(F21*s12dF21+F22*s12dF22-rho1*s12drho1)*sqrt((-(alpha1*(rho1-rho2))-rho2)*(-(F21*(s12dF11+s22dF21))-F22*(s12dF12+s22dF22)+rho1*s22drho1+rho2*s22drho2+sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2))));
  lEigenvectors(7, 5) = sqrt(2)*(F22*s12dF11*s12dF12+2*F22*s12dF22*s22dF11-2*rho1*s12drho1*s22dF11+F21*(pow(s12dF11,2)+2*s12dF21*s22dF11-s12dF11*s22dF21)-F22*s12dF11*s22dF22+rho1*s12dF11*s22drho1+rho2*s12dF11*s22drho2-s12dF11*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(7, 6) = sqrt(2)*(F21*s12dF11*s12dF12+2*F21*s12dF21*s22dF12-2*rho1*s12drho1*s22dF12-F21*s12dF12*s22dF21+F22*(pow(s12dF12,2)+2*s12dF22*s22dF12-s12dF12*s22dF22)+rho1*s12dF12*s22drho1+rho2*s12dF12*s22drho2-s12dF12*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(7, 7) = sqrt(2)*(F22*s12dF12*s12dF21+2*F22*s12dF22*s22dF21-2*rho1*s12drho1*s22dF21+F21*s12dF21*(s12dF11+s22dF21)-F22*s12dF21*s22dF22+rho1*s12dF21*s22drho1+rho2*s12dF21*s22drho2-s12dF21*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(7, 8) = sqrt(2)*(F21*s12dF11*s12dF22-F21*s12dF22*s22dF21+2*F21*s12dF21*s22dF22-2*rho1*s12drho1*s22dF22+F22*s12dF22*(s12dF12+s22dF22)+rho1*s12dF22*s22drho1+rho2*s12dF22*s22drho2-s12dF22*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));

  lEigenvectors(8, 0) = sqrt(2)*(F21*s12dalpha1*s12dF11+2*F21*s12dF21*s22dalpha1-2*rho1*s12drho1*s22dalpha1-F21*s12dalpha1*s22dF21+F22*(2*s12dF22*s22dalpha1+s12dalpha1*(s12dF12-s22dF22))+rho1*s12dalpha1*s22drho1+rho2*s12dalpha1*s22drho2-s12dalpha1*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(8, 1) = sqrt(2)*(F21*s12dF11*s12drho1-F21*s12drho1*s22dF21+2*F21*s12dF21*s22drho1-rho1*s12drho1*s22drho1+F22*(s12drho1*(s12dF12-s22dF22)+2*s12dF22*s22drho1)+rho2*s12drho1*s22drho2-s12drho1*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(8, 2) = 2*sqrt(2)*(F21*s12dF21+F22*s12dF22-rho1*s12drho1)*s22drho2;
  lEigenvectors(8, 3) = (F21*(-s12dF11+s22dF21)+F22*(-s12dF12+s22dF22)-rho1*s22drho1-rho2*s22drho2+sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)))*sqrt((-(alpha1*(rho1-rho2))-rho2)*(-(F21*(s12dF11+s22dF21))-F22*(s12dF12+s22dF22)+rho1*s22drho1+rho2*s22drho2+sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2))));
  lEigenvectors(8, 4) = -2*(F21*s12dF21+F22*s12dF22-rho1*s12drho1)*sqrt((-(alpha1*(rho1-rho2))-rho2)*(-(F21*(s12dF11+s22dF21))-F22*(s12dF12+s22dF22)+rho1*s22drho1+rho2*s22drho2+sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2))));
  lEigenvectors(8, 5) = sqrt(2)*(F22*s12dF11*s12dF12+2*F22*s12dF22*s22dF11-2*rho1*s12drho1*s22dF11+F21*(pow(s12dF11,2)+2*s12dF21*s22dF11-s12dF11*s22dF21)-F22*s12dF11*s22dF22+rho1*s12dF11*s22drho1+rho2*s12dF11*s22drho2-s12dF11*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(8, 6) = sqrt(2)*(F21*s12dF11*s12dF12+2*F21*s12dF21*s22dF12-2*rho1*s12drho1*s22dF12-F21*s12dF12*s22dF21+F22*(pow(s12dF12,2)+2*s12dF22*s22dF12-s12dF12*s22dF22)+rho1*s12dF12*s22drho1+rho2*s12dF12*s22drho2-s12dF12*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(8, 7) = sqrt(2)*(F22*s12dF12*s12dF21+2*F22*s12dF22*s22dF21-2*rho1*s12drho1*s22dF21+F21*s12dF21*(s12dF11+s22dF21)-F22*s12dF21*s22dF22+rho1*s12dF21*s22drho1+rho2*s12dF21*s22drho2-s12dF21*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));
  lEigenvectors(8, 8) = sqrt(2)*(F21*s12dF11*s12dF22-F21*s12dF22*s22dF21+2*F21*s12dF21*s22dF22-2*rho1*s12drho1*s22dF22+F22*s12dF22*(s12dF12+s22dF22)+rho1*s12dF22*s22drho1+rho2*s12dF22*s22drho2-s12dF22*sqrt(pow(F21,2)*(4*s12dF21*s22dF11+pow(s12dF11-s22dF21,2))+pow(F22,2)*(4*s12dF22*s22dF12+pow(s12dF12-s22dF22,2))+pow(rho1*s22drho1+rho2*s22drho2,2)+2*F21*(rho1*(-2*s12drho1*s22dF11+(s12dF11-s22dF21)*s22drho1)+rho2*(s12dF11-s22dF21)*s22drho2)+2*F22*(F21*(2*(s12dF22*s22dF11+s12dF21*s22dF12)+(s12dF11-s22dF21)*(s12dF12-s22dF22))+rho1*(-2*s12drho1*s22dF12+(s12dF12-s22dF22)*s22drho1)+rho2*(s12dF12-s22dF22)*s22drho2)));

  // @TODO Check rank.

  return std::make_pair(eigenvalues, lEigenvectors);
}

} // namespace Eigensystem

