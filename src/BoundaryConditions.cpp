//: BoundaryConditions.cpp

#include "BoundaryConditions.hpp"
#include "Definitions.hpp"
#include "Mesh.hpp"
#include "MeshRec.hpp"
#include "MeshFluxNumerical.hpp"

#include "BoundaryConditionXBack.hpp"
#include "BoundaryConditionXFront.hpp"
#include "BoundaryConditionYBack.hpp"
#include "BoundaryConditionYFront.hpp"

#include "VariablesAdditional.hpp"
#include "FluxDifferential.hpp"
#include "Energy.hpp"
#include "ShearStress.hpp"
#include "TotalStress.hpp"

#include <cmath>

namespace BC {

void applyBoundaryConditions1( const Mesh& p
                             , MeshRec&    rec)
{
  // Implement x-back (left) boundary condition.
  switch (BC::boundaryCondXBack) {

  case (BC::BoundaryCondXBack::Periodic): break;
  case (BC::BoundaryCondXBack::NonReflect): break;

  case (BC::BoundaryCondXBack::NonReflectCD):
    for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
      BC::nonReflectXB(p, j, rec);
    }
  break;
  }

  // Implement x-front (right) boundary condition.
  switch (BC::boundaryCondXFront) {

  case (BC::BoundaryCondXFront::Periodic): break;

  case (BC::BoundaryCondXFront::NonReflect): break;

  case (BC::BoundaryCondXFront::NonReflectCD):
    for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
      BC::nonReflectXF(p, j, rec);
    }
  break;
  }

  // Implement y-back boundary condition.
  switch (BC::boundaryCondYBack) {

  case (BC::BoundaryCondYBack::Periodic): break;

  case (BC::BoundaryCondYBack::NonReflect): break;

  case (BC::BoundaryCondYBack::NonReflectCD):
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
      BC::nonReflectYB(p, i, rec);
    }
  break;
  }

  // Implement y-front boundary condition.
  switch (BC::boundaryCondYFront) {

  case (BC::BoundaryCondYFront::Periodic): break;

  case (BC::BoundaryCondYFront::NonReflect): break;
  }
}

void calcFluxByRecX( const Mesh& rec
                   , Mesh& fNC
                   , Mesh& fNnC
                   , const size_t iRec
                   , const size_t ifN
                   , const size_t j)
{
  const double alpha1 = rec(0, iRec, j);
  const double rho1   = rec(1, iRec, j);
  const double rho2   = rec(2, iRec, j);
  const double u1     = rec(3, iRec, j);
  const double u2     = rec(4, iRec, j);
  const double F11    = rec(5, iRec, j);
  const double F12    = rec(6, iRec, j);
  const double F21    = rec(7, iRec, j);
  const double F22    = rec(8, iRec, j);

  // Calc additional variables.
  const double alpha2 = VariablesAdditional::alpha2(alpha1);
  const double rho    = VariablesAdditional::rho(alpha1, alpha2, rho1, rho2);

  const double p1 = Energy::p1(rho1);
  const double p2 = Energy::p2(rho2);

  const double shearStress11 = ShearStress::shearStress11(rho1, F11, F12, F21, F22);
  const double shearStress21 = ShearStress::shearStress21(rho1, F11, F12, F21, F22);

  const double totalStress11 = TotalStress::totalStress11(alpha1, alpha2, p1, p2, shearStress11);
  const double totalStress21 = TotalStress::totalStress21(alpha1, shearStress21);

  // Calc differential flux for pure conservative equations.
  fNC(0, ifN, j) = Flux::xF0(alpha1, u1);
  fNC(1, ifN, j) = Flux::xF1(alpha1, rho1, u1);
  fNC(2, ifN, j) = Flux::xF2(alpha2, rho2, u1);
  fNC(3, ifN, j) = Flux::xF3(rho, u1,     totalStress11);
  fNC(4, ifN, j) = Flux::xF4(rho, u1, u2, totalStress21);
  fNC(5, ifN, j) = Flux::xF5(rho, u1, F11);
  fNC(6, ifN, j) = Flux::xF6(rho, u1, F12);
  fNC(7, ifN, j) = Flux::xF7(rho, u1, F21);
  fNC(8, ifN, j) = Flux::xF8(rho, u1, F22);

  fNnC(0, ifN, j) = u1;
  fNnC(5, ifN, j) = u1;
  fNnC(6, ifN, j) = u1;
  fNnC(7, ifN, j) = u2;
  fNnC(8, ifN, j) = u2;
}

void calcFluxByRecY( const Mesh& rec
                   , Mesh& fNC
                   , Mesh& fNnC
                   , const size_t i
                   , const size_t jRec
                   , const size_t jfN)
{
  const double alpha1 = rec(0, i, jRec);
  const double rho1   = rec(1, i, jRec);
  const double rho2   = rec(2, i, jRec);
  const double u1     = rec(3, i, jRec);
  const double u2     = rec(4, i, jRec);
  const double F11    = rec(5, i, jRec);
  const double F12    = rec(6, i, jRec);
  const double F21    = rec(7, i, jRec);
  const double F22    = rec(8, i, jRec);

  // Calc additional variables.
  const double alpha2 = VariablesAdditional::alpha2(alpha1);
  const double rho    = VariablesAdditional::rho(alpha1, alpha2, rho1, rho2);

  const double p1 = Energy::p1(rho1);
  const double p2 = Energy::p2(rho2);

  const double shearStress22 = ShearStress::shearStress22(rho1, F11, F12, F21, F22);
  const double shearStress12 = ShearStress::shearStress12(rho1, F11, F12, F21, F22);

  const double totalStress22 = TotalStress::totalStress22(alpha1, alpha2, p1, p2, shearStress22);
  const double totalStress12 = TotalStress::totalStress12(alpha1, shearStress12);

  // Calc differential flux for pure conservative equations.
  fNC(0, i, jfN) = Flux::yF0(alpha1, u2);
  fNC(1, i, jfN) = Flux::yF1(alpha1, rho1, u2);
  fNC(2, i, jfN) = Flux::yF2(alpha2, rho2, u2);
  fNC(3, i, jfN) = Flux::yF3(rho, u1, u2, totalStress12);
  fNC(4, i, jfN) = Flux::yF4(rho,     u2, totalStress22);
  fNC(5, i, jfN) = Flux::yF5(rho, u2, F11);
  fNC(6, i, jfN) = Flux::yF6(rho, u2, F12);
  fNC(7, i, jfN) = Flux::yF7(rho, u2, F21);
  fNC(8, i, jfN) = Flux::yF8(rho, u2, F22);

  fNnC(0, i, jfN) = u2;
  fNnC(5, i, jfN) = u1;
  fNnC(6, i, jfN) = u1;
  fNnC(7, i, jfN) = u2;
  fNnC(8, i, jfN) = u2;
}

void applyBoundaryConditions2( const Mesh& p
                             , const MeshRec& rec
                             , FluxNumerical::MeshFluxNumerical& fluxNum)
{

  // Implement x-back (left) boundary condition.
  switch (BC::boundaryCondXBack) {

  case (BC::BoundaryCondXBack::Periodic): break;

  case (BC::BoundaryCondXBack::NonReflect):

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t var = 0; var < Physics::varCount; ++var) {
      fluxNum.c.xB(var, Geometry::firstCell, j) = fluxNum.c.xF(var, Geometry::firstCell, j);
    }
  }

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    fluxNum.nC.xB(0, Geometry::firstCell, j) = fluxNum.nC.xF(0, Geometry::firstCell, j);
  }
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t var = 5; var < Physics::varCount; ++var) {
      fluxNum.nC.xB(var, Geometry::firstCell, j) = fluxNum.nC.xF(var, Geometry::firstCell, j);
    }
  }
  break;

  case (BC::BoundaryCondXBack::NonReflectCD):
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    calcFluxByRecX( rec.dataXB()
                  , fluxNum.c .dataEdgesX()
                  , fluxNum.nC.dataEdgesX()
                  , Geometry::firstCell
                  , Geometry::firstCell
                  , j);
  }
  break;
  }

  // Implement x-front (right) boundary condition.
  switch (BC::boundaryCondXFront) {

  case (BC::BoundaryCondXFront::Periodic): break;

  case (BC::BoundaryCondXFront::NonReflect):

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t var = 0; var < Physics::varCount; ++var) {
      fluxNum.c.xF(var, Geometry::lastCellX, j) = fluxNum.c.xB(var, Geometry::lastCellX, j);
    }
  }

  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    fluxNum.nC.xF(0, Geometry::lastCellX, j) = fluxNum.nC.xB(0, Geometry::lastCellX, j);
  }
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t var = 5; var < Physics::varCount; ++var) {
      fluxNum.nC.xF(var, Geometry::lastCellX, j) = fluxNum.nC.xB(var, Geometry::lastCellX, j);
    }
  }
  break;

  case (BC::BoundaryCondXFront::NonReflectCD):
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    calcFluxByRecX( rec.dataXF()
                  , fluxNum.c .dataEdgesX()
                  , fluxNum.nC.dataEdgesX()
                  , Geometry::lastCellX
                  , Geometry::lastCellX + 1
                  , j);
  }
  break;
  }

  // Implement y-back boundary condition.
  switch (BC::boundaryCondYBack) {

  case (BC::BoundaryCondYBack::Periodic): break;

  case (BC::BoundaryCondYBack::NonReflect):

  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
    for (size_t var = 0; var < Physics::varCount; ++var) {
      fluxNum.c.yB(var, i, Geometry::firstCell) = fluxNum.c.yF(var, i, Geometry::firstCell);
    }
  }

  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
    fluxNum.nC.yB(0, i, Geometry::firstCell) = fluxNum.nC.yF(0, i, Geometry::firstCell);
  }
  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
    for (size_t var = 5; var < Physics::varCount; ++var) {
      fluxNum.nC.yB(var, i, Geometry::firstCell) = fluxNum.nC.yF(var, i, Geometry::firstCell);
    }
  }
  break;

  case (BC::BoundaryCondYBack::NonReflectCD):
  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
    calcFluxByRecY( rec.dataYB()
                  , fluxNum.c .dataEdgesY()
                  , fluxNum.nC.dataEdgesY()
                  , i
                  , Geometry::firstCell
                  , Geometry::firstCell);
  }
  break;
  }

  // Implement y-front boundary condition.
  switch (BC::boundaryCondYFront) {

  case (BC::BoundaryCondYFront::Periodic): break;

  case (BC::BoundaryCondYFront::NonReflect):

  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
    for (size_t var = 0; var < Physics::varCount; ++var) {
      fluxNum.c.yF(var, i, Geometry::lastCellY) = fluxNum.c.yB(var, i, Geometry::lastCellY);
    }
  }

  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
    fluxNum.nC.yF(0, i, Geometry::lastCellY) = fluxNum.nC.yB(0, i, Geometry::lastCellY);
  }
  for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {
    for (size_t var = 5; var < Physics::varCount; ++var) {
      fluxNum.nC.yF(var, i, Geometry::lastCellY) = fluxNum.nC.yB(var, i, Geometry::lastCellY);
    }
  }
  break;
  }
}

} // namespace BC

