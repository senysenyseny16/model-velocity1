//: DebugOutput.cpp

#include "DebugOutput.hpp"

#include "Definitions.hpp"
#include "MeshFluxNumerical.hpp"
#include "MeshFluxDifferential.hpp"

#include "Utility.hpp"

#include <iostream>

namespace DebugOutput {

void printFluxNum1DX( const FluxNumerical::MeshFluxNumerical& fN
                 , const size_t j)
{
  std::cout << "FluxNum:" << std::endl;
  std::cout << "-----------------" << std::endl;

  for (size_t var = 0; var < Physics::varCount; ++var) {
    std::cout << "F" << var << ":" << std::endl;
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      // Calculate 1D flux.
      const double fluxConsPart    = fN.c .xF(var, i, j) - fN.c .xB(var, i, j);
      const double fluxNonConsPart = fN.nC.xF(var, i, j) - fN.nC.xB(var, i, j);
      const double flux = fluxConsPart + fluxNonConsPart;

      std::cout << Utility::toStringWithPrecision(flux, 50) << std::endl;
    }
    std::cout << std::endl;
  }
  std::cout << "-----------------" << std::endl;
}

void printMesh1DX( const Mesh& m
                 , const size_t j)
{

  std::cout << "Mesh:" << std::endl;
  std::cout << "-----------------" << std::endl;

  for (size_t var = 0; var < Physics::varCount; ++var) {
    std::cout << "Var" << var << ":" << std::endl;
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      std::cout << Utility::toStringWithPrecision(m(var, i, j), 50) << std::endl;

    }
    std::cout << std::endl;
  }
  std::cout << "-----------------" << std::endl;
}

void printFluxDiff1DX( const MeshFluxDifferential& fD
                     , const size_t j)
{
  std::cout << "FluxDiff:" << std::endl;
  std::cout << "-----------------" << std::endl;

  for (size_t var = 0; var < Physics::varCount; ++var) {
    std::cout << "F" << var << ":" << std::endl;
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      // Calculate 1D flux.
      const double fluxB = fD.xB(var, i, j);
      const double fluxF = fD.xF(var, i, j);

      std::cout << Utility::toStringWithPrecision(fluxB, 25) << "\t"
                << Utility::toStringWithPrecision(fluxF, 25) << std::endl;
    }
    std::cout << std::endl;
  }
  std::cout << "-----------------" << std::endl;
}

} // namespace DebugOutput

