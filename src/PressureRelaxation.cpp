//: PressureRelaxation.cpp

#include "PressureRelaxation.hpp"
#include "Definitions.hpp"
#include "Energy.hpp"
#include "VariablesAdditional.hpp"
#include "Mesh.hpp"

#include "Utility.hpp"

#include <cassert>
#include <cmath>
#include <limits>

#include <iostream>

namespace PressureRelaxation {

double relaxPressure( double alpha1
                    , double rho1
                    , double rho2)
{
  using namespace std;

  const double inputAlpha1 = alpha1;

  assert(isnormal(alpha1));
  assert(isnormal(rho1));
  assert(isnormal(rho2));

  // Mass of phase in the cell must remain unchanged.
  // theta1 represents partial density of porous medium.
  const double theta1 = alpha1 * rho1;
  // theta2 represents partial density of liquid.
  const double theta2 = (1. - alpha1) * rho2;

  assert(theta1 > 0.);
  assert(theta2 > 0.);

  // Variables for iteration method.
  // Differentials of pressure.
  double dp1 = 0.;
  double dp2 = 0.;

  double alpha1New = alpha1;
  double alpha2    = 1. - alpha1;

  double fn  = 0.;
  double dfn = 0.;

  size_t iteration = 0;
  bool isFailedNewton = false;

  // Try Newton method.
  do {
    alpha1 = alpha1New;
    alpha2 = 1. - alpha1;

    assert(isnormal(alpha1));
    assert(isnormal(alpha2));

    // Recalc phasic densities.
    rho1 = theta1 / alpha1;
    rho2 = theta2 / alpha2;

    assert(isnormal(rho1));
    assert(isnormal(rho2));
    assert(rho1 > 0.);
    assert(rho2 > 0.);

    // We want to find the root of f = p2 - p1.
    const double p1 = Energy::p1(rho1);
    const double p2 = Energy::p2(rho2);

    assert(isfinite(p1));
    assert(isfinite(p2));

    fn = p2 - p1;
    assert(isfinite(fn));

    if (std::abs(fn) <= PressureRelaxation::pressureRelaxationTol) {
      return alpha1New;
    }

    // Calc differentials of p1 and p2.
    dp1 = Energy::dp1(rho1);
    dp2 = Energy::dp2(rho2);

    assert(isnormal(dp1));
    assert(isnormal(dp2));

    // Calc differential of f;
    dfn = dp2 * theta2 / (alpha2 * alpha2) + dp1 * theta1 / (alpha1 * alpha1);
    assert(isnormal(dfn));

    // Update alpha1.
    alpha1New = alpha1New - fn / dfn;

    // Check solution.
    if (alpha1New <= 0. or alpha1New >= 1. or (not isnormal(alpha1New))) {
      isFailedNewton = true;
      break;
    }

    ++iteration;

  } while (std::abs(fn) > pressureRelaxationTol and iteration < PressureRelaxation::maxIterationNewton);

  if (std::abs(fn) > pressureRelaxationTol) {
    isFailedNewton = true;
  } else {
    Utility::boundAlpha1(alpha1New);
    return alpha1New;
  }

  // If Newton method diverges,
  // then try bisection method.
  if (isFailedNewton) {

    double a = SchemePars::alpha1Eps;      // left bound.
    double b = 1. - SchemePars::alpha1Eps; // right bound.

    double rho1a = theta1 / a;
    double rho1b = theta1 / b;

    assert(isnormal(rho1a));
    assert(isnormal(rho1b));

    double rho2a = theta2 / (1. - a);
    double rho2b = theta2 / (1. - b);

    assert(isnormal(rho2a));
    assert(isnormal(rho2b));

    double fna = Energy::p2(rho2a) - Energy::p1(rho1a);
    const double fnb = Energy::p2(rho2b) - Energy::p1(rho1b);

    assert(isfinite(fna));
    assert(isfinite(fnb));

    if (Utility::sgn(fna) == Utility::sgn(fnb)) {
      // No solution.
      return inputAlpha1;
    }

    // Check boundaries.
    if (std::abs(fna) < PressureRelaxation::pressureRelaxationTol) {
      alpha1New = a;

      Utility::boundAlpha1(alpha1New);
      return alpha1New;
    }

    if (std::abs(fnb) < PressureRelaxation::pressureRelaxationTol) {
      alpha1New = b;

      Utility::boundAlpha1(alpha1New);
      return alpha1New;
    }

    double t = (a + b) / 2.;
    double fnt = 0.;
    double rho1t = 0.;
    double rho2t = 0.;

    do {

      t = (a + b) / 2.;

      rho1a = theta1 / a;
      rho2a = theta2 / (1. - a);

      assert(isnormal(rho1a));
      assert(isnormal(rho2a));
      assert(rho1a > 0.);
      assert(rho2a > 0.);

      rho1t = theta1 / t;
      rho2t = theta2 / (1. - t);

      assert(isnormal(rho1t));
      assert(isnormal(rho2t));
      assert(rho1t > 0.);
      assert(rho2t > 0.);

      fna = Energy::p2(rho2a) - Energy::p1(rho1a);
      fnt = Energy::p2(rho2t) - Energy::p1(rho1t);

      assert(isfinite(fna));
      assert(isfinite(fnt));

      if (std::abs(fnt) < PressureRelaxation::pressureRelaxationTol) {
          alpha1New = t;

          Utility::boundAlpha1(alpha1New);
          return alpha1New;
      }

      if (Utility::sgn(fna) != Utility::sgn(fnt)) {
        b = t; // root is on the left.
      } else {
        a = t; // root is on the right.
      }

      iteration++;
    } while (std::abs(fnt) > PressureRelaxation::pressureRelaxationTol and iteration < PressureRelaxation::maxIterationBisection);

      alpha1New = (a + b) / 2.;

      Utility::boundAlpha1(alpha1New);
      return alpha1New;
  }

  // Shit happends.
  return inputAlpha1;
}

void relaxPressureMesh(Mesh& mesh)
{
  #pragma omp parallel for
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      // theta1 = alpha1 * rho1.
      const double theta1 = mesh(0, i, j) * mesh(2, i, j);
      // theta2 = alpha2 * rho2
      const double theta2 = (1. - mesh(0, i, j)) * mesh(1, i, j);

      // Correct alpha1.
      mesh(0, i, j) = relaxPressure( mesh(0, i, j) // alpha1
                                   , mesh(2, i, j) // rho1
                                   , mesh(1, i, j) // rho2
                                   );

      // Correct rho1 and rho2.
      mesh(2, i, j) = theta1 / mesh(0, i, j);
      mesh(1, i, j) = theta2 / (1. - mesh(0, i, j));
    }
  }
}

} // namespace PressureRelaxation

