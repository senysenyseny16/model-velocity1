//: MUSCL.cpp

#include "MUSCL.hpp"

#include <cmath>

namespace MUSCL {

double minmod(const double a, const double b)
{
  const double modA = std::abs(a);
  const double modB = std::abs(b);
  const double mulAb = a * b;

  if (modA < modB and mulAb > 0.) {
    return a;
  } else if (modB < modA and mulAb > 0.) {
    return b;
  } else {
    return 0.;
  }
}

void MUSCL2( const double  valB
           , const double  valC
           , const double  valF
           , double& recB
           , double& recF)
{
  const double sigma = minmod(valC - valB, valF - valC);

  recB = valC - sigma / 2.;
  recF = valC + sigma / 2.;
}

}
