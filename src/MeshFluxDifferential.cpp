//: MeshFluxDifferential.cpp

#include "MeshFluxDifferential.hpp"
#include "MeshRec.hpp"
#include "FluxDifferential.hpp"
#include "Definitions.hpp"

MeshFluxDifferential::MeshFluxDifferential()
  : MeshEdge()
{}

void MeshFluxDifferential::calcFlux(const MeshEdge& rec)
{
  #pragma omp parallel for
  for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
    for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

      // Calc flux on the edges of cell(i,j).
      Flux::calcFluxDXLocal(rec.dataXB(), i, j, meshXB);
      Flux::calcFluxDXLocal(rec.dataXF(), i, j, meshXF);
      Flux::calcFluxDYLocal(rec.dataYB(), i, j, meshYB);
      Flux::calcFluxDYLocal(rec.dataYF(), i, j, meshYF);

    }
  }
}

