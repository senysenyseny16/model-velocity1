//: InterfaceNormal.cpp

#include "InterfaceNormal.hpp"

#include <cmath>

namespace InterfaceNormal {

void interfaceNormalPY( const double fxByB
                      , const double fxCyB
                      , const double fxFyB
                      , const double fxByC
                      , const double fxFyC
                      , const double fxByF
                      , const double fxCyF
                      , const double fxFyF
                      , double& fx
                      , double& fy)
{
  /*  We assume that alpha = 2 in Parker and Young's method.
   *
   * +-------+-------+-------+------> x
   * |       |       |       |
   * | fxByB | fxCyB | fxFyB |
   * |       |       |       |
   * +-------+-------+-------+
   * |       |       |       |
   * | fxByC |   x   | fxFyC |
   * |       |       |       |
   * +-------+-------+-------+
   * |       |       |       |
   * | fxByF | fxCyF | fxFyF |
   * |       |       |       |
   * +-------+-------+-------+
   * |
   * |
   * |
   * v y
   *
   */
  const double fxF = (0.25) * (fxFyB + 2. * fxFyC + fxFyF);
  const double fxB = (0.25) * (fxByB + 2. * fxByC + fxByF);

  const double fyF = (0.25) * (fxByF + 2. * fxCyF + fxFyF);
  const double fyB = (0.25) * (fxByB + 2. * fxCyB + fxFyB);

  // Calc grad(f).
  fx = (fxF - fxB) / 2.;
  fy = (fyF - fyB) / 2.;

  // Normalize grad(f).
  const double gradNorm = std::sqrt(fx * fx + fy * fy);
  fx /= gradNorm;
  fy /= gradNorm;
}

} // namespace InterfaceNormal

