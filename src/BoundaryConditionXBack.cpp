//: BoundaryConditionXBack.cpp

#include "BoundaryConditionXBack.hpp"
#include "Definitions.hpp"
#include "DefinitionsEigen.hpp"
#include "Eigensystem.hpp"
#include "Utility.hpp"

#include "Mesh.hpp"
#include "MeshRec.hpp"

#include <iostream>
#include <cassert>

namespace BC {

void inflowXBack( const Mesh& p

                , const size_t j

                , const double velPorous
                , const double velLiquid

                , MeshRec& rec)
{
  using namespace Physics;

  /*  x-back boundary inflow boundary condition implementation
   *  (based on characteristic decomposition):
   *
   *  1. Linearize system of equation on previous time-step.
   *  2. Find Riemann invariants for linearized system.
   *  3. Find 3 Riemann invariants corresponding to negative eigenvalues
   *     and form 3 equations.
   *  4. Calc linearized equation for Total Shear Stress
   *     and form equation.
   *  5. Transport from previous time-step remaning variables.
   *  6. Solve linear system.
   */

  /* const double alpha1 = p(0,  Geometry::firstCell, j); */
  /* const double rho2   = p(1,  Geometry::firstCell, j); */
  /* const double rho1   = p(2,  Geometry::firstCell, j); */
  /* const double u11    = p(3,  Geometry::firstCell, j); */
  /* const double u12    = p(4,  Geometry::firstCell, j); */
  /* const double u21    = p(5,  Geometry::firstCell, j); */
  /* const double u22    = p(6,  Geometry::firstCell, j); */
  /* const double F11    = p(7,  Geometry::firstCell, j); */
  /* const double F12    = p(8,  Geometry::firstCell, j); */
  /* const double F21    = p(9,  Geometry::firstCell, j); */
  /* const double F22    = p(10, Geometry::firstCell, j); */

  /* // 1. Calc linearized flux matrix Ax for characteristic decomposition. */
  /* const Eigen::Matrix11 Ax(LinearizedFluxMatrices::Ax( alpha1 */
  /*                                                    , rho1 */
  /*                                                    , rho2 */
  /*                                                    , u11 */
  /*                                                    , u12 */
  /*                                                    , u21 */
  /*                                                    , u22 */
  /*                                                    , F11 */
  /*                                                    , F12 */
  /*                                                    , F21 */
  /*                                                    , F22)); */

  /* // 2. We need eigenvalues and eigenvectors. */
  /* const Eigen::EigenSolver11 eigenSolver11(Ax); */

  /* // @TODO: Check complex part. */
  /* const Eigen::VectorRow11 eigenValues (eigenSolver11.eigenvalues ().real()); */
  /* const Eigen::Matrix11    eigenVectors(eigenSolver11.eigenvectors().real()); */

  /* // Calc Riemann invariants for linearized system. */
  /* const Eigen::Matrix11 riemainnInvariants(eigenVectors.colPivHouseholderQr().solve(Eigen::Matrix11::Identity())); */

  /* // We want to solve linear system: leftSide * boundaryValues = rightSide. */
  /* // Form leftSide and rightSide. */
  /* Eigen::Matrix11 leftSide = Eigen::Matrix11::Zero(11, 11); */

  /* // Sort Riemann ivariants by eigenvalues. */
  /* std::vector<std::pair<double, Eigen::VectorRow11> > eigenSet; */
  /* for (size_t index = 0; index < Physics::varCount; ++index) { */
  /*   eigenSet.push_back(std::pair<double, Eigen::VectorRow11>( eigenValues(index) */
  /*                                                           , riemainnInvariants.row(index))); */
  /* } */

  /* auto sortedEigenSet = Utility::sortVectorsByValues(eigenSet); */

  /* // 3. Get 3 Riemann invariants corresponds to negative eigenvalues. */
  /* //leftSide.row(0) = sortedEigenSet.at(0).second; */
  /* //leftSide.row(1) = sortedEigenSet.at(1).second; */
  /* //leftSide.row(2) = sortedEigenSet.at(2).second; */

  /* // 4. We use linearized equation of Total Shear Stress (12). */
  /* // Compute linearized coefficients. */
  /* const double pdRho1 = -((F11*F21+F12*F22)*(pow(F12,2)*(-2+pow(F21,2))-2*F11*F12*F21*F22+pow(F11,2)*(-2+pow(F22,2))-2*(pow(F21,2)+pow(F22,2)))*mu)/(3.*pow(F12*F21-F11*F22,4)*rho10); */
  /* const double pdF11  =  ((-(pow(F11,3)*F21*F22*(-2+pow(F22,2)))+2*F12*(pow(F21,2)+pow(F22,2))*(pow(F21,2)+4*pow(F22,2))+pow(F11,2)*F12*(6*pow(F21,2)+(4+pow(F21,2))*pow(F22,2)-2*pow(F22,4))-pow(F12,3)*(pow(F21,4)-8*pow(F22,2)+2*pow(F21,2)*(-1+pow(F22,2)))+F11*F21*F22*(6*(pow(F21,2)+pow(F22,2))+pow(F12,2)*(10+pow(F21,2)+4*pow(F22,2))))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10); */
  /* const double pdF12  =  ((-(pow(F11,2)*F12*F21*F22*(10+4*pow(F21,2)+pow(F22,2)))+pow(F11,3)*(-8*pow(F21,2)+2*(-1+pow(F21,2))*pow(F22,2)+pow(F22,4))+F12*F21*F22*(pow(F12,2)*(-2+pow(F21,2))-6*(pow(F21,2)+pow(F22,2)))+F11*(-2*(pow(F21,2)+pow(F22,2))*(4*pow(F21,2)+pow(F22,2))+pow(F12,2)*(2*pow(F21,2)*(-2+pow(F21,2))-(6+pow(F21,2))*pow(F22,2))))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10); */
  /* const double pdF21  =  ((F11*F12*F21*(-6*(pow(F11,2)+pow(F12,2))+(-2+pow(F12,2))*pow(F21,2))-(2*(pow(F11,2)+pow(F12,2))*(pow(F11,2)+4*pow(F12,2))+(6*pow(F11,2)+(4+pow(F11,2))*pow(F12,2)-2*pow(F12,4))*pow(F21,2))*F22-F11*F12*(10+pow(F11,2)+4*pow(F12,2))*F21*pow(F22,2)+(pow(F11,4)-8*pow(F12,2)+2*pow(F11,2)*(-1+pow(F12,2)))*pow(F22,3))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10); */
  /* const double pdF22  =  ((2*(pow(F11,2)+pow(F12,2))*(4*pow(F11,2)+pow(F12,2))*F21-(-8*pow(F11,2)+2*(-1+pow(F11,2))*pow(F12,2)+pow(F12,4))*pow(F21,3)+F11*F12*(6*(pow(F11,2)+pow(F12,2))+(10+4*pow(F11,2)+pow(F12,2))*pow(F21,2))*F22+(-2*pow(F11,4)+6*pow(F12,2)+pow(F11,2)*(4+pow(F12,2)))*F21*pow(F22,2)-F11*(-2+pow(F11,2))*F12*pow(F22,3))*mu*rho1)/(3.*pow(F12*F21-F11*F22,5)*rho10); */

  /* // Form row for linearized equation of Total Shear Stress (12). */
  /* leftSide.row(0) << 0., 0., pdRho1, 0., 0., 0., 0., pdF11, pdF12, pdF21, pdF22; */

  /* // 5. Fill remaining rows: following variables we transport from (n)-timelayer. */
  /* leftSide(1,   1) = 1.; // rho2 */
  /* leftSide(2,   2) = 1.; // rho1 */
  /* leftSide(3,   6) = 1.; // u22 */

  /* leftSide(4,   3) = 1.; // u11 */
  /* leftSide(5,   4) = 1.; // u12 */
  /* leftSide(6,   5) = 1.; // u21 */
  /* leftSide(7,   0) = 1.; // alpha1 */
  /* leftSide(8,   7) = 1.; // F11 */
  /* leftSide(9,   8) = 1.; // F12 */
  /* leftSide(10, 10) = 1.; // F22 */

  /* // Leftside of equation is filled. */
  /* // Form rightside. */

  /* Eigen::VectorCol11 cell; */
  /* cell << alpha1, rho2, rho1, u11, u12, u21, u22, F11, F12, F21, F22; */

  /* Eigen::VectorCol11 rightSide; */
  /* rightSide(0)  = leftSide.row(0) * cell; */
  /* rightSide(1)  = rho2;//leftSide.row(1) * cell; */
  /* rightSide(2)  = rho1;//leftSide.row(2) * cell; */
  /* //rightSide(3)  = leftSide.row(3) * cell; */
  /* rightSide(3)  = u22; */
  /* rightSide(4)  = velPorous; */
  /* rightSide(5)  = velLiquid; */
  /* rightSide(6)  = u21;    // transport */
  /* rightSide(7)  = alpha1; // transport */
  /* rightSide(8)  = F11;    // transport */
  /* rightSide(9)  = F12;    // transport */
  /* rightSide(10) = F22;    // transport */

  /* // Rightside of equation is filled. */

  /* // 6. Solve linear system. */
  /* const Eigen::VectorCol11 bcP(leftSide.colPivHouseholderQr().solve(rightSide)); */

  /* // Unpack solution. */
  /* const double bAlpha1 = bcP(0); */
  /* const double bRho2   = bcP(1); */
  /* const double bRho1   = bcP(2); */
  /* const double bU11    = bcP(3); */
  /* const double bU12    = bcP(4); */
  /* const double bU21    = bcP(5); */
  /* const double bU22    = bcP(6); */
  /* const double bF11    = bcP(7); */
  /* const double bF12    = bcP(8); */
  /* const double bF21    = bcP(9); */
  /* const double bF22    = bcP(10); */

  /* rec.xB(0,  Geometry::firstCell, j) = bAlpha1; */
  /* rec.xB(1,  Geometry::firstCell, j) = bRho2; */
  /* rec.xB(2,  Geometry::firstCell, j) = bRho1; */
  /* rec.xB(3,  Geometry::firstCell, j) = bU11; */
  /* rec.xB(4,  Geometry::firstCell, j) = bU12; */
  /* rec.xB(5,  Geometry::firstCell, j) = bU21; */
  /* rec.xB(6,  Geometry::firstCell, j) = bU22; */
  /* rec.xB(7,  Geometry::firstCell, j) = bF11; */
  /* rec.xB(8,  Geometry::firstCell, j) = bF12; */
  /* rec.xB(9,  Geometry::firstCell, j) = bF21; */
  /* rec.xB(10, Geometry::firstCell, j) = bF22; */

  /* // Check solution. */
  /* assert(bAlpha1 > 0. and bAlpha1 < 1.); */
  /* assert(bRho2 > 0.); */
  /* assert(bRho1 > 0.); */
}

void nonReflectXB( const Mesh& p
                 , size_t j
                 , MeshRec& rec)
{
  using namespace Physics;

  const double alpha1 = p(0, Geometry::firstCell, j);
  const double rho1   = p(1, Geometry::firstCell, j);
  const double rho2   = p(2, Geometry::firstCell, j);
  const double u1     = p(3, Geometry::firstCell, j);
  const double u2     = p(4, Geometry::firstCell, j);
  const double F11    = p(5, Geometry::firstCell, j);
  const double F12    = p(6, Geometry::firstCell, j);
  const double F21    = p(7, Geometry::firstCell, j);
  const double F22    = p(8, Geometry::firstCell, j);

  const auto eigensystem = Eigensystem::eigensystemX( alpha1
                                                    , rho1
                                                    , rho2
                                                    , u1
                                                    , F11
                                                    , F12
                                                    , F21
                                                    , F22);

  const Eigen::Matrix9 lEigenvectors  = eigensystem.second;
  std::cout << lEigenvectors.fullPivHouseholderQr().rank() << std::endl;

  Eigen::VectorCol9 cell;
  cell << alpha1, rho1, rho2, u1, u2, F11, F12, F21, F22;

  Eigen::VectorCol9 cell0;
  cell0 << 0.5, 8.49, 1.0, 0., 0., 1., 0., 0., 1.;

  const auto eigensystem0 = Eigensystem::eigensystemX( cell0(0)
                                                     , cell0(1)
                                                     , cell0(2)
                                                     , cell0(3)
                                                     , cell0(5)
                                                     , cell0(6)
                                                     , cell0(7)
                                                     , cell0(8));

  const Eigen::Matrix9 lEigenvectors0  = eigensystem0.second;

  Eigen::VectorCol9 riemannInvariants;

  riemannInvariants(0) = lEigenvectors.row(0) * cell;
  riemannInvariants(1) = lEigenvectors.row(1) * cell;
  riemannInvariants(2) = lEigenvectors.row(2) * cell;
  riemannInvariants(3) = lEigenvectors.row(3) * cell;
  riemannInvariants(4) = lEigenvectors.row(4) * cell;
  riemannInvariants(5) = lEigenvectors.row(5) * cell; // u-
  riemannInvariants(6) = lEigenvectors.row(6) * cell; // u-

  riemannInvariants(7) = lEigenvectors0.row(7) * cell0; // u+
  riemannInvariants(8) = lEigenvectors0.row(8) * cell0; // u+

  Eigen::Matrix9 L;
  L.row(0) = lEigenvectors.row(0);
  L.row(1) = lEigenvectors.row(1);
  L.row(2) = lEigenvectors.row(2);
  L.row(3) = lEigenvectors.row(3);
  L.row(4) = lEigenvectors.row(4);
  L.row(5) = lEigenvectors.row(5);
  L.row(6) = lEigenvectors.row(6);
  L.row(7) = lEigenvectors0.row(7);
  L.row(8) = lEigenvectors0.row(8);

  const Eigen::VectorCol9 bcP(L.fullPivHouseholderQr().solve(riemannInvariants));

  const double alpha1B = bcP(0);
  const double rho1B   = bcP(1);
  const double rho2B   = bcP(2);
  const double u1B     = bcP(3);
  const double u2B     = bcP(4);
  const double F11B    = bcP(5);
  const double F12B    = bcP(6);
  const double F21B    = bcP(7);
  const double F22B    = bcP(8);

  std::cout << bcP << std::endl;

  // Check solution.
  assert(alpha1B > 0. and alpha1B < 1.);
  assert(rho1B > 0.);
  assert(rho2B > 0.);

  rec.xB(0, Geometry::firstCell, j) = alpha1B;
  rec.xB(1, Geometry::firstCell, j) = rho1B;
  rec.xB(2, Geometry::firstCell, j) = rho2B;
  rec.xB(3, Geometry::firstCell, j) = u1B;
  rec.xB(4, Geometry::firstCell, j) = u2B;
  rec.xB(5, Geometry::firstCell, j) = F11B;
  rec.xB(6, Geometry::firstCell, j) = F12B;
  rec.xB(7, Geometry::firstCell, j) = F21B;
  rec.xB(8, Geometry::firstCell, j) = F22B;
}

} // namespace BC

