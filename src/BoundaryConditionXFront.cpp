//: BoundaryConditionXFront.cpp

#include "BoundaryConditionXFront.hpp"
#include "Definitions.hpp"
#include "DefinitionsEigen.hpp"
#include "Eigensystem.hpp"
#include "Utility.hpp"

#include "Mesh.hpp"
#include "MeshRec.hpp"

#include <cassert>

namespace BC {

void inflowXFront( const double alpha1
                 , const double rho
                 , const double rho1
                 , const double u11
                 , const double u12
                 , const double u21
                 , const double u22
                 , const double F11
                 , const double F12
                 , const double F21
                 , const double F22

                 , const double velPorous
                 , const double velLiquid

                 , double& xF0
                 , double& xF1
                 , double& xF2
                 , double& xF3
                 , double& xF4
                 , double& xF5
                 , double& xF6
                 , double& xF7
                 , double& xF8
                 , double& xF9
                 , double& xF10)
{
  using namespace Physics;

  /*  x-front boundary inflow boundary condition implementation
   *  (based on characteristic decomposition):
   *
   *  1. Linearize system of equation on previous time-step.
   *  2. Find Riemann invariants for linearized system.
   *  3. Find 3 Riemann invariants corresponding to negative eigenvalues
   *     and form 3 equations.
   *  4. Calc linearized equation for Total Shear Stress
   *     and form equation.
   *  5. Transport from previous time-step remaning variables.
   *  6. Solve linear system.
   */


  /* // 1. Calc linearized flux matrix Ax for characteristic decomposition. */
  /* const Eigen::Matrix11 Ax(LinearizedFluxMatrices::Ax( alpha1 */
  /*                                                    , rho */
  /*                                                    , rho1 */
  /*                                                    , u11 */
  /*                                                    , u12 */
  /*                                                    , u21 */
  /*                                                    , u22 */
  /*                                                    , F11 */
  /*                                                    , F12 */
  /*                                                    , F21 */
  /*                                                    , F22)); */

  /* // 2. We need eigenvalues and eigenvectors. */
  /* const Eigen::EigenSolver11 eigenSolver11(Ax); */

  /* // @TODO: Check complex part. */
  /* const Eigen::VectorRow11 eigenValues (eigenSolver11.eigenvalues ().real()); */
  /* const Eigen::Matrix11    eigenVectors(eigenSolver11.eigenvectors().real()); */

  /* //std::cout << eigenValues << std::endl; */

  /* // Calc Riemann invariants for linearized system. */
  /* const Eigen::Matrix11 riemainnInvariants(eigenVectors.colPivHouseholderQr().solve(Eigen::Matrix11::Identity())); */

  /* // We want to solve linear system: leftSide * boundaryValues = rightSide. */
  /* // Form leftSide and rightSide. */
  /* Eigen::Matrix11 leftSide = Eigen::Matrix11::Zero(11, 11); */

  /* // Sort Riemann invariants by eigenvalues. */
  /* std::vector<std::pair<double, Eigen::VectorRow11> > eigenSet; */
  /* for (size_t index = 0; index < Physics::varCount; ++index) { */
  /*   eigenSet.push_back(std::pair<double, Eigen::VectorRow11>( eigenValues(index) */
  /*                                                           , riemainnInvariants.row(index))); */
  /* } */

  /* auto sortedEigenSet = Utility::sortVectorsByValues(eigenSet); */

  /* // 3. Get 3 Riemann invariants corresponds to negative eigenvalues. */
  /* leftSide.row(0) = sortedEigenSet.at(10).second; */
  /* leftSide.row(1) = sortedEigenSet.at(9).second; */
  /* leftSide.row(2) = sortedEigenSet.at(8).second; */

  /* // 4. We use linearized equation of Total Shear Stress (12). */
  /* // Compute linearized coefficients. */
  /* const double pdRho1 =  (pow(pow(F12*F21-F11*F22,-2),4.0/3.0)*(F11*F21+F12*F22)*(pow(F11,2)+pow(F12,2)+pow(F21,2)+pow(F22,2))*mu)/(2.*rho10); */
  /* const double pdF11  =  (pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(-(pow(F11,3)*F21*F22)+pow(F11,2)*F12*(9*pow(F21,2)+2*pow(F22,2))+F12*(pow(F12,2)+pow(F21,2)+pow(F22,2))*(3*pow(F21,2)+8*pow(F22,2))+F11*F21*F22*(11*pow(F12,2)+5*(pow(F21,2)+pow(F22,2))))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho10); */
  /* const double pdF12  =  (pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(-2*F11*pow(F21,2)*(4*pow(F11,2)+pow(F12,2)+4*pow(F21,2))+F12*F21*(-11*pow(F11,2)+pow(F12,2)-5*pow(F21,2))*F22-F11*(3*pow(F11,2)+9*pow(F12,2)+11*pow(F21,2))*pow(F22,2)-5*F12*F21*pow(F22,3)-3*F11*pow(F22,4))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho10); */
  /* const double pdF21  = -(pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*(5*pow(F11,3)*F12*F21+3*pow(F11,4)*F22+pow(F11,2)*F22*(11*pow(F12,2)+9*pow(F21,2)+3*pow(F22,2))+2*pow(F12,2)*F22*(4*pow(F12,2)+pow(F21,2)+4*pow(F22,2))+F11*F12*F21*(5*pow(F12,2)-pow(F21,2)+11*pow(F22,2)))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho10); */
  /* const double pdF22  =  (pow(pow(F12*F21-F11*F22,-2),1.0/3.0)*((8*pow(F11,2)+3*pow(F12,2))*F21*(pow(F11,2)+pow(F12,2)+pow(F21,2))+F11*F12*(5*(pow(F11,2)+pow(F12,2))+11*pow(F21,2))*F22+(2*pow(F11,2)+9*pow(F12,2))*F21*pow(F22,2)-F11*F12*pow(F22,3))*mu*rho1)/(6.*pow(F12*F21-F11*F22,3)*rho10); */ 

  /* // Form row for linearized equation of Total Shear Stress (12). */
  /* leftSide.row(3) << 0., 0., pdRho1, 0., 0., 0., 0., pdF11, pdF12, pdF21, pdF22; */

  /* // 5. Fill remaining rows: following variables we transport from (n)-timelayer. */
  /* leftSide(4,   3) = 1.; // u11 */
  /* leftSide(5,   4) = 1.; // u12 */
  /* leftSide(6,   5) = 1.; // u21 */
  /* leftSide(7,   0) = 1.; // alpha1 */
  /* leftSide(8,   7) = 1.; // F11 */
  /* leftSide(9,   8) = 1.; // F12 */
  /* leftSide(10, 10) = 1.; // F22 */

  /* // Leftside of equation is filled. */
  /* // Form rightside. */

  /* Eigen::VectorCol11 cell; */
  /* cell << alpha1, rho, rho1, u11, u12, u21, u22, F11, F12, F21, F22; */

  /* Eigen::VectorCol11 rightSide; */
  /* rightSide(0)  = leftSide.row(0) * cell; */
  /* rightSide(1)  = leftSide.row(1) * cell; */
  /* rightSide(2)  = leftSide.row(2) * cell; */
  /* rightSide(3)  = leftSide.row(3) * cell; */
  /* rightSide(4)  = velPorous; */
  /* rightSide(5)  = velLiquid; */
  /* rightSide(6)  = u21;    // transport */
  /* rightSide(7)  = alpha1; // transport */
  /* rightSide(8)  = F11;    // transport */
  /* rightSide(9)  = F12;    // transport */
  /* rightSide(10) = F22;    // transport */

  /* // Rightside of equation is filled. */

  /* // 6. Solve linear system. */
  /* const Eigen::VectorCol11 bcP(leftSide.colPivHouseholderQr().solve(rightSide)); */

  /* // Unpack solution. */
  /* const double bAlpha1 = bcP(0); */
  /* const double bRho    = bcP(1); */
  /* const double bRho1   = bcP(2); */
  /* const double bU11    = bcP(3); */
  /* const double bU12    = bcP(4); */
  /* const double bU21    = bcP(5); */
  /* const double bU22    = bcP(6); */
  /* const double bF11    = bcP(7); */
  /* const double bF12    = bcP(8); */
  /* const double bF21    = bcP(9); */
  /* const double bF22    = bcP(10); */

  /* // Check solution. */
  /* assert(bAlpha1 > 0. and bAlpha1 < 1.); */
  /* assert(bRho  > 0.); */
  /* assert(bRho1 > 0.); */

  /* // Calc additional variables. */
  /* const double bAlpha2 = VariablesAdditional::alpha2(bAlpha1); */
  /* const double bRho2   = VariablesAdditional::rho2(bAlpha1, bRho, bRho1); */
  /* const double bC1     = VariablesAdditional::c1(bAlpha1, bRho, bRho1); */
  /* const double bC2     = VariablesAdditional::c2(bAlpha1, bRho, bRho1); */
  /* const double bU1     = VariablesAdditional::u1(bC1, bC2, bU11, bU12); */
  /* const double bU2     = VariablesAdditional::u2(bC1, bC2, bU21, bU22); */
  /* // Calc additional average variables. */
  /* const double c1 = VariablesAdditional::c1(alpha1, rho, rho1); */
  /* const double c2 = VariablesAdditional::c2(alpha1, rho, rho1); */
  /* const double u1 = VariablesAdditional::u1(c1, c2, u11, u12); */
  /* // Calc energy and pressure. */
  /* const double bE1 = Energy::e1(bRho1, bF11, bF12, bF21, bF22); */
  /* const double bE2 = Energy::e2(bRho2); */
  /* const double bP1 = Energy::p1(bRho1); */
  /* const double bP2 = Energy::p2(bRho2); */
  /* // Calc shear stresses. */
  /* const double bSigma11 = ShearStress::sigma11(bRho1, bF11, bF12, bF21, bF22); */
  /* const double bSigma21 = ShearStress::sigma12(bRho1, bF11, bF12, bF21, bF22); */

  /* // Calc flux. */
  /* xF0  = Flux::xF0(bAlpha1, u1); */
  /* xF1  = Flux::xF1(bRho, bU1); */
  /* xF2  = Flux::xF2(bAlpha1, bRho1, bU11); */
  /* xF3  = Flux::xF3(bRho1, bRho2, bU11, bU12, bU21, bU22, bE1, bE2, bP1, bP2); */
  /* xF4  = 0.; */
  /* xF5  = Flux::xF5(bAlpha1, bAlpha2, bRho1, bRho2, bU11, bU12, bP1,  bP2,  bSigma11); */
  /* xF6  = Flux::xF6(bAlpha1, bAlpha2, bRho1, bRho2, bU11, bU12, bU21, bU22, bSigma21); */
  /* xF7  = 0.; */
  /* xF8  = 0.; */
  /* xF9  = Flux::xF9 (bRho, bF11, bF21, bU1, bU2); */
  /* xF10 = Flux::xF10(bRho, bF12, bF22, bU1, bU2); */
}

void nonReflectXF( const Mesh& p
                 , size_t j
                 , MeshRec& rec)
{
  using namespace Physics;

  const double alpha1 = p(0, Geometry::lastCellX, j);
  const double rho1   = p(1, Geometry::lastCellX, j);
  const double rho2   = p(2, Geometry::lastCellX, j);
  const double u1     = p(3, Geometry::lastCellX, j);
  const double u2     = p(4, Geometry::lastCellX, j);
  const double F11    = p(5, Geometry::lastCellX, j);
  const double F12    = p(6, Geometry::lastCellX, j);
  const double F21    = p(7, Geometry::lastCellX, j);
  const double F22    = p(8, Geometry::lastCellX, j);

  const auto eigensystem = Eigensystem::eigensystemX( alpha1
                                                    , rho1
                                                    , rho2
                                                    , u1
                                                    , F11
                                                    , F12
                                                    , F21
                                                    , F22);

  const Eigen::Matrix9 lEigenvectors  = eigensystem.second;

  Eigen::VectorCol9 cell;
  cell << alpha1, rho1, rho2, u1, u2, F11, F12, F21, F22;

  Eigen::VectorCol9 cell0;
  cell0 << 0.5, 8.49, 1.0, 0., 0., 1., 0., 0., 1.;

  const auto eigensystem0 = Eigensystem::eigensystemX( cell0(0)
                                                     , cell0(1)
                                                     , cell0(2)
                                                     , cell0(3)
                                                     , cell0(5)
                                                     , cell0(6)
                                                     , cell0(7)
                                                     , cell0(8));

  const Eigen::Matrix9 lEigenvectors0  = eigensystem0.second;

  Eigen::VectorCol9 riemannInvariants;

  riemannInvariants(0) = lEigenvectors.row(0) * cell;
  riemannInvariants(1) = lEigenvectors.row(1) * cell;
  riemannInvariants(2) = lEigenvectors.row(2) * cell;
  riemannInvariants(3) = lEigenvectors.row(3) * cell;
  riemannInvariants(4) = lEigenvectors.row(4) * cell;
  riemannInvariants(5) = lEigenvectors.row(7) * cell; // u+
  riemannInvariants(6) = lEigenvectors.row(8) * cell; // u+

  riemannInvariants(7) = lEigenvectors0.row(5) * cell0; // u-
  riemannInvariants(8) = lEigenvectors0.row(6) * cell0; // u-

  Eigen::Matrix9 L;
  L.row(0) = lEigenvectors.row(0);
  L.row(1) = lEigenvectors.row(1);
  L.row(2) = lEigenvectors.row(2);
  L.row(3) = lEigenvectors.row(3);
  L.row(4) = lEigenvectors.row(4);
  L.row(5) = lEigenvectors.row(7);
  L.row(6) = lEigenvectors.row(8);
  L.row(7) = lEigenvectors0.row(5);
  L.row(8) = lEigenvectors0.row(6);

  const Eigen::VectorCol9 bcP(L.fullPivHouseholderQr().solve(riemannInvariants));

  const double alpha1B = bcP(0);
  const double rho1B   = bcP(1);
  const double rho2B   = bcP(2);
  const double u1B     = bcP(3);
  const double u2B     = bcP(4);
  const double F11B    = bcP(5);
  const double F12B    = bcP(6);
  const double F21B    = bcP(7);
  const double F22B    = bcP(8);

  // Check solution.
  assert(alpha1B > 0. and alpha1B < 1.);
  assert(rho1B > 0.);
  assert(rho2B > 0.);

  rec.xF(0, Geometry::lastCellX, j) = alpha1B;
  rec.xF(1, Geometry::lastCellX, j) = rho1B;
  rec.xF(2, Geometry::lastCellX, j) = rho2B;
  rec.xF(3, Geometry::lastCellX, j) = u1B;
  rec.xF(4, Geometry::lastCellX, j) = u2B;
  rec.xF(5, Geometry::lastCellX, j) = F11B;
  rec.xF(6, Geometry::lastCellX, j) = F12B;
  rec.xF(7, Geometry::lastCellX, j) = F21B;
  rec.xF(8, Geometry::lastCellX, j) = F22B;
}

} // namespace BC

