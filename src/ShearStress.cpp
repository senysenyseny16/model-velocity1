//: ShearStress.cpp

#include "ShearStress.hpp"
#include "Definitions.hpp"

#include <cmath>

using namespace std;
using namespace Physics;

namespace ShearStress {

double shearStress11( const double rho1
                    , const double F11
                    , const double F12
                    , const double F21
                    , const double F22)
{
  return -((4*F11*F12*F21*F22+pow(F11,2)*(F21-F22)*(F21+F22)+pow(F12,2)*(-pow(F21,2)+pow(F22,2))+pow(pow(F21,2)+pow(F22,2),2))*mu*rho1)/(2.*pow(F12*F21-F11*F22,4)*rho10);
}

double shearStress12( const double rho1
                    , const double F11
                    , const double F12
                    , const double F21
                    , const double F22)
{
  return shearStress21(rho1, F11, F12, F21, F22);
}

double shearStress21( const double rho1
                    , const double F11
                    , const double F12
                    , const double F21
                    , const double F22)
{
  return ((F11*F21+F12*F22)*(pow(F11,2)+pow(F12,2)+pow(F21,2)+pow(F22,2))*mu*rho1)/(2.*pow(F12*F21-F11*F22,4)*rho10);
}

double shearStress22( const double rho1
                    , const double F11
                    , const double F12
                    , const double F21
                    , const double F22)
{
  return -((pow(F11,4)+4*F11*F12*F21*F22+pow(F11,2)*(2*pow(F12,2)+pow(F21,2)-pow(F22,2))+pow(F12,2)*(pow(F12,2)-pow(F21,2)+pow(F22,2)))*mu*rho1)/(2.*pow(F12*F21-F11*F22,4)*rho10);
}

double s11dRho1( const double F11
               , const double F12
               , const double F21
               , const double F22)
{
  return -((4*F11*F12*F21*F22+pow(F11,2)*(F21-F22)*(F21+F22)+pow(F12,2)*(-pow(F21,2)+pow(F22,2))+pow(pow(F21,2)+pow(F22,2),2))*mu)/(2.*pow(F12*F21-F11*F22,4)*rho10);
}

double s11dF11( const double rho1
              , const double F11
              , const double F12
              , const double F21
              , const double F22)
{
  return -(((pow(F11,2)*(F21-F22)*F22*(F21+F22)+F11*F12*F21*(pow(F21,2)+5*pow(F22,2))+2*(pow(F21,4)*F22+(pow(F12,2)+2*pow(F21,2))*pow(F22,3)+pow(F22,5)))*mu*rho1)/(pow(F12*F21-F11*F22,5)*rho10));
}

double s11dF12( const double rho1
              , const double F11
              , const double F12
              , const double F21
              , const double F22)
{
  return ((2*pow(F11,2)*pow(F21,3)+pow(F12,2)*F21*(-pow(F21,2)+pow(F22,2))+2*F21*pow(pow(F21,2)+pow(F22,2),2)+F11*F12*F22*(5*pow(F21,2)+pow(F22,2)))*mu*rho1)/(pow(F12*F21-F11*F22,5)*rho10);
}

double s11dF21( const double rho1
              , const double F11
              , const double F12
              , const double F21
              , const double F22)
{
  return (((F11-F12)*F12*(F11+F12)*pow(F21,2)+F11*F21*(pow(F11,2)+5*pow(F12,2)+2*pow(F21,2))*F22+2*F12*(pow(F12,2)+pow(F21,2))*pow(F22,2)+2*F11*F21*pow(F22,3)+2*F12*pow(F22,4))*mu*rho1)/(pow(F12*F21-F11*F22,5)*rho10);
}

double s11dF22( const double rho1
              , const double F11
              , const double F12
              , const double F21
              , const double F22)
{
  return -(((2*F11*pow(F21,2)*(pow(F11,2)+pow(F21,2))+F12*F21*(5*pow(F11,2)+pow(F12,2)+2*pow(F21,2))*F22+F11*(-pow(F11,2)+pow(F12,2)+2*pow(F21,2))*pow(F22,2)+2*F12*F21*pow(F22,3))*mu*rho1)/(pow(F12*F21-F11*F22,5)*rho10));
}

double s12dRho1( const double F11
               , const double F12
               , const double F21
               , const double F22)
{
  return ((F11*F21+F12*F22)*(pow(F11,2)+pow(F12,2)+pow(F21,2)+pow(F22,2))*mu)/(2.*pow(F12*F21-F11*F22,4)*rho10);
}

double s12dF11( const double rho1
              , const double F11
              , const double F12
              , const double F21
              , const double F22)
{
  return ((pow(F11,3)*F21*F22+pow(F11,2)*F12*(3*pow(F21,2)+2*pow(F22,2))+F12*(pow(F12,2)+pow(F21,2)+pow(F22,2))*(pow(F21,2)+4*pow(F22,2))+F11*F21*F22*(5*pow(F12,2)+3*(pow(F21,2)+pow(F22,2))))*mu*rho1)/(2.*pow(F12*F21-F11*F22,5)*rho10);
}

double s12dF12( const double rho1
              , const double F11
              , const double F12
              , const double F21
              , const double F22)
{
  return -((2*F11*pow(F21,2)*(2*pow(F11,2)+pow(F12,2)+2*pow(F21,2))+F12*F21*(5*pow(F11,2)+pow(F12,2)+3*pow(F21,2))*F22+F11*(pow(F11,2)+3*pow(F12,2)+5*pow(F21,2))*pow(F22,2)+3*F12*F21*pow(F22,3)+F11*pow(F22,4))*mu*rho1)/(2.*pow(F12*F21-F11*F22,5)*rho10);
}

double s12dF21( const double rho1
              , const double F11
              , const double F12
              , const double F21
              , const double F22)
{
  return -((3*pow(F11,3)*F12*F21+pow(F11,4)*F22+pow(F11,2)*F22*(5*pow(F12,2)+3*pow(F21,2)+pow(F22,2))+2*pow(F12,2)*F22*(2*pow(F12,2)+pow(F21,2)+2*pow(F22,2))+F11*F12*F21*(3*pow(F12,2)+pow(F21,2)+5*pow(F22,2)))*mu*rho1)/(2.*pow(F12*F21-F11*F22,5)*rho10);
}

double s12dF22( const double rho1
              , const double F11
              , const double F12
              , const double F21
              , const double F22)
{
  return (((4*pow(F11,2)+pow(F12,2))*F21*(pow(F11,2)+pow(F12,2)+pow(F21,2))+F11*F12*(3*(pow(F11,2)+pow(F12,2))+5*pow(F21,2))*F22+(2*pow(F11,2)+3*pow(F12,2))*F21*pow(F22,2)+F11*F12*pow(F22,3))*mu*rho1)/(2.*pow(F12*F21-F11*F22,5)*rho10);
}

} // namespace ShearStress

