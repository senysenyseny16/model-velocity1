//: WENO5.cpp

#include "WENO.hpp"
#include "Definitions.hpp" // for SchemePars::WenoEps.

#include <cmath>

namespace WENO {

void weno5( const double vBB
          , const double vB
          , const double vC
          , const double vF
          , const double vFF
          , double& recB
          , double& recF)
{
  // 1. Obtain 3 reconstructed values v0_{i+0.5}, v1_{i+0.5}, v2_{i+0.5}
  //    of 3-th order accuracy, based on the stencils
  //    S0 = {x_{ i }, x_{i+1}, x_{i+2}},
  //    S1 = {x_{i-1}, x_{ i }, x_{i+1}},
  //    S2 = {x_{i-2}, x_{i-1}, x_{ i }}.
  /* const double v0F =  (1. / 3.) * vC  + (5. / 6.) * vF - (1.  / 6.) * vFF; */
  const double v0F = -(1.  / 6.) * vFF  + (5. / 6.) * vF  + (1. / 3.) * vC;
  /* const double v1F = -(1. / 6.) * vB  + (5. / 6.) * vC + (1.  / 3.) * vF ; */
  const double v1F =  (1.  / 3.) * vF + (5. / 6.) * vC - (1. / 6.) * vB;
  /* const double v2F =  (1. / 3.) * vBB - (7. / 6.) * vB + (11. / 6.) * vC ; */
  const double v2F =  (11. / 6.) * vC - (7. / 6.) * vB + (1. / 3.) * vBB;

  // 2. Obtain 3 reconstructed values v0_{i-0.5}, v1_{i-0.5}, v2_{i-0.5}
  //    of 3-th order accuracy, again based on the same stencils.
  const double v0B =  (11. / 6.) * vC  - (7. / 6.) * vF + (1. / 3.) * vFF;
  const double v1B =  (1.  / 3.) * vB  + (5. / 6.) * vC - (1. / 6.) * vF ;
  const double v2B = -(1.  / 6.) * vBB + (5. / 6.) * vB + (1. / 3.) * vC ;

  // 3. Find the constants d_B and d_F.
  const double d0F = 3. / 10.;
  const double d1F = 6. / 10.;
  const double d2F = 1. / 10.;

  const double d0B = d2F;
  const double d1B = d1F;
  const double d2B = d0F;

  // 4. Find the smooth indicators.
  const double B0  =   (13. / 12.) * std::pow(std::abs(     vC - 2. * vF + vFF), 2)
                     + (1.  /  4.) * std::pow(std::abs(3. * vC - 4. * vF + vFF), 2);

  const double B1  =   (13. / 12.) * std::pow(std::abs(vB - 2. * vC + vF), 2)
                     + (1.  /  4.) * std::pow(std::abs(vB           - vF), 2);

  /* const double B2  =   (13. / 12.) * std::pow(std::abs(vBB - 2. * vB +      vC), 2) */
  /*                    + (1.  /  4.) * std::pow(std::abs(vBB - 4. * vB + 3. * vC), 2); */
  const double B2  =   (13. / 12.) * std::pow(std::abs(vC - 2. * vB +      vBB), 2)
                     + (1.  /  4.) * std::pow(std::abs(3. * vC - 4. * vB + vBB), 2);

  // 5. Form the weights w.
  const double denominator0 = (SchemePars::wenoEps + B0) * (SchemePars::wenoEps + B0);
  const double denominator1 = (SchemePars::wenoEps + B1) * (SchemePars::wenoEps + B1);
  const double denominator2 = (SchemePars::wenoEps + B2) * (SchemePars::wenoEps + B2);

  const double a0F = d0F / denominator0;
  const double a1F = d1F / denominator1;
  const double a2F = d2F / denominator2;

  const double aFsum = a2F + a1F + a0F;

  const double w0F = a0F / aFsum;
  const double w1F = a1F / aFsum;
  const double w2F = a2F / aFsum;

  const double a0B = d0B / denominator0;
  const double a1B = d1B / denominator1;
  const double a2B = d2B / denominator2;

  const double aBsum = a0B + a1B + a2B;

  const double w0B = a0B / aBsum;
  const double w1B = a1B / aBsum;
  const double w2B = a2B / aBsum;

  // 6. Find the reconstruction.
  recB = w0B * v0B + w1B * v1B + w2B * v2B;
  /* recF = w0F * v0F + w1F * v1F + w2F * v2F; */
  recF = w2F * v2F + w1F * v1F + w0F * v0F;
}

} // namespace WENO

