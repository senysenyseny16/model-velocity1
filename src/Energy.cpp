//: Energy.cpp

#include "Energy.hpp"
#include "Definitions.hpp"

#include <cmath>

#include <cassert>

namespace Energy {

double liquid( const double C
             , const double gamma
             , const double rho
             , const double rho0
             , const double p0)
{
  assert(rho > 0.);

  const double liquidEnergy = (-gamma * p0 + (C * C * rho0 * (gamma - 1. + std::pow(rho / rho0, gamma))) / (gamma - 1.)) / (gamma * rho);

  assert(liquidEnergy > 0.);

  return (liquidEnergy);
}

double solid( const double mu
            , const double rho0
            , const double F11
            , const double F12
            , const double F21
            , const double F22)
{
  using namespace std;

  /* const double solidEnergy = ((pow(F12+F21,2)+pow(F11-F22,2))*(pow(F12-F21,2)+pow(F11+F22,2))*mu)/(8.*pow(F12*F21-F11*F22,4)*rho0); */
  /* const double solidEnergy = ((pow(F12,4)*(1-pow(F21,2)+pow(F21,4))+2*pow(F11,3)*F12*F21*F22*(1-2*pow(F22,2))+pow(pow(F21,2)+pow(F22,2),2)+2*F11*F12*F21*F22*(3+pow(F21,2)+pow(F12,2)*(1-2*pow(F21,2))+pow(F22,2))+pow(F11,4)*(1-pow(F22,2)+pow(F22,4))-pow(F12,2)*(-2*pow(F22,2)+pow(F21,2)*(1+pow(F21,2)+pow(F22,2)))+pow(F11,2)*(2*pow(F21,2)-(1+pow(F21,2))*pow(F22,2)-pow(F22,4)+pow(F12,2)*(2-pow(F21,2)+(-1+6*pow(F21,2))*pow(F22,2))))*mu)/(6.*pow(F12*F21-F11*F22,4)*rho0); */
  const double solidEnergy = ((pow(pow(F11,2)+pow(F12,2),2)+pow(F12*F21-F11*F22,4)-3*pow(pow(F12*F21-F11*F22,-2),0.6666666666666666)*pow(F12*F21-F11*F22,4)+2*pow(F11*F21+F12*F22,2)+pow(pow(F21,2)+pow(F22,2),2))*mu)/(8.*pow(pow(F12*F21-F11*F22,-2),0.6666666666666666)*pow(F12*F21-F11*F22,4)*rho0);

  // @TODO: Check theory for constraints of elastic energy.

  /* return (solidEnergy > 0. ? solidEnergy : 0.); */
  return solidEnergy;
}

double porous( const double C
             , const double gamma
             , const double rho
             , const double rho0
             , const double p0
             , const double mu
             , const double F11
             , const double F12
             , const double F21
             , const double F22)
{

  const double liquidEnergy = liquid(C, gamma, rho, rho0, p0);
  const double solidEnergy  = solid(mu, rho0, F11, F12, F21, F22);

  //assert(liquidEnergy >= 0.);
  //assert(solidEnergy  >= 0.);

  return (liquidEnergy + solidEnergy);
}

double pressure( const double C
               , const double gamma
               , const double rho
               , const double rho0
               , const double p0)
{
  assert(rho > 0.);

  const double pressure = p0 + (((C * C) * rho0) * (std::pow(rho / rho0, gamma) - 1.)) / gamma;

  return (pressure);
}

double e1( const double rho1
         , const double F11
         , const double F12
         , const double F21
         , const double F22)
{
  return porous( Physics::C1
               , Physics::gamma1
               , rho1
               , Physics::rho10
               , Physics::p10
               , Physics::mu
               , F11, F12, F21, F22);
}

double e2(const double rho2)
{
  return liquid( Physics::C2
               , Physics::gamma2
               , rho2
               , Physics::rho20
               , Physics::p20);
}

double p1(const double rho1)
{
  return pressure( Physics::C1
                 , Physics::gamma1
                 , rho1
                 , Physics::rho10
                 , Physics::p10);
}

double p2(const double rho2)
{
  return pressure( Physics::C2
                 , Physics::gamma2
                 , rho2
                 , Physics::rho20
                 , Physics::p20);
}

double dp( const double C
         , const double gamma
         , const double rho
         , const double rho0)
{
  return ((C * C) * std::pow(rho / rho0, gamma - 1.));
}

double dp1(const double rho1)
{
  return dp( Physics::C1
           , Physics::gamma1
           , rho1
           , Physics::rho10);
}

double dp2(const double rho2)
{
  return dp( Physics::C2
           , Physics::gamma2
           , rho2
           , Physics::rho20);
}

double rho( const double C
          , const double gamma
          , const double rho0
          , const double p0
          , const double p)
{
  assert(p > 0.);

  const double rho = rho0 * std::pow(gamma * (p - p0) / (rho0 * (C * C)) + 1., 1. / gamma);

  assert(rho > 0.);

  return rho;
}

double rho1(const double p1)
{
  return rho( Physics::C1
            , Physics::gamma1
            , Physics::rho10
            , Physics::p10
            , p1);
}

double rho2(const double p2)
{
  return rho( Physics::C2
            , Physics::gamma2
            , Physics::rho20
            , Physics::p20
            , p2);
}

} // namespace Energy

