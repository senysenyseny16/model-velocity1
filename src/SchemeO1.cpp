//: SchemeO1.cpp

#include "SchemeO1.hpp"

#include "MeshRec.hpp"
#include "MeshFluxDifferential.hpp"
#include "MeshFluxNumerical.hpp"

#include "MeshConversion.hpp"
#include "PressureRelaxation.hpp"
#include "THINC.hpp"
#include "BoundaryConditions.hpp"
#include "VariablesAdditional.hpp"

#include "Utility.hpp"
#include "Visualization.hpp"
#include "DebugOutput.hpp"

#include <memory>
#include <iostream>

SchemeO1::SchemeO1(const Mesh& initialData)
  : Scheme(initialData)
{}

void SchemeO1::run()
{
  using namespace SchemePars;

  MeshRec              rec;
  MeshEdge             recC;
  MeshFluxDifferential fD;   // Differential flux.
  FluxNumerical::HLLC  fN;   // Numerical flux.

  // Temporary vectors for conservative cells.
  double cVector0  [Physics::varCount];
  double cVectorNew[Physics::varCount];

  // Plot initial data.
  //Visualization::plotMeshPhysical(p0);
  //Visualization::plotMeshPhysicalToFile(p0, std::to_string(iteration));
  //Utility::saveMeshToFile(p0, std::to_string(iteration) + ".dat");
  Visualization::plotMeshPhysical1DX(p0, Geometry::meshSizeY / 2);
  //Visualization::plotMeshPhysical1DY(p0, Geometry::meshSizeX / 2);

  // Time-loop.
  for (timeCur = 0.; timeCur < Physics::timeEnd; timeCur += tau)
  {
    // Part of boundary conditions.
    if (BC::isBoundaryCondXPeriodic) {
      Utility::glueMeshBoundariesX(p0);
    } else {
      Utility::fillMeshBoundariesX(p0);
    }
    if (BC::isBoundaryCondYPeriodic) {
      Utility::glueMeshBoundariesY(p0);
    } else {
      Utility::fillMeshBoundariesY(p0);
    }

    // Calculate reconstruction.
    rec.reconstruct(p0, MeshRec::RecType::Muscl2);
    if (THINC::enableTHINC) THINC::bvd(p0, rec, THINC::RecType::Muscl2);

    /* Utility::boundAlpha1(rec); */

    // Apply boundary conditions.
    BC::applyBoundaryConditions1(p0, rec);

    // Part of boundary conditions.
    if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(rec);
    if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(rec);

    // Calculate differential flux.
    fD.calcFlux(rec);

    // Part of boundary conditions.
    if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(fD);
    if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(fD);

    // Calculate conservative variables.
    MeshConversion::toConservative(rec, recC);

    // Part of boundary conditions.
    if (BC::isBoundaryCondXPeriodic) Utility::glueMeshEdgeBoundariesX(recC);
    if (BC::isBoundaryCondYPeriodic) Utility::glueMeshEdgeBoundariesY(recC);

    // Calculate numerical flux.
    fN.calcFlux(fD, recC, rec, p0);

    // Recalc timestep.
    calcTau(fN.maxCX(), fN.maxCY());

    // Apply boundary conditions.
    BC::applyBoundaryConditions2(p0, rec, fN);

    if (Physics::timeEnd - timeCur < tau) {
      tau = Physics::timeEnd - timeCur;
    }

    const double kx = tau / hx;
    const double ky = tau / hy;

    #pragma omp parallel for private(cVector0, cVectorNew)
    for (size_t j = Geometry::firstCell; j <= Geometry::lastCellY; ++j) {
      for (size_t i = Geometry::firstCell; i <= Geometry::lastCellX; ++i) {

        // Convert primitive cell to conservative cell cVector0.
        MeshConversion::toConservative(p0, cVector0, i, j);

        // Evolve U0 (alpha1).
        const double xFlux0 = ((fN.c.xF(0, i, j) - fN.c.xB(0, i, j)) + (-cVector0[0]) * (fN.nC.xF(0, i, j) - fN.nC.xB(0, i, j))) * kx;
        const double yFlux0 = ((fN.c.yF(0, i, j) - fN.c.yB(0, i, j)) + (-cVector0[0]) * (fN.nC.yF(0, i, j) - fN.nC.yB(0, i, j))) * ky;
        cVectorNew[0] = cVector0[0] - (xFlux0 + yFlux0);

        // Evaluate U1 - U4.
        for (size_t v = 1; v < 5; ++v) {
          const double xFlux = (fN.c.xF(v, i, j) - fN.c.xB(v, i, j)) * kx;
          const double yFlux = (fN.c.yF(v, i, j) - fN.c.yB(v, i, j)) * ky;
          cVectorNew[v] = cVector0[v] - (xFlux + yFlux);
        }

        // Evaluate U5 - U8.
        const double xFlux5 = ((fN.c.xF(5, i, j) - fN.c.xB(5, i, j)) + (-cVector0[5]) * (fN.nC.xF(5, i, j) - fN.nC.xB(5, i, j))) * kx;
        const double yFlux5 = ((fN.c.yF(5, i, j) - fN.c.yB(5, i, j)) + (-cVector0[7]) * (fN.nC.yF(5, i, j) - fN.nC.yB(5, i, j))) * ky;
        cVectorNew[5] = cVector0[5] - (xFlux5 + yFlux5);

        const double xFlux6 = ((fN.c.xF(6, i, j) - fN.c.xB(6, i, j)) + (-cVector0[6]) * (fN.nC.xF(6, i, j) - fN.nC.xB(6, i, j))) * kx;
        const double yFlux6 = ((fN.c.yF(6, i, j) - fN.c.yB(6, i, j)) + (-cVector0[8]) * (fN.nC.yF(6, i, j) - fN.nC.yB(6, i, j))) * ky;
        cVectorNew[6] = cVector0[6] - (xFlux6 + yFlux6);

        const double xFlux7 = ((fN.c.xF(7, i, j) - fN.c.xB(7, i, j)) + (-cVector0[5]) * (fN.nC.xF(7, i, j) - fN.nC.xB(7, i, j))) * kx;
        const double yFlux7 = ((fN.c.yF(7, i, j) - fN.c.yB(7, i, j)) + (-cVector0[7]) * (fN.nC.yF(7, i, j) - fN.nC.yB(7, i, j))) * ky;
        cVectorNew[7] = cVector0[7] - (xFlux7 + yFlux7);

        const double xFlux8 = ((fN.c.xF(8, i, j) - fN.c.xB(8, i, j)) + (-cVector0[6]) * (fN.nC.xF(8, i, j) - fN.nC.xB(8, i, j))) * kx;
        const double yFlux8 = ((fN.c.yF(8, i, j) - fN.c.yB(8, i, j)) + (-cVector0[8]) * (fN.nC.yF(8, i, j) - fN.nC.yB(8, i, j))) * ky;
        cVectorNew[8] = cVector0[8] - (xFlux8 + yFlux8);

        // Save new cell as primitive to p0.
        MeshConversion::toPrimitive(cVectorNew, p0, i, j);
      }
    }

    if (PressureRelaxation::enablePressureRelaxation) PressureRelaxation::relaxPressureMesh(p0);

    // Update iteration.
    ++iteration;

    // Update status and graps.
    if (iteration % statusInterval == 0) {
      status();

      //Visualization::plotMeshPhysical(p0);
      //Visualization::plotMeshPhysicalToFile(p0, std::to_string(iteration));
      //Utility::saveMeshToFile(p0, std::to_string(iteration) + ".dat");
      Visualization::plotMeshPhysical1DX(p0, Geometry::meshSizeY / 2);
      //Visualization::plotMeshPhysical1DY(p0, Geometry::meshSizeX / 2);
    }
  }

  // Show status and solution.
  status();
  //Visualization::plotMeshPhysical(p0);
  Visualization::plotMeshPhysicalToFile(p0, std::to_string(iteration));
  //Visualization::plotMeshPhysical1DX(p0, Geometry::meshSizeY / 2);
  //Visualization::plotMeshPhysical1DY(p0, Geometry::meshSizeX / 2);

  // Save solution to file.
  std::string filename(   "mesh-"
                        + std::to_string(Geometry::cellCountX)
                        + "x"
                        + std::to_string(Geometry::cellCountY)
                        + "-i"
                        + std::to_string(iteration)
                        + "-t"
                        + std::to_string(timeCur)
                        + ".dat");

  Utility::saveMeshToFile(p0, filename);
}

void SchemeO1::info() const
{
  Scheme::info();

  std::cout << "Scheme order: 1" << std::endl;
}

