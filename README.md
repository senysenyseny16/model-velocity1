# Numerical scheme for multiphase porous-liquid flow

## Built With

* [Catch2](https://github.com/catchorg/Catch2)
* [Eigen3](http://eigen.tuxfamily.org/)

```
Copy Catch2/include/catch.hpp to externals/catch2
Copy all eigen files to to externals/eigen
```

